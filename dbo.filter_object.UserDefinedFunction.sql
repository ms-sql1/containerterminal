USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[filter_object]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[filter_object](@object_num varchar(50), @userid int, @doctype int, @Docid int)
RETURNS int
AS
BEGIN
	
	declare @c varchar(50), @v varchar(250), @res int, @cnt int;
	
	set @res = 0;
	
	/*select @cnt = count(*) from temp_filter f 
	where f.userid = @userid 
	and CHARINDEX(convert(varchar(10),@doctype),f.doctypes)>0
	and c='object_num';*/
	
	--if @cnt = 0 return 3;
	
	select @res=3 where exists 
	(
		select 1 from temp_filter f
		where f.c = 'object_num' 
		and (f.userid = @userid and CHARINDEX(convert(varchar(10),@doctype),f.doctypes)>0)
		and charindex(f.v, @object_num)>0
	) 
	
	return @res;


END

GO
