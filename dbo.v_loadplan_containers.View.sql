USE [sot]
GO
/****** Object:  View [dbo].[v_loadplan_containers]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_loadplan_containers]
 
AS
SELECT        l.container_id, j.load_plan_id
FROM            dbo.docload AS l INNER JOIN
                         dbo.docloadjoint AS j ON l.joint_id = j.id INNER JOIN
                         dbo.loadplan AS p ON j.load_plan_id = p.id
where not container_id is null
GO
