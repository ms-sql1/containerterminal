USE [sot]
GO
/****** Object:  View [dbo].[v_containermatrixinfo]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[v_containermatrixinfo] 
as
select 
c.id as container_id, 
o.real_state_id, 
c.cnum, 
isnull((select ck.code from containerkinds ck where ck.id = c.kind_id), '') as kind_code,
c.carrying,
c.container_weight,  
cr.weight_fact, 
(case when isnull(ic.weight_shift,0) = 1 then 'ДА' else 'НЕТ' end) weight_shift, 
isnull((select ct.code from counteragents ct where ct.id = o.owner_id), '') as owner,
(case 
  when o.owner_id in (select ct.id from counteragents ct, settings s where s.code = 'fesco_owners' and charindex(ct.code, s.value)>0)
  then '' else 'С'
end) as ownmark,
c.date_made, 
c.date_stiker, 
isnull(c.isdefective,0) as isdefective, 
(case when isnull(c.isdefective,0) = 1 then 'ДА' else 'НЕТ' end) as defect_mark,
(case when isnull(c.isnotready,0) = 1 then 'НЕТ' else 'ДА' end) as isready,
(case when cr.isempty = 1 then 'ПОРОЖНИЙ ' when cr.isempty = 0 then 'ГРУЖЕННЫЙ' else '-' end) as loaded, 
isnull(cr.isempty,0) as isempty,
dlv.code as current_process, 
lat.start_date, 
lat.stay_start_date, 
isnull(lat.inplace_state_code, (select sk1.code from objectstatekinds sk1 where sk1.id = o.real_state_id)) as inplace_state_code,
(case 
	when exists (
	select 1 from tasks t0, objects o0, objectstatekinds sk0, deliverytypes dt0 
	where t0.object_id = o0.id and o0.state_id = sk0.id and t0.dlv_type_id = dt0.id
	and dt0.global_section='output'
	and isnull(sk0.inplacesign,0)=1 and isnull(sk0.isformalsign,0) = 1 and isnull(sk0.inworksign,0) = 0 
	and (lat.start_date-t0.start_date < 100) and o0.linked_object_id = c.id
	) 
	then 'Есть заявка на отправку'
	when exists (select 1 from docload l, documents d where l.id = d.id and isnull(d.isconfirmed,0) = 0 and l.container_id = c.id)
	then 'Спланирована погрузка'
	else
	lat.formal_state_code
	end
) as formal_state_code,
(case 
	when exists (select 1 from docload l, documents d where l.id = d.id and isnull(d.isconfirmed,0) = 0 and l.container_id = c.id)
	then '1' else '0'
	end
) as loadplanned,
sk_formal.mark as formalstate_mark,
isnull((select mark from objectstatekinds sk where sk.id = lat.real_state_id),'') as state_mark,  
sk.mark as next_state_mark, 
sk.code as next_state_code,
(select name from cargotypes ct where ct.id = cr.type_id) as cargotype,
(select top 1 p.car_number+', '+d.person_name from passes p 
inner join passoperations po  on (po.parent_id = p.id)
left outer join persons d on (d.id = driver_id)
and po.task_id = lat.task_id
order by po.id desc) as pass_data
from containers c
left outer join objects o on (c.id = o.id)
left outer join v_activecontainertask lat on (lat.container_id = c.id)
left outer join deliverytypes dlv on (lat.dlv_type_id = dlv.id)
left outer join cargos cr on (cr.id = lat.cargo_id)
left outer join docincomespec ic on (ic.container_id = lat.container_id and ic.doc_id = lat.doc_id) 
left outer join objectstatekinds sk on (sk.id = lat.next_state_id) 
left outer join objectstatekinds sk_formal on (sk_formal.id = lat.state_id) 







GO
