USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[CancelConfirmDoc]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CancelConfirmDoc](
@DocId int, @CargoId int, @UserId int
) 
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @id int,@RootDocId int,@PlanDate datetime,@ForwarderId int,@ConsigneeId int,@PayerId int, @ObjectId int;
	
	declare @e int, @m varchar(250);
	
	set @e = 0;
	
	--1) Проверяем, что запись в истории последняя
	
	select @e=max(1) from  objectstatehistory osh 
	where osh.doc_id = @DocId
	and (isnull(@CargoId,0) = 0 or osh.object_id = @CargoId)
	and exists (
		select 1 from objectstatehistory osh1  where osh1.object_id = osh.object_id 
		and osh1.task_id = osh.task_id
		and osh1.doc_id <> osh.doc_id
		and osh1.date_factexecution>osh.date_factexecution 
	);
	
	if @e = 1 
	begin
		select @m = dt.code+' № '+d.doc_number from doctypes dt, documents d where d.id = @DocId and d.doctype_id = dt.id;
		set @m = 'Документ '+@m+' имеет более поздние операции. Отмена проведения невозможна.';
		raiserror(@m, 16,1);
		return 0;
	end;

	-- удалили корневой обьект - груз
	delete from objectstatehistory where doc_id = @DocId and (isnull(@CargoId,0) = 0 or object_id = @CargoId);

	-- удаляем все связанные с грузом
	--1)
	delete from objectstatehistory
	where not exists (
		select 1 from objectstatehistory osh1  
		where osh1.doc_id = @DocId 
		and osh1.linked_object_id = objectstatehistory.object_id
		and osh1.task_id = objectstatehistory.task_id
	) 
	and not exists 
	(select 1 from objects o where o.id = objectstatehistory.object_id and o.object_type = 'cargo')
	and doc_id = @DocId;

	--2)
	delete from objectstatehistory
	where not exists (
		select 1 from objectstatehistory osh1  
		where osh1.doc_id = @DocId 
		and osh1.linked_object_id = objectstatehistory.object_id
		and osh1.task_id = objectstatehistory.task_id
	) 
	and not exists 
	(select 1 from objects o where o.id = objectstatehistory.object_id and o.object_type = 'cargo')
	and doc_id = @DocId;

	delete from containerdefecthistory where register_doc_id = @DocId and container_id in 
	(select object_id from objects2docspec where doc_id = @Docid);
	
    exec UpdateObjectState @DocId, 1;

    -- проверяем полное уничтожение всей истории документа. если так, то удаляем задачу
    delete from tasks where not exists (select 1 from objectstatehistory osh where osh.task_id = tasks.id);

    delete from objects2docspec where not exists (select 1 from tasks t where t.id = objects2docspec.task_id);

	if isnull(@CargoId,0) <> 0 
	begin
		delete from objects2docspec 
		where not exists (select 1 from objectstatehistory osh where osh.doc_id = objects2docspec.doc_id and osh.object_id = objects2docspec.object_id)
		and doc_id = @DocId;
	end;

	if isnull(@CargoId,0) = 0 
	begin
		insert into change_log(table_id, table_name, user_id, event_type, event_datetime)
		values (@DocId, 'documents', @UserId, 'cancel', getdate());
	end;
    
	update documents set isconfirmed = 0, date_confirm = null where id = @DocId and isnull(@CargoId,0) = 0;

	update doccargosheet set task_id = null 
	where not exists (select 1 from documents d where d.id = doccargosheet.id and d.isconfirmed = 1)
	and not exists (select 1 from docfeed f, documents d1 where f.id = d1.id and d1.isconfirmed = 1 and f.id = doccargosheet.unload_doc_id);

	update docfeed set task_id = null 
	where not exists (select 1 from documents d where d.id = docfeed.id and d.isconfirmed = 1)
	and not exists (select 1 from doccargosheet sh, documents d1 where sh.id = d1.id and d1.isconfirmed = 1 and docfeed.id = sh.unload_doc_id);

END

GO
