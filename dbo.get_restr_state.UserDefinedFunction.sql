USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[get_restr_state]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[get_restr_state](@DocId int)
RETURNS varchar(20)
AS
BEGIN

	declare @res varchar(20);

	set @res = '   ';

	select @res = 'Есть' where exists (select 1 from doccargorestrictions r where r.doc_id = @DocId);

	if @res = 'Есть'
	select @res = 'Сняты' where not exists (select 1 from doccargorestrictions r where r.doc_id = @DocId and r.dateend is null);

	return @res;

END;

GO
