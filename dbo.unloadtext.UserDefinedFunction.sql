USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[unloadtext]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[unloadtext]
(
	@checkresult int, @dateunload datetime
)
RETURNS varchar(200)
AS
BEGIN

	declare @datetext varchar(29), @checktext varchar(450);
	
	if @checkresult is null return '';
	if @dateunload is null return '';
	
	select @datetext = CONVERT(varchar, @dateunload, 104)+' '+CONVERT(varchar, @dateunload, 108);
	
	select @checktext = 
	(case when @checkresult = 1 then 'Проверку прошел/Разгружен' 
	when @checkresult = 2 then 'Проверку НЕпрошел/Разгружен' 
	when @checkresult>2 then 'Прочий статус разгрузки' 
	else ' ' end);	
	
	RETURN @checktext+' '+@datetext;

END

GO
