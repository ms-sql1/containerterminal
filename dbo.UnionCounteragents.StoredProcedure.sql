USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[UnionCounteragents]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UnionCounteragents](@id1 integer, @id2 integer)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	update doccargosheet set forwarder_id = @id2 where forwarder_id = @id1;
	update docorder set forwarder_id = @id2 where forwarder_id = @id1;
	update tasks set forwarder_id = @id2 where forwarder_id = @id1;
	
	update doccargosheet set consignee_id = @id2 where consignee_id = @id1;
	update docorder set consignee_id = @id2 where consignee_id = @id1;
	update tasks set consignee_id = @id2 where consignee_id = @id1;
	
	update doccargosheet set payer_id = @id2 where payer_id = @id1;
	update docorder set payer_id = @id2 where payer_id = @id1;
	update tasks set payer_id = @id2 where payer_id = @id1;
	
	update docextraservices set customer_id = @id2 where customer_id = @id1;
	update price_store_custs set customer_id = @id2 where customer_id = @id1;
	update contracts set customer_id = @id2 where customer_id = @id1;
	update price_service_custs set customer_id = @id2 where customer_id = @id1;
	update docemptyincome set customer_id = @id2 where customer_id = @id1;
	
	update alertque	 set counteragent_id =  @id2 where counteragent_id = @id1;
	
	insert into externaldatalinks (data_type, text_caption, data_caption)
	select 'counteragents', 
	(select code from counteragents where id = @id1),
	(select code from counteragents where id = @id2)
	where not exists (
		select 1 from externaldatalinks where data_type = 'counteragents'
		and text_caption = (select code from counteragents where id = @id1)
		and data_caption = (select code from counteragents where id = @id2)
	)
	
END
GO
