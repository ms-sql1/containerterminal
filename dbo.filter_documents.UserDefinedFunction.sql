USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[filter_documents]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[filter_documents](@userid int, @doctype int, @Docid int)
RETURNS int
AS
BEGIN
	
	declare @c varchar(50), @v varchar(250), @res int, @cnt int;
	
	set @res = 0;
	set @cnt = 0;

	select @cnt = 1 where not exists (select 1 from temp_filter f where f.userid = @userid 
	and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0 and c='date_begin');

	if @cnt = 1 return 0;

	select @cnt = 1 where exists (select 1 from temp_filter f where f.userid = @userid 
	and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0 and c<>'');
	
	if @cnt = 0 return 3;
	
	select @res=1 where not exists 
	(
		select 1 from temp_filter f 
		where f.c = 'object_num' 
		and (f.userid = @userid and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0)
	)
	or exists
	(
		select 1 from objects2docspec o2s, v_objects o, temp_filter f
		where o.id = o2s.object_id and o2s.doc_id = @Docid and 
		f.c = 'object_num' and charindex(f.v, o.cnum)>0 
		and (f.userid = @userid and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0)
	) 
	
	if @res>0
	select @res=@res+1 
	from documents d where d.id = @Docid 
	and (
		d.doc_date >= (
		select max(CONVERT(datetime, f.v, 104)) from temp_filter f
		where f.c = 'date_begin' 
		and (f.userid = @userid and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0))
		or not exists 
		(
			select 1 from temp_filter f 
			where f.c = 'date_begin' 
			and (f.userid = @userid and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0)
		)
	)
	and (
		d.doc_date < (
		select min(CONVERT(datetime, f.v, 104)+1) from temp_filter f
		where f.c = 'date_end' 
		and (f.userid = @userid and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0))
		or not exists 
		(
			select 1 from temp_filter f 
			where f.c = 'date_end' 
			and (f.userid = @userid and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0)
		)
	);
	
	if @res>0
	select @res=@res+1 from documents d where d.id = @Docid 
	and (
		(d.isconfirmed = 1 and (select f.v from temp_filter f 
		where (f.userid = @userid and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0) 
		and f.c = 'isconfirmed') = 1)
		or not exists
		(select 1 from temp_filter f 
		where (f.userid = @userid 
		and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0) and f.c = 'isconfirmed')
	)
	
	return @res;


END;
GO
