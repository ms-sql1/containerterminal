USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[CreatePassSpec]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CreatePassSpec](@currentid int, @docid int, @containerid int)
as
begin

	insert into passoperations (parent_id, passoperation_type, passoperation_id, container_id, task_id, seal_number, isempty)
	select @currentid ,0, pk.id as operation_id, c.id as container_id, t.id as task_id,
	cr.seal_number, pos.isempty
	from v_lastobjectstates los, objectstatehistory osh, tasks t, containers c, cargos cr, documents d, doctypes dt, passoperationkinds pk, passopsettings pos
	where osh.task_id = t.id and osh.linked_object_id = c.id and osh.object_id = cr.id and pk.id = pos.kind_id
	and osh.doc_id = d.id and dt.id = d.doctype_id and pk.system_section = dt.system_section
	and pos.dlv_type_id = t.dlv_type_id and los.object_state_id = pos.start_state_id 
	and los.task_id = t.id and los.object_id = osh.object_id
	and (
		(isnull(cr.isempty,0) = isnull(pos.isempty,0) and isnull(pos.ismain,0) = 1)
    )
	and osh.doc_id = @docid and (osh.linked_object_id = @containerid or @containerid = 0);

	insert into passoperations (parent_id, passoperation_type, passoperation_id, container_id, task_id, seal_number, isempty)
	select @currentid ,0, pk.id as operation_id, c.id as container_id, t.id as task_id,
	(case when pos.seal_number = '@' then '' else cr.seal_number end), pos.isempty
	from v_lastobjectstates los, objectstatehistory osh, tasks t, containers c, cargos cr, documents d, doctypes dt, 
	passoperationkinds pk, passopsettings pos
	where osh.task_id = t.id and osh.linked_object_id = c.id and osh.object_id = cr.id and pk.id = pos.kind_id
	and osh.doc_id = d.id and dt.id = d.doctype_id and pk.system_section = dt.system_section
	and pos.dlv_type_id = t.dlv_type_id and los.object_state_id = pos.start_state_id 
	and los.task_id = t.id and los.object_id = osh.object_id
	and (isnull(pos.ismain,0) = 0)
	and osh.doc_id = @docid and (osh.linked_object_id = @containerid or @containerid = 0);

    update po0 set higher_id = (
    select distinct po.id from passoperations po, passoperationkinds pk, passopsettings pos
    where po.parent_id = @currentid
    and po.passoperation_id = pk.id
    and pk.id = pos.kind_id
    and po.container_id = po0.container_id
    and pk0.id = pos.spare_kind_id
    and pos.dlv_type_id = pos0.dlv_type_id
    )
    from passoperations po0, passoperationkinds pk0, passopsettings pos0
    where po0.passoperation_id = pk0.id and pos0.kind_id = pk0.id
    and po0.parent_id = @currentid;

	/*delete p
	from passoperations p, objectstatehistory osh, cargos cr 
	where p.parent_id = @currentid
	and osh.object_id = cr.id and osh.task_id = p.task_id
	and (
	   isnull(cr.isempty,0) <> isnull(p.isempty,0)
	   and not exists (
	     select 1 from passoperations p1, objectstatehistory osh1, cargos cr1 
		 where p1.parent_id = @currentid
		 and osh1.object_id = cr1.id and osh1.task_id = p1.task_id
		 and (p1.higher_id = p.id or p.higher_id = p1.id)
		 and (isnull(p1.isempty,0) = isnull(cr1.isempty,0) or isnull(p1.isempty,0) = isnull(cr.isempty,0))
	   )
	)*/

end;
GO
