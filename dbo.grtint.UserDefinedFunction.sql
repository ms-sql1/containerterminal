USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[grtint]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[grtint]
(
	@Val1 int, @Val2 int
)
RETURNS int
AS
BEGIN

	return (case when @Val1>@Val2 then @Val1 else @Val2 end);

END

GO
