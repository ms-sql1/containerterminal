USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[CreateDocFromPass]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateDocFromPass](@passopid int, @folderid int, @userid int)
AS
BEGIN
	SET NOCOUNT ON;

	declare @newdoctypeid int, @id int, @defaultdlvtypeid int, @cnum varchar(20), @ownerid int, @kindid int, @ContId int,
	@drivername varchar(70), @cardata varchar(120), @attorney varchar(120);
	
	select  @newdoctypeid = pk.end_doctype_id, @defaultdlvtypeid = pk.end_dlv_type_id
	from passoperations po, passoperationkinds pk where po.passoperation_id = pk.id and po.id = @passopid;

	select @drivername = pr.person_name, @cardata  = p.car_data+' '+p.car_number, @attorney = p.attorney,
	@Cnum = po.container_num, @KindId = po.container_kind_id, @OwnerId = po.container_owner_id
	from passes p inner join passoperations po on (po.parent_id = p.id)
	left outer join persons pr on (pr.id = p.driver_id)
	where po.id = @passopid;
	
	insert into documents (doctype_id, folder_id, isbased, user_created) 
	values (@newdoctypeid, @folderid, 1, @userid);

	select @id = @@IDENTITY;
	
	insert into docincome (id, dlv_type_id, driver_name, car_data, attorney) values (@id, @defaultdlvtypeid, @drivername, @cardata, @attorney);

	update passoperations set childdoc_id = @id where id = @passopid;

	exec UpdateRegisterContainer @ContId output, @Cnum, @KindId, null, @OwnerId, '', null;

	insert into docincomespec (
	doc_id, o2s_id, container_id, container_num, container_kind_id, container_owner_id, cargo_id, seal_number, cargotype_id, isempty
	)
	select @id, null, @ContId, isnull(c.cnum, po.container_num), po.container_kind_id, po.container_owner_id, null, po.seal_number, null, po.isempty 
	from passoperations po 
	left outer join containers c on (c.id = po.container_id) 
	where po.id = @passopid;

END

GO
