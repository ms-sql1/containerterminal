USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[GetPurposeDocumentTypes]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPurposeDocumentTypes] (@selectguid varchar(40))
AS
BEGIN
	SET NOCOUNT ON;

	select d.id, d.doctype_id into #d from documents d with (nolock), selectlist sl with (nolock) where d.id = sl.id and sl.guid = @selectguid;
	
	select t.id as task_id, d.doctype_id, t.dlv_type_id, los.linked_object_id, los.object_type, los.object_state_id, los.real_state_id
	into #tlos from #d d
	inner join objects2docspec ods on (d.id = ods.doc_id)
	inner join tasks t on (t.id = ods.task_id)
	inner join v_lastobjectstates los on (ods.object_id = los.object_id and los.task_id = t.id)

	select r.id as rid, r.end_doctype_id, rs.id as rsid, tlos.task_id,
	(select code from objectstatekinds sk where sk.id = rs.start_object_state_id)+'->' as start_state_code, 
	(select code from doctypes dt where dt.id = r.end_doctype_id) as doctype_code, 
	convert(bit, isnull(CHARINDEX(','+ltrim(str(tlos.real_state_id))+',',','+r.pref_states_ids+','),0)+
	isnull(CHARINDEX(','+ltrim(str(o1.real_state_id))+',',','+r.pref_states_ids+','),0))
	as pref_sign
	into #temp_doc_types
	from #tlos as tlos
	inner join objecttypes ot on (ot.code = tlos.object_type)
	inner join docroutes r on (r.dlv_type_id = tlos.dlv_type_id)
	left outer join docroute2objectstates rs WITH (INDEX(docroute_id_idx)) on (
		r.id = rs.docroute_id
		and charindex(isnull(convert(varchar, tlos.object_state_id),''), isnull(r.exclude_states_ids,''))=0
		and charindex(isnull(convert(varchar, tlos.real_state_id),''), isnull(r.exclude_states_ids,''))=0
		and
			(
				(((rs.start_object_state_id is null or rs.start_object_state_id = tlos.object_state_id) and ot.id = rs.objecttype_id) or (isnull(rs.start_object_state_id,0) = 0 and not rs.start_link_type is null))
				and (rs.start_real_state_id is null or rs.start_real_state_id = tlos.real_state_id ) 
			)
		) 
	left outer join objects o1 on (o1.id = tlos.linked_object_id) 
	where r.end_doctype_id <> tlos.doctype_id
	and isnull(r.isrestricted,0) = 0
	and isnull(rs.end_object_state_id,0) <> tlos.object_state_id
	order by 3 desc;

	select distinct rid, /*start_state_code, */end_doctype_id, doctype_code, convert(bit,max(convert(integer, pref_sign))) as pref_sign
	from #temp_doc_types t
	where not exists (select 1 from #temp_doc_types t1 where t1.rid = t.rid and t1.task_id = t.task_id and t1.rsid is null)
	group by rid, /*start_state_code, */end_doctype_id, doctype_code
	order by 4 desc;
	
END
GO
