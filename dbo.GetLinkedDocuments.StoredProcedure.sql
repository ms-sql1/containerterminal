USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[GetLinkedDocuments]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetLinkedDocuments](@DocId int)
as
begin
	select d1.*, (select code from doctypes t where t.id = d1.doctype_id) as type_code
	from documents d1 where  d1.id>@DocId and d1.id in
	(
		select ds1.doc_id from 
		(
		select ds.object_id, ds.task_id 
		from documents d, objects2docspec ds 
		where ds.doc_id = d.id and d.id = @DocId
		) s1, objects2docspec ds1
		where ds1.task_id = s1.task_id
		and ds1.object_id = s1.object_id 
		and ds1.doc_id<> @DocId
	)
end;

GO
