USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[InsertObject]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[InsertObject](@object_type varchar(50), @id int out)
as
begin
	insert into objects (start_datetime, object_type) values (GETDATE(), @object_type);
	
	select @id = @@IDENTITY;
	
end;
GO
