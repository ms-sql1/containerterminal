USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[NumPhrase]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Function [dbo].[NumPhrase] (@Num BigInt, @IsMaleGender Bit = 1)
Returns Varchar(255)
As
Begin 
	Declare @nword Varchar(255), 
		@th TinyInt, 
		@gr SmallInt, 
		@d3 TinyInt, 
		@d2 TinyInt, 
		@d1 TinyInt
  If @Num < 0 Return '*** Error: Negative value' 
	Else If @Num = 0 Return 'Ноль' /* особый случай */ 
  While @Num > 0
  Begin
    Set @th = IsNull(@th, 0) + 1    
    Set @gr = @Num % 1000    
    Set @Num = (@Num - @gr) / 1000
    If @gr > 0
    Begin
      Set @d3 = (@gr - @gr % 100) / 100
      Set @d1 = @gr % 10
      Set @d2 = (@gr - @d3 * 100 - @d1) / 10
      If @d2 = 1 Set @d1 = 10 + @d1 
      Set @nword = Case @d3 
                When 1 Then ' сто' 
		When 2 Then ' двести' 
		When 3 Then ' триста'
                When 4 Then ' четыреста' 
		When 5 Then ' пятьсот' 
		When 6 Then ' шестьсот'
                When 7 Then ' семьсот' 
		When 8 Then ' восемьсот' 
		When 9 Then ' девятьсот' 
		Else '' End
                + Case @d2
                When 2 Then ' двадцать' 
		When 3 Then ' тридцать' 
		When 4 Then ' сорок'
                When 5 Then ' пятьдесят' 
		When 6 Then ' шестьдесят' 
		When 7 Then ' семьдесят'
                When 8 Then ' восемьдесят' 
		When 9 Then ' девяносто' 
		Else '' End
                + Case @d1
                When 1 Then (Case When @th = 2 Or (@th = 1 And @IsMaleGender = 0) Then ' одна' Else ' один' End)
                When 2 Then (Case When @th = 2 Or (@th = 1 And @IsMaleGender = 0) Then ' две' Else ' два' End)
                When 3 Then ' три' 
		When 4 Then ' четыре' 
		When 5 Then ' пять'
                When 6 Then ' шесть' 
		When 7 Then ' семь' 
		When 8 Then ' восемь'
                When 9 Then ' девять' 
		When 10 Then ' десять' 
		When 11 Then ' одиннадцать' 
                When 12 Then ' двенадцать' 
		When 13 Then ' тринадцать' 
		When 14 Then ' четырнадцать' 
                When 15 Then ' пятнадцать' 
		When 16 Then ' шестнадцать' 
		When 17 Then ' семнадцать'
                When 18 Then ' восемнадцать' 
		When 19 Then ' девятнадцать' 
		Else '' End
                + Case @th
                  When 2 Then ' тысяч' + (Case When @d1=1 Then 'а' When @d1 In (2, 3, 4) Then 'и' Else '' End)
                  When 3 Then ' миллион' When 4 Then ' миллиард' When 5 Then ' триллион' When 6 Then ' квадрилион' When 7 Then ' квинтилион'
                  Else '' End
                + case When @th In (3, 4, 5, 6, 7) Then (Case When @d1 = 1 Then '' When @d1 In (2, 3, 4) Then 'а' Else 'ов' End) Else '' End 
                + IsNull(@nword, '')
    End  
  End
  Return Upper(SubString(@nword, 2, 1)) + SubString(@nword, 3, Len(@nword) - 2) 
End

GO
