USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[CreatePassHeader]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CreatePassHeader](@docid int)
as
begin
	insert into passes (pass_type, payer_id, dealer_id, base_doc_id)
	select top 1 0, t.payer_id, t.payer_id, d.id
	from objectstatehistory osh, tasks t, containers c, cargos cr, documents d, doctypes dt, passoperationkinds pk
	where osh.task_id = t.id and osh.linked_object_id = c.id and osh.object_id = cr.id
	and osh.doc_id = d.id and dt.id = d.doctype_id and pk.system_section = dt.system_section
	and exists (select 1 from passopsettings pos where pos.kind_id = pk.id and pos.dlv_type_id = t.dlv_type_id)
	and osh.doc_id = @docid;
end;

GO
