USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[UpdateRegisterCarriageFull]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateRegisterCarriageFull]
(
@Cnum varchar(50),
@KindCode varchar(50),
@OwnerCode varchar(150),
@ModelCode varchar(50), 
@TechCondCode varchar(50)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @ObjectId int, @id int, @oldownerid int, @OwnerId int;
	
	select @Objectid = c.id from carriages c where c.pnum = @Cnum;
	
	select @OwnerId = id from counteragents where code = @OwnerCode or name = @OwnerCode;
	
	select @id = id from carriagemodels where model_code = @ModelCode;
	if @id is null
	insert into carriagemodels (model_code) values (@ModelCode);
	
	select @id = id from techconditions where code = @TechCondCode
	if @id is null
	insert into techconditions (code) values (@TechCondCode);
	
	if isnull(@Objectid,0)=0
	begin
	   
	   exec InsertObject 'carriage', @ObjectId output;
	   
	   insert into carriages (id, pnum, kind_id, model_id, techcond_id)
	   values (@ObjectId, @Cnum, 
		(select id from carriagekinds k where (k.code = @KindCode or convert(varchar, k.footsize) = @KindCode)), 
		(select id from carriagemodels m where model_code = @ModelCode), 
		(select id from techconditions tc where code = @TechCondCode)
		); 
	   
	end else
	begin
	   
	   update carriages set 
	   kind_id =  isnull(kind_id, (select id from carriagekinds k where (k.code = @KindCode or convert(varchar, k.footsize) = @KindCode))),
	   model_id =  isnull(model_id, (select id from carriagemodels where model_code = @ModelCode)),
	   techcond_id =  isnull(techcond_id, (select id from techconditions where code = @TechCondCode))
	   where id = @ObjectId;
	   
	end;
	
	select @oldownerid = owner_id from objects where id = @ObjectId;
	--if (isnull(@oldownerid,0) <> @OwnerId) and (ISNULL(@OwnerId,0)<>0)
	
	if isnull(@oldownerid,0) <= 0
	insert into objectownerhistory (object_id, owner_id, date_change)
	values (@ObjectId, @OwnerId, GETDATE());
	
END

GO
