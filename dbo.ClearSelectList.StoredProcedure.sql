USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[ClearSelectList]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ClearSelectList](@guid varchar(40))
AS
BEGIN
	
	SET NOCOUNT ON;
	
	delete from selectlist where guid = @guid;
	
END
GO
