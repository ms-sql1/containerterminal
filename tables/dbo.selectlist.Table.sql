USE [sot]
GO
/****** Object:  Table [dbo].[selectlist]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[selectlist](
	[guid] [varchar](40) NULL,
	[id] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
