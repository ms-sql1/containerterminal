USE [sot]
GO
/****** Object:  Table [dbo].[price_service_custs]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[price_service_custs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[servicekind_id] [int] NULL,
	[customer_id] [int] NULL,
	[date_change] [datetime] NULL,
	[price_default] [float] NULL,
 CONSTRAINT [PK_price_service_custs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
