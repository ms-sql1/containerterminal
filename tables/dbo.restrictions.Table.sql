USE [sot]
GO
/****** Object:  Table [dbo].[restrictions]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[restrictions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[start_date] [datetime] NULL,
	[kind_id] [int] NULL,
	[task_id] [int] NULL,
	[cancel_datetime] [datetime] NULL,
	[cancel_base] [varchar](850) NULL,
	[cancel_user_id] [int] NULL,
 CONSTRAINT [PK_restrictions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[restrictions] ADD  CONSTRAINT [DF_restrictions_start_date]  DEFAULT (getdate()) FOR [start_date]
GO
ALTER TABLE [dbo].[restrictions]  WITH CHECK ADD  CONSTRAINT [FK_restrictions_restrictkinds] FOREIGN KEY([kind_id])
REFERENCES [dbo].[restrictkinds] ([id])
GO
ALTER TABLE [dbo].[restrictions] CHECK CONSTRAINT [FK_restrictions_restrictkinds]
GO
