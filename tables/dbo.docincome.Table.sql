USE [sot]
GO
/****** Object:  Table [dbo].[docincome]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docincome](
	[id] [int] NOT NULL,
	[dlv_type_id] [int] NULL,
	[dealer_id] [int] NULL,
	[driver_name] [varchar](50) NULL,
	[car_data] [varchar](150) NULL,
	[attorney] [varchar](150) NULL,
	[note] [varchar](450) NULL,
	[date_income] [datetime] NULL,
	[inspector_id] [int] NULL,
 CONSTRAINT [PK_docincome] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[docincome]  WITH CHECK ADD  CONSTRAINT [FK_docincome_counteragents] FOREIGN KEY([dealer_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[docincome] CHECK CONSTRAINT [FK_docincome_counteragents]
GO
ALTER TABLE [dbo].[docincome]  WITH CHECK ADD  CONSTRAINT [FK_docincome_counteragents1] FOREIGN KEY([dealer_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[docincome] CHECK CONSTRAINT [FK_docincome_counteragents1]
GO
ALTER TABLE [dbo].[docincome]  WITH CHECK ADD  CONSTRAINT [FK_docincome_persons] FOREIGN KEY([inspector_id])
REFERENCES [dbo].[persons] ([id])
GO
ALTER TABLE [dbo].[docincome] CHECK CONSTRAINT [FK_docincome_persons]
GO
