USE [sot]
GO
/****** Object:  Table [dbo].[cargos]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cargos](
	[id] [int] NOT NULL,
	[cnum] [varchar](50) NULL,
	[type_id] [int] NULL,
	[note] [varchar](450) NULL,
	[weight_sender] [float] NULL,
	[weight_doc] [float] NULL,
	[weight_fact] [float] NULL,
	[seal_number] [varchar](50) NULL,
	[isempty] [bit] NULL,
	[issealwaste] [varchar](50) NULL,
 CONSTRAINT [PK_cargos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
