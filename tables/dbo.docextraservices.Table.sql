USE [sot]
GO
/****** Object:  Table [dbo].[docextraservices]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docextraservices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc_date] [datetime] NULL,
	[doc_number] [varchar](50) NULL,
	[customer_id] [int] NULL,
	[basedoc_kind] [varchar](50) NULL,
	[basedoc_number] [varchar](50) NULL,
	[basedoc_date] [datetime] NULL,
	[date_factexecution] [datetime] NULL,
	[summa] [float] NULL,
 CONSTRAINT [PK_docextraservices] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
