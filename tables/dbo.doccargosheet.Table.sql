USE [sot]
GO
/****** Object:  Table [dbo].[doccargosheet]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doccargosheet](
	[id] [int] NULL,
	[prefix] [varchar](15) NULL,
	[datesent] [datetime] NULL,
	[train_num] [varchar](50) NULL,
	[container_num] [varchar](50) NULL,
	[container_foot] [varchar](50) NULL,
	[container_kind_id] [int] NULL,
	[container_owner_id] [int] NULL,
	[carriage_num] [varchar](50) NULL,
	[carriage_type] [varchar](50) NULL,
	[carriage_owner_id] [int] NULL,
	[unload_doc_id] [int] NULL,
	[cargotype_id] [int] NULL,
	[isempty] [bit] NOT NULL CONSTRAINT [DF_doccargosheet_isempty]  DEFAULT ((0)),
	[cargo_description] [varchar](2000) NULL,
	[seal_number] [varchar](50) NULL,
	[weight_sender] [float] NULL,
	[weight_doc] [float] NULL,
	[weight_fact] [float] NULL,
	[forwarder_id] [int] NULL,
	[consignee_id] [int] NULL,
	[payer_id] [int] NULL,
	[dlv_type_id] [int] NULL,
	[dlv_term] [datetime] NULL,
	[guard_sign] [bit] NOT NULL CONSTRAINT [DF_doccargosheet_guard_sign]  DEFAULT ((0)),
	[docreturn_sign] [bit] NOT NULL CONSTRAINT [DF_doccargosheet_docreturn_sign]  DEFAULT ((0)),
	[emptyreturn_sign] [bit] NOT NULL CONSTRAINT [DF_doccargosheet_emptyreturn_sign]  DEFAULT ((0)),
	[emptyreturn_date] [datetime] NULL,
	[need_pay] [bit] NOT NULL CONSTRAINT [DF_doccargosheet_need_pay]  DEFAULT ((0)),
	[task_id] [int] NULL,
	[alert_date] [datetime] NULL,
	[last_alert_date] [datetime] NULL,
	[alert_que_guid] [uniqueidentifier] NULL,
	[notifysign] [bit] NULL,
	[rowuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_doccargosheet_uid]  DEFAULT (newid()),
	[user_created] [int] NULL,
	[paidsumm] [float] NULL,
	[paidfull] [bit] NULL,
	[paiddate_register] [datetime] NULL,
	[dlvallowed] [bit] NULL,
	[dlvallowed_date] [datetime] NULL,
	[dlvallowed_note] [varchar](850) NULL,
	[paydocs] [varchar](450) NULL,
	[plugstart] [datetime] NULL,
	[plugend] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[doccargosheet]  WITH CHECK ADD  CONSTRAINT [FK_doccargosheet_counteragents] FOREIGN KEY([consignee_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[doccargosheet] CHECK CONSTRAINT [FK_doccargosheet_counteragents]
GO
ALTER TABLE [dbo].[doccargosheet]  WITH CHECK ADD  CONSTRAINT [FK_doccargosheet_counteragents1] FOREIGN KEY([forwarder_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[doccargosheet] CHECK CONSTRAINT [FK_doccargosheet_counteragents1]
GO
ALTER TABLE [dbo].[doccargosheet]  WITH CHECK ADD  CONSTRAINT [FK_doccargosheet_counteragents2] FOREIGN KEY([payer_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[doccargosheet] CHECK CONSTRAINT [FK_doccargosheet_counteragents2]
GO
ALTER TABLE [dbo].[doccargosheet]  WITH CHECK ADD  CONSTRAINT [FK_doccargosheet_deliverytypes] FOREIGN KEY([dlv_type_id])
REFERENCES [dbo].[deliverytypes] ([id])
GO
ALTER TABLE [dbo].[doccargosheet] CHECK CONSTRAINT [FK_doccargosheet_deliverytypes]
GO
