USE [sot]
GO
/****** Object:  Table [dbo].[docfeed]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docfeed](
	[id] [int] NOT NULL,
	[load_plan_id] [int] NULL,
	[prefix] [varchar](15) NULL,
	[train_num] [varchar](50) NULL,
	[container_num] [varchar](50) NULL,
	[container_foot] [varchar](50) NULL,
	[container_owner_id] [int] NULL,
	[carriage_num] [varchar](50) NULL,
	[carriage_type] [varchar](50) NULL,
	[carriage_owner_id] [int] NULL,
	[seal_number] [varchar](50) NULL,
	[weight_sender] [float] NULL,
	[weight_doc] [float] NULL,
	[weight_fact] [float] NULL,
	[check_result] [int] NULL,
	[check_note] [varchar](450) NULL,
	[weight_shift] [bit] NULL,
	[check_datetime] [datetime] NULL,
	[cargo_description] [varchar](2000) NULL,
	[dlv_type_id] [int] NULL,
	[unload_front_id] [int] NULL,
	[task_id] [int] NULL,
	[rowuid] [uniqueidentifier] NULL CONSTRAINT [DF_docfeed_rowuid]  DEFAULT (newid()),
	[user_created] [int] NULL,
 CONSTRAINT [PK_docfeed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[docfeed]  WITH CHECK ADD  CONSTRAINT [FK_docfeed_counteragents] FOREIGN KEY([carriage_owner_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[docfeed] CHECK CONSTRAINT [FK_docfeed_counteragents]
GO
ALTER TABLE [dbo].[docfeed]  WITH CHECK ADD  CONSTRAINT [FK_docfeed_deliverytypes] FOREIGN KEY([dlv_type_id])
REFERENCES [dbo].[deliverytypes] ([id])
GO
ALTER TABLE [dbo].[docfeed] CHECK CONSTRAINT [FK_docfeed_deliverytypes]
GO
ALTER TABLE [dbo].[docfeed]  WITH CHECK ADD  CONSTRAINT [FK_docfeed_loadplan] FOREIGN KEY([load_plan_id])
REFERENCES [dbo].[loadplan] ([id])
GO
ALTER TABLE [dbo].[docfeed] CHECK CONSTRAINT [FK_docfeed_loadplan]
GO
ALTER TABLE [dbo].[docfeed]  WITH CHECK ADD  CONSTRAINT [FK_docfeed_unloadfronts] FOREIGN KEY([unload_front_id])
REFERENCES [dbo].[unloadfronts] ([id])
GO
ALTER TABLE [dbo].[docfeed] CHECK CONSTRAINT [FK_docfeed_unloadfronts]
GO
