USE [sot]
GO
/****** Object:  Table [dbo].[extrainfo]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[extrainfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[maindata_id] [int] NULL,
	[preson_role] [varchar](250) NULL,
	[person_id] [int] NULL,
 CONSTRAINT [PK_extrainfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
