USE [sot]
GO
/****** Object:  Table [dbo].[documents]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[documents](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[folder_id] [int] NULL,
	[doc_number] [varchar](20) NULL,
	[doc_date] [datetime] NULL CONSTRAINT [DF_documents_doc_date]  DEFAULT (getdate()),
	[doctype_id] [int] NULL,
	[isconfirmed] [int] NULL,
	[date_confirm] [datetime] NULL,
	[cost_store] [float] NULL,
	[cost_extra] [float] NULL,
	[cost_main] [float] NULL,
	[isbased] [bit] NULL,
	[date_created] [datetime] NULL CONSTRAINT [DF_documents_date_created]  DEFAULT (CONVERT([datetime],CONVERT([varchar](18),getdate(),(120)),(120))),
	[date_modified] [datetime] NULL,
	[user_created] [int] NULL,
	[user_modified] [int] NULL,
	[allow_edit] [bit] NULL,
	[allow_user_id] [int] NULL,
	[isdeleted] [bit] NULL,
	[isabolished] [bit] NULL,
	[docdatetrunc] [datetime] NULL,
	[inwait] [bit] NULL,
	[datestopinwait] [datetime] NULL,
 CONSTRAINT [PK_documents] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[documents]  WITH CHECK ADD  CONSTRAINT [FK_documents_users] FOREIGN KEY([user_created])
REFERENCES [dbo].[users] ([id])
GO
ALTER TABLE [dbo].[documents] CHECK CONSTRAINT [FK_documents_users]
GO
