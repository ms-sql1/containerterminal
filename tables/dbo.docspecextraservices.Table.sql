USE [sot]
GO
/****** Object:  Table [dbo].[docspecextraservices]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[docspecextraservices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc_id] [int] NULL,
	[object_id] [int] NULL,
	[service_id] [int] NULL,
	[amount] [float] NULL,
	[price] [float] NULL,
	[summa] [float] NULL,
 CONSTRAINT [PK_docspecextraservices] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
