USE [sot]
GO
/****** Object:  Table [dbo].[passes]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[passes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[isprinted] [bit] NULL,
	[isabolished] [bit] NULL,
	[pass_type] [int] NULL,
	[pass_date] [datetime] NULL CONSTRAINT [DF_passes_pass_date]  DEFAULT (getdate()),
	[pass_number] [varchar](50) NULL,
	[payer_id] [int] NULL,
	[dealer_id] [int] NULL,
	[car_data] [varchar](50) NULL,
	[car_number] [varchar](50) NULL,
	[driver_id] [int] NULL,
	[attorney] [varchar](250) NULL,
	[onedaypass_term_start] [datetime] NULL CONSTRAINT [DF_passes_onedaypass_term_start]  DEFAULT (getdate()),
	[onedaypass_term_end] [datetime] NULL,
	[onedaypass_paytype] [int] NULL,
	[base_doc_id] [int] NULL,
	[user_id] [int] NULL,
	[date_created] [datetime] NULL,
 CONSTRAINT [PK_passes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[passes]  WITH CHECK ADD  CONSTRAINT [FK_passes_counteragents] FOREIGN KEY([payer_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[passes] CHECK CONSTRAINT [FK_passes_counteragents]
GO
ALTER TABLE [dbo].[passes]  WITH CHECK ADD  CONSTRAINT [FK_passes_counteragents1] FOREIGN KEY([dealer_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[passes] CHECK CONSTRAINT [FK_passes_counteragents1]
GO
