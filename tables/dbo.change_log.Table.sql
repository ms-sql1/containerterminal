USE [sot]
GO
/****** Object:  Table [dbo].[change_log]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[change_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[table_name] [varchar](250) NULL,
	[table_id] [int] NULL,
	[user_id] [int] NULL,
	[event_type] [varchar](20) NULL,
	[event_datetime] [datetime] NULL,
 CONSTRAINT [PK_change_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[change_log]  WITH CHECK ADD  CONSTRAINT [FK_change_log_users] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
ALTER TABLE [dbo].[change_log] CHECK CONSTRAINT [FK_change_log_users]
GO
