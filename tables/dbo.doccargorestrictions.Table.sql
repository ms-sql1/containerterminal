USE [sot]
GO
/****** Object:  Table [dbo].[doccargorestrictions]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doccargorestrictions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc_id] [int] NULL,
	[isactive] [bit] NULL,
	[type_id] [int] NULL,
	[datestart] [datetime] NULL,
	[dateend] [datetime] NULL,
	[base] [varchar](50) NULL,
	[note] [varchar](850) NULL,
	[task_id] [int] NULL,
	[object_id] [int] NULL,
 CONSTRAINT [PK_doccargorestrictions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
