USE [sot]
GO
/****** Object:  Table [dbo].[docorder]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docorder](
	[id] [int] NOT NULL,
	[usedproc] [float] NULL,
	[ldproc] [float] NULL,
	[uplproc] [float] NULL,
	[forwarder_id] [int] NULL,
	[consignee_id] [int] NULL,
	[direction_id] [int] NULL,
	[station_id] [int] NULL,
	[endpoint_consignee_id] [int] NULL,
	[consignee_address] [varchar](450) NULL,
	[consignee_contact] [varchar](250) NULL,
	[payer_id] [int] NULL,
	[contract_id] [int] NULL,
	[els_payer] [int] NULL,
	[plan_date] [datetime] NULL,
	[dlv_type_id] [int] NULL,
	[begin_date] [datetime] NULL,
	[end_date] [datetime] NULL,
	[shippingoption_id] [int] NULL,
	[extra_services] [varbinary](max) NULL,
	[tarif_type_id] [int] NULL,
	[train_num] [varchar](50) NULL,
	[note] [varchar](450) NULL,
 CONSTRAINT [PK_docorder] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[docorder]  WITH CHECK ADD  CONSTRAINT [FK_docorder_counteragents] FOREIGN KEY([endpoint_consignee_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[docorder] CHECK CONSTRAINT [FK_docorder_counteragents]
GO
ALTER TABLE [dbo].[docorder]  WITH CHECK ADD  CONSTRAINT [FK_docorder_deliverytypes] FOREIGN KEY([dlv_type_id])
REFERENCES [dbo].[deliverytypes] ([id])
GO
ALTER TABLE [dbo].[docorder] CHECK CONSTRAINT [FK_docorder_deliverytypes]
GO
ALTER TABLE [dbo].[docorder]  WITH CHECK ADD  CONSTRAINT [FK_docorder_directions] FOREIGN KEY([direction_id])
REFERENCES [dbo].[directions] ([id])
GO
ALTER TABLE [dbo].[docorder] CHECK CONSTRAINT [FK_docorder_directions]
GO
ALTER TABLE [dbo].[docorder]  WITH CHECK ADD  CONSTRAINT [FK_docorder_stations] FOREIGN KEY([station_id])
REFERENCES [dbo].[stations] ([id])
GO
ALTER TABLE [dbo].[docorder] CHECK CONSTRAINT [FK_docorder_stations]
GO
ALTER TABLE [dbo].[docorder]  WITH CHECK ADD  CONSTRAINT [FK_docorder_tariftypes] FOREIGN KEY([tarif_type_id])
REFERENCES [dbo].[tariftypes] ([id])
GO
ALTER TABLE [dbo].[docorder] CHECK CONSTRAINT [FK_docorder_tariftypes]
GO
