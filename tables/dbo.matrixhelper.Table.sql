USE [sot]
GO
/****** Object:  Table [dbo].[matrixhelper]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[matrixhelper](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_start] [datetime] NULL,
	[container_id] [int] NULL,
	[operation] [varchar](20) NULL,
	[operation_type] [int] NULL,
	[matrix2container_id] [int] NULL,
	[loaded_mark] [varchar](20) NULL,
	[isempty] [bit] NULL,
	[state_mark] [varchar](4) NULL,
	[formalstate_mark] [varchar](4) NULL,
	[loadplanned] [varchar](1) NULL,
	[nextstate_mark] [varchar](4) NULL,
	[isdefective] [varchar](4) NULL,
	[ownmark] [varchar](4) NULL,
	[iscompleted] [bit] NULL,
 CONSTRAINT [PK_matrixhelper] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
