USE [sot]
GO
/****** Object:  Table [dbo].[services]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[services](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parent_id] [int] NULL,
	[servicekind_id] [int] NULL,
	[datetime_created] [datetime] NULL,
	[doc_base_id] [int] NULL,
	[doc_invoice_id] [int] NULL,
	[responsible_person_id] [int] NULL,
	[service_summ] [float] NULL,
	[user_created_id] [int] NULL,
 CONSTRAINT [PK_services_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
