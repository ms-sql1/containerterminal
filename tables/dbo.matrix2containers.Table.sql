USE [sot]
GO
/****** Object:  Table [dbo].[matrix2containers]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[matrix2containers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sector_id] [int] NULL,
	[row_id] [int] NULL,
	[stack_id] [int] NULL,
	[cell_num] [int] NULL,
	[level_num] [int] NULL,
	[container_id] [int] NULL,
 CONSTRAINT [PK_matrix2containers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
