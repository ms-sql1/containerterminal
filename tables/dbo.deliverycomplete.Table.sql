USE [sot]
GO
/****** Object:  Table [dbo].[deliverycomplete]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[deliverycomplete](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dlv_type_id] [int] NULL,
	[objecttype_id] [int] NULL,
	[object_state_id] [int] NULL,
	[link_type] [int] NULL,
 CONSTRAINT [PK_deliverycomplete] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
