USE [sot]
GO
/****** Object:  Table [dbo].[doctemplates]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doctemplates](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doctype_id] [int] NULL,
	[title] [varchar](50) NULL,
	[template] [varchar](450) NULL,
	[sql_text] [varchar](max) NULL,
	[group_field] [varchar](50) NULL,
 CONSTRAINT [PK_doctemplates] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
