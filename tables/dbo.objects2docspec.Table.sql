USE [sot]
GO
/****** Object:  Table [dbo].[objects2docspec]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[objects2docspec](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[task_id] [int] NULL,
	[doc_id] [int] NULL,
	[object_id] [int] NULL,
	[linked_object_id] [int] NULL,
	[date_created] [datetime] NULL CONSTRAINT [DF_objects2docshistory_date_created]  DEFAULT (getdate()),
 CONSTRAINT [PK_objects2docshistory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[objects2docspec]  WITH CHECK ADD  CONSTRAINT [FK_objects2docspec_objects] FOREIGN KEY([object_id])
REFERENCES [dbo].[objects] ([id])
GO
ALTER TABLE [dbo].[objects2docspec] CHECK CONSTRAINT [FK_objects2docspec_objects]
GO
