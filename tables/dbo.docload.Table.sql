USE [sot]
GO
/****** Object:  Table [dbo].[docload]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docload](
	[id] [int] NOT NULL,
	[joint_id] [int] NULL,
	[order_num] [int] NULL,
	[container_id] [int] NULL,
	[task_id] [int] NULL,
	[orderspec_id] [int] NULL,
	[isfreeload] [bit] NULL,
	[kind_id] [int] NULL,
	[owner_id] [int] NULL,
	[station_id] [int] NULL,
	[note] [varchar](250) NULL,
	[mtukind_id] [int] NULL,
	[picturekind_id] [int] NULL,
	[dispatch_numbers] [varchar](50) NULL,
	[receipt_number] [varchar](50) NULL,
	[receipt_summa] [float] NULL,
	[temp_move_sign] [int] NULL,
	[fulfilled] [varchar](1) NULL,
 CONSTRAINT [PK_docload] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[docload]  WITH CHECK ADD  CONSTRAINT [FK_docload_containers] FOREIGN KEY([container_id])
REFERENCES [dbo].[containers] ([id])
GO
ALTER TABLE [dbo].[docload] CHECK CONSTRAINT [FK_docload_containers]
GO
