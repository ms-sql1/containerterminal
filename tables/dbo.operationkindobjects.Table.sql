USE [sot]
GO
/****** Object:  Table [dbo].[operationkindobjects]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[operationkindobjects](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[operationkind_id] [int] NULL,
	[object_type] [varchar](50) NULL,
	[relation] [int] NULL,
	[update_type] [int] NULL,
	[objectstate_id] [int] NULL,
	[send_mail_consignee] [bit] NULL,
	[mail_template] [varchar](max) NULL,
 CONSTRAINT [PK_operationkindobjects] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
