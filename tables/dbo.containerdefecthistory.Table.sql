USE [sot]
GO
/****** Object:  Table [dbo].[containerdefecthistory]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[containerdefecthistory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[container_id] [int] NULL,
	[isdefective] [bit] NULL,
	[register_date] [datetime] NULL,
	[register_doc_id] [int] NULL,
	[register_docspec_id] [int] NULL,
 CONSTRAINT [PK_containerdefecthistory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
