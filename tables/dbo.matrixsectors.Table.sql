USE [sot]
GO
/****** Object:  Table [dbo].[matrixsectors]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[matrixsectors](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NULL,
	[name] [varchar](250) NULL,
	[place_order] [int] NULL,
	[rows] [int] NULL,
	[stacks] [int] NULL,
	[levels] [int] NULL,
	[roworderreverse] [bit] NULL,
	[levelorderreverse] [bit] NULL,
	[stackorderreverse] [bit] NULL,
	[isactive] [bit] NULL,
 CONSTRAINT [PK_matrixsectors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
