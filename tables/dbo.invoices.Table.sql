USE [sot]
GO
/****** Object:  Table [dbo].[invoices]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[invoices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc_num] [nchar](10) NULL,
	[doc_date] [datetime] NULL,
	[maindata_id] [int] NULL,
	[summa] [float] NULL
) ON [PRIMARY]

GO
