USE [sot]
GO
/****** Object:  Table [dbo].[docdispatch]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docdispatch](
	[id] [int] NOT NULL,
	[direction_id] [int] NULL,
	[station_id] [int] NULL,
	[dispatchkind_id] [int] NULL,
	[train_num] [varchar](50) NULL,
	[dispatch_number] [varchar](50) NULL,
	[date_notification] [datetime] NULL,
	[date_serve] [datetime] NULL,
	[sheet_serve] [varchar](50) NULL,
	[date_remove] [datetime] NULL,
	[sheet_remove] [varchar](50) NULL,
 CONSTRAINT [PK_docdispatch] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
