USE [sot]
GO
/****** Object:  Table [dbo].[docinspect]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docinspect](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc_date] [datetime] NULL,
	[doc_number] [varchar](50) NULL,
	[fact_datetime] [datetime] NULL,
	[person_id] [int] NULL,
	[objecttype] [varchar](50) NULL,
	[isconfirmed] [int] NULL,
	[date_confirm] [datetime] NULL,
 CONSTRAINT [PK_docinspecheads] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
