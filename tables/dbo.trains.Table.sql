USE [sot]
GO
/****** Object:  Table [dbo].[trains]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[trains](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tnum] [varchar](50) NOT NULL,
	[note] [varchar](450) NULL,
	[date_create]  AS (getdate()),
 CONSTRAINT [PK_trains] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
