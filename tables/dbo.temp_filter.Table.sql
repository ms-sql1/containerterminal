USE [sot]
GO
/****** Object:  Table [dbo].[temp_filter]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_filter](
	[userid] [int] NULL,
	[doctypes] [varchar](100) NULL,
	[c] [varchar](50) NULL,
	[v] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
