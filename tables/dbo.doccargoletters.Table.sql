USE [sot]
GO
/****** Object:  Table [dbo].[doccargoletters]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doccargoletters](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc_id] [int] NULL,
	[dateregister] [datetime] NULL,
	[sender_id] [int] NULL,
	[text] [varchar](2000) NULL,
	[task_id] [int] NULL,
	[object_id] [int] NULL,
 CONSTRAINT [PK_doccargoletters] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
