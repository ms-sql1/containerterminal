USE [sot]
GO
/****** Object:  Table [dbo].[form_columns]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[form_columns](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[form_id] [int] NULL,
	[field_name] [varchar](250) NULL,
	[column_title] [varchar](450) NULL,
	[show_in_grid] [bit] NULL,
	[show_in_form] [bit] NULL,
	[order_num] [int] NULL,
 CONSTRAINT [PK_form_columns] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
