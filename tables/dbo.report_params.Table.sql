USE [sot]
GO
/****** Object:  Table [dbo].[report_params]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[report_params](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [int] NULL,
	[param_name] [varchar](50) NULL,
	[param_title] [varchar](250) NULL,
	[param_type] [varchar](50) NULL,
	[param_field_length] [int] NULL,
	[param_decimal_length] [int] NULL,
	[isobligatory] [bit] NULL,
	[param_default_expression] [varchar](450) NULL,
	[param_sql] [varchar](850) NULL,
	[param_key] [varchar](50) NULL,
	[param_display] [varchar](50) NULL,
	[param_order] [int] NULL,
 CONSTRAINT [PK_report_params] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
