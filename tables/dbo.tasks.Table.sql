USE [sot]
GO
/****** Object:  Table [dbo].[tasks]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tasks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dlv_type_id] [int] NULL,
	[start_date] [datetime] NULL,
	[plan_date] [datetime] NULL,
	[fact_date] [datetime] NULL,
	[forwarder_id] [int] NULL,
	[consignee_id] [int] NULL,
	[station_id] [int] NULL,
	[payer_id] [int] NULL,
	[object_id] [int] NULL,
	[basedoc_id] [int] NULL,
	[basedoc_number] [varchar](50) NULL,
	[basedoc_date] [datetime] NULL,
	[alert_date] [datetime] NULL,
	[is_completed] [bit] NULL,
 CONSTRAINT [PK_tasks] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tasks]  WITH CHECK ADD  CONSTRAINT [FK_tasks_counteragents] FOREIGN KEY([forwarder_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[tasks] CHECK CONSTRAINT [FK_tasks_counteragents]
GO
ALTER TABLE [dbo].[tasks]  WITH CHECK ADD  CONSTRAINT [FK_tasks_counteragents1] FOREIGN KEY([consignee_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[tasks] CHECK CONSTRAINT [FK_tasks_counteragents1]
GO
ALTER TABLE [dbo].[tasks]  WITH CHECK ADD  CONSTRAINT [FK_tasks_counteragents2] FOREIGN KEY([payer_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[tasks] CHECK CONSTRAINT [FK_tasks_counteragents2]
GO
ALTER TABLE [dbo].[tasks]  WITH CHECK ADD  CONSTRAINT [FK_tasks_deliverytypes] FOREIGN KEY([dlv_type_id])
REFERENCES [dbo].[deliverytypes] ([id])
GO
ALTER TABLE [dbo].[tasks] CHECK CONSTRAINT [FK_tasks_deliverytypes]
GO
ALTER TABLE [dbo].[tasks]  WITH CHECK ADD  CONSTRAINT [FK_tasks_objects] FOREIGN KEY([object_id])
REFERENCES [dbo].[objects] ([id])
GO
ALTER TABLE [dbo].[tasks] CHECK CONSTRAINT [FK_tasks_objects]
GO
ALTER TABLE [dbo].[tasks]  WITH CHECK ADD  CONSTRAINT [FK_tasks_stations] FOREIGN KEY([station_id])
REFERENCES [dbo].[stations] ([id])
GO
ALTER TABLE [dbo].[tasks] CHECK CONSTRAINT [FK_tasks_stations]
GO
