USE [sot]
GO
/****** Object:  Table [dbo].[event_log]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[event_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[task_id] [int] NULL,
	[restriction_id] [int] NULL,
	[alert_que_guid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_event_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
