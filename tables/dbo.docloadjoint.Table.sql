USE [sot]
GO
/****** Object:  Table [dbo].[docloadjoint]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docloadjoint](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[folder_id] [int] NULL,
	[carriage_id] [int] NULL,
	[load_plan_id] [int] NULL,
	[path_id] [int] NULL,
	[isdeleted] [bit] NULL,
	[order_on_path] [int] NULL,
	[mark] [varchar](80) NULL,
	[train_num] [varchar](50) NULL,
	[train_num_source] [varchar](50) NULL,
	[note] [varchar](450) NULL,
	[isactive] [bit] NULL,
 CONSTRAINT [PK_docloadknot] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[docloadjoint]  WITH CHECK ADD  CONSTRAINT [FK_docloadjoint_loadplan] FOREIGN KEY([load_plan_id])
REFERENCES [dbo].[loadplan] ([id])
GO
ALTER TABLE [dbo].[docloadjoint] CHECK CONSTRAINT [FK_docloadjoint_loadplan]
GO
ALTER TABLE [dbo].[docloadjoint]  WITH CHECK ADD  CONSTRAINT [FK_docloadjoint_paths] FOREIGN KEY([path_id])
REFERENCES [dbo].[paths] ([id])
GO
ALTER TABLE [dbo].[docloadjoint] CHECK CONSTRAINT [FK_docloadjoint_paths]
GO
