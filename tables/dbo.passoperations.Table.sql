USE [sot]
GO
/****** Object:  Table [dbo].[passoperations]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[passoperations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parent_id] [int] NULL,
	[higher_id] [int] NULL,
	[passoperation_type] [int] NULL,
	[note] [varchar](450) NULL,
	[passoperation_id] [int] NULL,
	[container_id] [int] NULL,
	[container_num] [varchar](50) NULL,
	[container_kind_id] [int] NULL,
	[container_owner_id] [int] NULL,
	[seal_number] [varchar](50) NULL,
	[isempty] [bit] NULL,
	[task_id] [int] NULL,
	[state] [int] NULL,
	[childdoc_id] [int] NULL,
	[container_digits] [varchar](10) NULL,
 CONSTRAINT [PK_passoperations] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
