USE [sot]
GO
/****** Object:  Table [dbo].[docroute2objectstates]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docroute2objectstates](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[docroute_id] [int] NULL,
	[objecttype_id] [int] NULL,
	[start_object_state_id] [int] NULL,
	[start_real_state_id] [int] NULL,
	[end_object_state_id] [int] NULL,
	[start_link_type] [int] NULL,
	[end_link_type] [int] NULL,
	[default_isempty] [bit] NULL,
	[default_seal_number] [varchar](50) NULL,
	[fesco_sign] [varchar](50) NULL,
 CONSTRAINT [PK_docroute2objectstates] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
