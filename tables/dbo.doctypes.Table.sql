USE [sot]
GO
/****** Object:  Table [dbo].[doctypes]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doctypes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NULL,
	[name] [varchar](250) NULL,
	[confirm_procedure] [varchar](50) NULL,
	[system_section] [varchar](50) NULL,
	[global_section] [varchar](50) NULL,
	[dlv_type_id] [int] NULL,
	[edit_confirmed] [bit] NULL CONSTRAINT [DF_doctypes_edit_confirmed]  DEFAULT ((0)),
	[roles_ids] [varchar](250) NULL,
	[avoid_object_update] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
