USE [sot]
GO
/****** Object:  Table [dbo].[StartChecks]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StartChecks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Check_title] [varchar](250) NULL,
	[Check_Query] [varchar](max) NULL,
	[Column_Widths] [varchar](250) NULL,
	[Column_Titles] [varchar](4000) NULL,
	[Object_Filter_Column] [varchar](80) NULL,
	[System_Section_Destiny] [varchar](80) NULL,
 CONSTRAINT [PK_StartChecks] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
