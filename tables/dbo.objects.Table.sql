USE [sot]
GO
/****** Object:  Table [dbo].[objects]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[objects](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[start_datetime] [datetime] NULL,
	[object_type] [varchar](50) NULL,
	[state_id] [int] NULL,
	[linked_object_id] [int] NULL,
	[address_id] [int] NULL,
	[owner_id] [int] NULL,
	[current_task_id] [int] NULL,
	[start_state_id] [int] NULL,
	[real_state_id] [int] NULL,
 CONSTRAINT [PK_objects] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[objects]  WITH CHECK ADD  CONSTRAINT [FK_objects_counteragents] FOREIGN KEY([owner_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[objects] CHECK CONSTRAINT [FK_objects_counteragents]
GO
ALTER TABLE [dbo].[objects]  WITH CHECK ADD  CONSTRAINT [FK_objects_counteragents1] FOREIGN KEY([owner_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[objects] CHECK CONSTRAINT [FK_objects_counteragents1]
GO
