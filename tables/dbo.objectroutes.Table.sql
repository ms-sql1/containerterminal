USE [sot]
GO
/****** Object:  Table [dbo].[objectroutes]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[objectroutes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[operationobjectkind_id] [int] NULL,
	[state_id] [int] NULL,
 CONSTRAINT [PK_objectroutes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
