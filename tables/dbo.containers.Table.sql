USE [sot]
GO
/****** Object:  Table [dbo].[containers]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[containers](
	[id] [int] NOT NULL,
	[cnum] [varchar](50) NOT NULL,
	[kind_id] [int] NULL,
	[size] [int] NULL,
	[container_weight] [float] NULL,
	[carrying] [float] NULL,
	[date_made] [datetime] NULL,
	[techcond_id] [int] NULL,
	[date_inspection] [datetime] NULL,
	[isdefective] [bit] NULL CONSTRAINT [DF_containers_isdefective]  DEFAULT ((0)),
	[isnotready] [bit] NULL CONSTRAINT [DF_containers_isnotready]  DEFAULT ((0)),
	[date_stiker] [datetime] NULL,
	[acep] [bit] NULL,
	[picture] [bit] NULL CONSTRAINT [DF_containers_picture]  DEFAULT ((0)),
	[note] [varchar](850) NULL,
	[current_address_id] [int] NULL,
	[date_income] [datetime] NULL,
 CONSTRAINT [PK_containers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[containers]  WITH CHECK ADD  CONSTRAINT [FK_containers_containerkinds] FOREIGN KEY([kind_id])
REFERENCES [dbo].[containerkinds] ([id])
GO
ALTER TABLE [dbo].[containers] CHECK CONSTRAINT [FK_containers_containerkinds]
GO
