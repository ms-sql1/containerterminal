USE [sot]
GO
/****** Object:  Table [dbo].[ExternalCargoSheet]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExternalCargoSheet](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](50) NULL,
	[filename] [varchar](250) NULL,
	[conttype] [varchar](50) NULL,
	[contnumber] [varchar](50) NULL,
	[contowner] [varchar](450) NULL,
	[weight] [varchar](50) NULL,
	[datesent] [varchar](50) NULL,
	[carowner] [varchar](450) NULL,
	[carnumber] [varchar](250) NULL,
	[seal] [varchar](250) NULL,
	[descr] [varchar](max) NULL,
	[guard] [varchar](250) NULL,
 CONSTRAINT [PK_ExternalCargoSheet] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
