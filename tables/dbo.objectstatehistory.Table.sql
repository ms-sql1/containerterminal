USE [sot]
GO
/****** Object:  Table [dbo].[objectstatehistory]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[objectstatehistory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[object_id] [int] NULL,
	[object_state_id] [int] NULL,
	[linked_object_id] [int] NULL,
	[docspec_id] [int] NULL,
	[doc_id] [int] NULL,
	[task_id] [int] NULL,
	[date_history] [datetime] NULL,
	[date_factexecution] [datetime] NULL,
	[is_completed] [bit] NULL,
 CONSTRAINT [PK_objectstatehistory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[objectstatehistory]  WITH CHECK ADD  CONSTRAINT [FK_objectstatehistory_objects] FOREIGN KEY([object_id])
REFERENCES [dbo].[objects] ([id])
GO
ALTER TABLE [dbo].[objectstatehistory] CHECK CONSTRAINT [FK_objectstatehistory_objects]
GO
ALTER TABLE [dbo].[objectstatehistory]  WITH CHECK ADD  CONSTRAINT [FK_objectstatehistory_objectstatekinds] FOREIGN KEY([object_state_id])
REFERENCES [dbo].[objectstatekinds] ([id])
GO
ALTER TABLE [dbo].[objectstatehistory] CHECK CONSTRAINT [FK_objectstatehistory_objectstatekinds]
GO
