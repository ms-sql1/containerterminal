USE [sot]
GO
/****** Object:  Table [dbo].[shippingoptionrules]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[shippingoptionrules](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[payer_code] [varchar](50) NULL,
	[direction_code] [varchar](80) NULL,
	[station_code] [varchar](80) NULL,
	[extra_services] [varchar](2000) NULL,
	[exclude_sign] [bit] NULL,
	[shippingoption_id] [int] NULL,
	[priority] [int] NULL,
 CONSTRAINT [PK_shippingoptionrules] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
