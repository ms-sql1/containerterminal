USE [sot]
GO
/****** Object:  Table [dbo].[roles]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](150) NULL,
	[readonly_sections] [varchar](850) NULL,
	[confirmrestrict_sections] [varchar](850) NULL,
	[unavalable_sections] [varchar](850) NULL,
	[allowdelete_sections] [varchar](850) NULL,
	[allow_edit_price] [bit] NULL,
	[restrict_edit_dicts] [bit] NULL,
	[restrict_edit_objects] [bit] NULL,
 CONSTRAINT [PK_roles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
