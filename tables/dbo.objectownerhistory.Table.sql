USE [sot]
GO
/****** Object:  Table [dbo].[objectownerhistory]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[objectownerhistory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[object_id] [int] NULL,
	[owner_id] [int] NULL,
	[date_change] [datetime] NULL,
 CONSTRAINT [PK_carriagehistory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[objectownerhistory]  WITH CHECK ADD  CONSTRAINT [FK_objectownerhistory_counteragents] FOREIGN KEY([owner_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[objectownerhistory] CHECK CONSTRAINT [FK_objectownerhistory_counteragents]
GO
ALTER TABLE [dbo].[objectownerhistory]  WITH CHECK ADD  CONSTRAINT [FK_objectownerhistory_counteragents1] FOREIGN KEY([owner_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[objectownerhistory] CHECK CONSTRAINT [FK_objectownerhistory_counteragents1]
GO
