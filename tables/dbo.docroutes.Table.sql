USE [sot]
GO
/****** Object:  Table [dbo].[docroutes]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docroutes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dlv_type_id] [int] NULL,
	[end_doctype_id] [int] NULL,
	[method_name] [varchar](250) NULL,
	[isrestricted] [bit] NULL CONSTRAINT [DF_docroutes_isrestricted]  DEFAULT ((0)),
	[exclude_states_ids] [varchar](50) NULL,
	[roles_ids] [varchar](250) NULL,
	[pref_states_ids] [varchar](50) NULL,
	[copy_prev_state] [bit] NULL,
	[create_new_object] [bit] NULL,
 CONSTRAINT [PK_documentroutes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
