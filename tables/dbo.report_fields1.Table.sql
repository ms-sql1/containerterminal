USE [sot]
GO
/****** Object:  Table [dbo].[report_fields1]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[report_fields1](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [nchar](10) NULL,
	[field_name] [varchar](150) NULL,
	[field_caption] [varchar](450) NULL,
	[field_order] [int] NULL,
	[field_width] [int] NULL,
	[field_align] [int] NULL,
	[is_total] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
