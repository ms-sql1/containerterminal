USE [sot]
GO
/****** Object:  Table [dbo].[doccargoservices]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doccargoservices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc_id] [nchar](10) NULL,
	[doc_number] [varchar](50) NULL,
	[doc_date] [datetime] NULL,
	[service_id] [int] NULL,
	[customer_id] [int] NULL,
	[tarif_id] [int] NULL,
	[summa] [float] NULL,
	[task_id] [int] NULL,
	[object_id] [int] NULL,
 CONSTRAINT [PK_doccargoservices] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
