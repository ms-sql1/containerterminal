USE [sot]
GO
/****** Object:  Table [dbo].[objectstatekinds]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[objectstatekinds](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NULL,
	[name] [varchar](450) NULL,
	[inplacesign] [bit] NULL,
	[inworksign] [bit] NULL,
	[isformalsign] [bit] NULL,
	[allow_repeat] [bit] NULL,
	[stop_sign] [bit] NULL,
	[mark] [varchar](4) NULL,
 CONSTRAINT [PK_objectstatekinds] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[objectstatekinds]  WITH CHECK ADD  CONSTRAINT [FK_objectstatekinds_objectstatekinds] FOREIGN KEY([id])
REFERENCES [dbo].[objectstatekinds] ([id])
GO
ALTER TABLE [dbo].[objectstatekinds] CHECK CONSTRAINT [FK_objectstatekinds_objectstatekinds]
GO
