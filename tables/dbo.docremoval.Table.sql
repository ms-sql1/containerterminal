USE [sot]
GO
/****** Object:  Table [dbo].[docremoval]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docremoval](
	[id] [int] NOT NULL,
	[dlv_type_id] [int] NULL,
	[dealer_id] [int] NULL,
	[attorney] [varchar](150) NULL,
	[driver_name] [varchar](150) NULL,
	[driver_id] [int] NULL,
	[recept_org_name] [varchar](250) NULL,
	[recept_datetime] [datetime] NULL,
	[recept_address] [varchar](450) NULL,
	[recept_contact] [varchar](250) NULL,
	[delivery_org_name] [varchar](250) NULL,
	[delivery_datetime] [datetime] NULL,
	[arrival_datetime_fact] [datetime] NULL,
	[go_datetime_fact] [datetime] NULL,
	[delivery_address] [varchar](450) NULL,
	[delivery_contact] [varchar](250) NULL,
	[return_datetime] [datetime] NULL,
	[return_address] [varchar](450) NULL,
	[return_contact] [varchar](250) NULL,
	[plan_date] [datetime] NULL,
	[fact_date] [datetime] NULL,
	[car_data] [varchar](150) NULL,
	[car_number] [varchar](50) NULL,
	[note] [varchar](450) NULL,
	[basedoc_type] [varchar](50) NULL,
	[basedoc_number] [varchar](50) NULL,
	[movers_amount] [int] NULL,
	[inspector_id] [int] NULL,
	[extra_place_sign] [bit] NULL,
	[extra_datetime] [datetime] NULL,
	[extra_address] [varchar](450) NULL,
	[extra_contact] [varchar](450) NULL,
	[extra_go_datetime_fact] [datetime] NULL,
	[extra_arrival_datetime_fact] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
