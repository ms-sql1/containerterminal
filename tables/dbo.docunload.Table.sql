USE [sot]
GO
/****** Object:  Table [dbo].[docunload]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docunload](
	[id] [int] NULL,
	[doc_base] [varchar](450) NULL,
	[person_id] [int] NULL,
	[plan_date] [datetime] NULL,
	[fact_date] [datetime] NULL,
	[alert_date] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
