USE [sot]
GO
/****** Object:  Table [dbo].[raport_settings_details]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[raport_settings_details](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[head_id] [int] NULL,
	[standard_time] [datetime] NULL,
	[operation_code] [varchar](50) NULL,
	[event_code] [varchar](50) NULL,
	[depart_code] [varchar](50) NULL,
	[dest_code] [varchar](50) NULL,
	[transp_code] [varchar](50) NULL,
	[instruction_code] [varchar](50) NULL,
 CONSTRAINT [PK_raport_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
