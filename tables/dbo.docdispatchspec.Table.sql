USE [sot]
GO
/****** Object:  Table [dbo].[docdispatchspec]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docdispatchspec](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc_id] [int] NOT NULL,
	[o2s_id] [int] NULL,
	[docload_id] [int] NULL,
	[container_id] [int] NULL,
	[cargo_id] [int] NULL,
	[carriage_id] [int] NULL,
	[mtukind_id] [int] NULL,
	[picturekind_id] [int] NULL,
	[dispatch_numbers] [varchar](450) NULL,
	[car_feed_date] [datetime] NULL,
	[car_paper_number] [varchar](50) NULL,
	[sheet_serve] [varchar](50) NULL,
	[date_serve] [datetime] NULL,
	[receipt_ready] [bit] NULL,
	[receipt_number] [varchar](50) NULL,
	[receipt_summa] [float] NULL,
 CONSTRAINT [PK_docdispatchspec] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[docdispatchspec]  WITH CHECK ADD  CONSTRAINT [FK_docdispatchspec_mtukinds] FOREIGN KEY([mtukind_id])
REFERENCES [dbo].[mtukinds] ([id])
GO
ALTER TABLE [dbo].[docdispatchspec] CHECK CONSTRAINT [FK_docdispatchspec_mtukinds]
GO
ALTER TABLE [dbo].[docdispatchspec]  WITH CHECK ADD  CONSTRAINT [FK_docdispatchspec_picturekinds] FOREIGN KEY([picturekind_id])
REFERENCES [dbo].[picturekinds] ([id])
GO
ALTER TABLE [dbo].[docdispatchspec] CHECK CONSTRAINT [FK_docdispatchspec_picturekinds]
GO
