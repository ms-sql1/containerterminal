USE [sot]
GO
/****** Object:  Table [dbo].[docroute2services]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[docroute2services](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[docroute_id] [int] NULL,
	[service_id] [int] NULL,
	[amount] [float] NULL,
	[price] [float] NULL,
 CONSTRAINT [PK_docroute2services] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
