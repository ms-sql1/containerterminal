USE [sot]
GO
/****** Object:  Table [dbo].[reports]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reports](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_name] [varchar](50) NULL,
	[report_title] [varchar](450) NULL,
	[catalog_id] [int] NULL,
	[group_fields] [varchar](2000) NULL,
	[sql_text] [varchar](max) NULL,
	[access_roles] [varchar](450) NULL,
	[tree_parent_id_name] [int] NULL,
	[tree_child_id_name] [int] NULL,
 CONSTRAINT [PK_reports] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
