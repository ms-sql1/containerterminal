USE [sot]
GO
/****** Object:  Table [dbo].[loadplan]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[loadplan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[plan_date] [datetime] NULL,
	[note] [varchar](850) NULL,
	[train_num] [varchar](50) NULL,
	[system_section] [varchar](50) NULL,
	[dispatch_kind_id] [int] NULL,
	[sheet_serve] [varchar](150) NULL,
 CONSTRAINT [PK_loadplan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[loadplan]  WITH CHECK ADD  CONSTRAINT [FK_loadplan_dispatchkinds] FOREIGN KEY([dispatch_kind_id])
REFERENCES [dbo].[dispatchkinds] ([id])
GO
ALTER TABLE [dbo].[loadplan] CHECK CONSTRAINT [FK_loadplan_dispatchkinds]
GO
