USE [sot]
GO
/****** Object:  Table [dbo].[externaldatalinks]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[externaldatalinks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_type] [varchar](50) NULL,
	[text_caption] [varchar](450) NULL,
	[data_id] [int] NULL,
	[data_caption] [varchar](50) NULL,
 CONSTRAINT [PK_externaldatalinks] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
