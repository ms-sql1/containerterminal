USE [sot]
GO
/****** Object:  Table [dbo].[billed]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[billed](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[container_id] [int] NULL,
	[task_id] [int] NULL,
	[invoice_amount] [float] NULL,
	[invoice_date] [datetime] NOT NULL,
	[invoice_number] [varchar](50) NOT NULL,
	[create_datetime] [datetime] NULL CONSTRAINT [DF_billed_create_datetime]  DEFAULT (getdate()),
	[user_id] [int] NULL,
 CONSTRAINT [PK_billed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
