USE [sot]
GO
/****** Object:  Table [dbo].[mtu2pictures]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mtu2pictures](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mtu_id] [int] NULL,
	[picture_id] [int] NULL,
 CONSTRAINT [PK_mtu2pictures] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
