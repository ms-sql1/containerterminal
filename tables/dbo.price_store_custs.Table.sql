USE [sot]
GO
/****** Object:  Table [dbo].[price_store_custs]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[price_store_custs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type_id] [int] NULL,
	[customer_id] [int] NULL,
	[date_change] [datetime] NULL,
	[price_default] [float] NULL,
 CONSTRAINT [PK_price_store_custs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[price_store_custs]  WITH CHECK ADD  CONSTRAINT [FK_price_store_custs_counteragents] FOREIGN KEY([customer_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[price_store_custs] CHECK CONSTRAINT [FK_price_store_custs_counteragents]
GO
