USE [sot]
GO
/****** Object:  Table [dbo].[matrixexcludes]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[matrixexcludes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sector_id] [int] NULL,
	[limit_type] [int] NULL,
	[row_start] [int] NULL,
	[row_end] [int] NULL,
	[stack_start] [int] NULL,
	[stack_end] [int] NULL,
	[levels] [int] NULL,
	[cells] [int] NULL,
 CONSTRAINT [PK_matrixexcludes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
