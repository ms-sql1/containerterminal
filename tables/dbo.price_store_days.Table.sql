USE [sot]
GO
/****** Object:  Table [dbo].[price_store_days]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[price_store_days](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pricecust_id] [int] NULL,
	[days] [int] NULL,
	[price] [float] NULL,
 CONSTRAINT [PK_price_store_days] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
