USE [sot]
GO
/****** Object:  Table [dbo].[users]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_login] [varchar](50) NULL,
	[user_name] [varchar](250) NULL,
	[password] [varchar](50) NULL,
	[user_role] [int] NULL,
	[isAdmin] [bit] NULL,
	[short_name] [varchar](50) NULL,
	[post_name] [varchar](250) NULL,
	[contact] [varchar](250) NULL,
	[roles_ids] [varchar](250) NULL,
	[isblocked] [bit] NULL,
	[IsSuperVisor] [bit] NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
