USE [sot]
GO
/****** Object:  Table [dbo].[service2prices]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[service2prices](
	[id] [int] NOT NULL,
	[service_id] [int] NOT NULL,
	[date_start] [datetime] NOT NULL,
	[price] [float] NULL
) ON [PRIMARY]

GO
