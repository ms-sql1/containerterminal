USE [sot]
GO
/****** Object:  Table [dbo].[emailtemplates]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[emailtemplates](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[section_code] [varchar](250) NULL,
	[greeting_template] [varchar](800) NULL,
	[single_body_template] [varchar](2000) NULL,
	[multi_body_template] [varchar](2000) NULL,
	[sign_template] [varchar](2000) NULL,
 CONSTRAINT [PK_email_templates] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
