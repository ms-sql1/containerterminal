USE [sot]
GO
/****** Object:  Table [dbo].[passoperationkinds]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[passoperationkinds](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NULL,
	[short_code] [varchar](50) NULL,
	[direction] [int] NULL,
	[name] [varchar](250) NULL,
	[end_doctype_id] [int] NULL,
	[system_section] [varchar](50) NULL,
	[end_dlv_type_id] [int] NULL,
 CONSTRAINT [PK_passoperationkinds] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
