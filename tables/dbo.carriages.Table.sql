USE [sot]
GO
/****** Object:  Table [dbo].[carriages]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[carriages](
	[id] [int] NOT NULL,
	[pnum] [varchar](50) NULL,
	[kind_id] [int] NULL,
	[techcond_id] [int] NULL,
	[isdefective] [bit] NOT NULL CONSTRAINT [DF_carriages_isdefective]  DEFAULT ((0)),
	[need_inspection] [bit] NULL,
	[date_inspection] [datetime] NULL,
	[date_next_repair] [datetime] NULL,
	[model_id] [int] NULL,
	[rotation_id] [int] NULL,
	[note] [varchar](450) NULL,
	[date_income] [datetime] NULL,
	[car_paper_number] [varchar](50) NULL,
	[train_num] [varchar](50) NULL,
	[current_load_id] [int] NULL,
 CONSTRAINT [PK_carriages] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[carriages]  WITH CHECK ADD  CONSTRAINT [FK_carriages_carriagekinds] FOREIGN KEY([kind_id])
REFERENCES [dbo].[carriagekinds] ([id])
GO
ALTER TABLE [dbo].[carriages] CHECK CONSTRAINT [FK_carriages_carriagekinds]
GO
ALTER TABLE [dbo].[carriages]  WITH CHECK ADD  CONSTRAINT [FK_carriages_carriagemodels] FOREIGN KEY([model_id])
REFERENCES [dbo].[carriagemodels] ([id])
GO
ALTER TABLE [dbo].[carriages] CHECK CONSTRAINT [FK_carriages_carriagemodels]
GO
ALTER TABLE [dbo].[carriages]  WITH CHECK ADD  CONSTRAINT [FK_carriages_techconditions] FOREIGN KEY([techcond_id])
REFERENCES [dbo].[techconditions] ([id])
GO
ALTER TABLE [dbo].[carriages] CHECK CONSTRAINT [FK_carriages_techconditions]
GO
