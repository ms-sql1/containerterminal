USE [sot]
GO
/****** Object:  Table [dbo].[report_fields]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[report_fields](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [nchar](10) NULL,
	[field_name] [varchar](150) NULL,
	[field_caption] [varchar](450) NULL,
	[field_order] [int] NULL,
	[field_width] [int] NULL,
	[field_align] [int] NULL,
	[is_total] [bit] NULL,
 CONSTRAINT [PK_report_fields] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
