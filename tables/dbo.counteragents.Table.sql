USE [sot]
GO
/****** Object:  Table [dbo].[counteragents]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[counteragents](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parent_id] [int] NULL,
	[folder_id] [int] NULL,
	[INN] [varchar](14) NULL,
	[code] [varchar](50) NULL,
	[name] [varchar](450) NOT NULL,
	[location] [varchar](2000) NULL,
	[chief] [varchar](150) NULL,
	[manager] [varchar](450) NULL,
	[email] [varchar](2000) NULL,
	[contacts] [varchar](450) NULL,
	[contract_num] [varchar](450) NULL,
	[contract_date] [datetime] NULL,
	[els_code] [varchar](50) NULL,
	[station_id] [int] NULL,
 CONSTRAINT [PK_customers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
