USE [sot]
GO
/****** Object:  Table [dbo].[operationroutes]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[operationroutes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[operationkind_id] [int] NULL,
	[state_id] [int] NULL,
 CONSTRAINT [PK_operationroutes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
