USE [sot]
GO
/****** Object:  Table [dbo].[emailaccounts]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[emailaccounts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[server_name] [varchar](50) NULL,
	[ssl_check] [bit] NULL,
	[port_number] [int] NULL,
	[email_sender] [varchar](250) NULL,
	[password] [varchar](50) NULL,
 CONSTRAINT [PK_emailaccounts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
