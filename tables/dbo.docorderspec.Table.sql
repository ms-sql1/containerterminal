USE [sot]
GO
/****** Object:  Table [dbo].[docorderspec]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docorderspec](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc_id] [int] NOT NULL,
	[o2s_id] [int] NULL,
	[container_id] [int] NULL,
	[container_num] [varchar](50) NULL,
	[container_kind_id] [int] NULL,
	[container_owner_id] [int] NULL,
	[cargo_id] [int] NULL,
	[seal_number] [varchar](50) NULL,
	[cargotype_id] [int] NULL,
	[isempty] [bit] NULL,
	[weight_cargo] [float] NULL,
	[weight_container] [float] NULL,
	[amount] [int] NULL,
	[used_amount] [int] NULL,
	[external_order_num] [varchar](50) NULL,
	[external_order_signed] [bit] NULL,
 CONSTRAINT [PK_docorderspec] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[docorderspec]  WITH CHECK ADD  CONSTRAINT [FK_docorderspec_counteragents] FOREIGN KEY([container_owner_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[docorderspec] CHECK CONSTRAINT [FK_docorderspec_counteragents]
GO
