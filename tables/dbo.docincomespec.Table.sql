USE [sot]
GO
/****** Object:  Table [dbo].[docincomespec]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[docincomespec](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc_id] [int] NOT NULL,
	[o2s_id] [int] NULL,
	[container_id] [int] NULL,
	[container_num] [varchar](50) NULL,
	[container_kind_id] [int] NULL,
	[container_owner_id] [int] NULL,
	[is_defective] [bit] NULL CONSTRAINT [DF_docincomespec_is_defective]  DEFAULT ((0)),
	[isnotready] [bit] NULL CONSTRAINT [DF_docincomespec_isnotready]  DEFAULT ((0)),
	[defect_note] [varchar](450) NULL,
	[container_weight] [float] NULL,
	[carrying] [float] NULL,
	[date_made] [datetime] NULL,
	[date_inspection] [datetime] NULL,
	[date_stiker] [datetime] NULL,
	[acep] [bit] NULL CONSTRAINT [DF_docincomespec_acep]  DEFAULT ((0)),
	[picture] [bit] NULL CONSTRAINT [DF_docincomespec_picture]  DEFAULT ((0)),
	[cargo_id] [int] NULL,
	[seal_number] [varchar](50) NULL,
	[cargotype_id] [int] NULL,
	[isempty] [bit] NULL CONSTRAINT [DF_docincomespec_isempty]  DEFAULT ((0)),
	[weight_cargo] [float] NULL,
	[weight_container] [float] NULL,
	[external_order_num] [varchar](50) NULL,
	[note] [varchar](250) NULL,
	[amount] [int] NULL,
	[address_id] [int] NULL,
	[weight_shift] [bit] NULL,
	[external_order_signed] [bit] NULL,
 CONSTRAINT [PK_docincomespec] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
