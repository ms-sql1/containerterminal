USE [sot]
GO
/****** Object:  Table [dbo].[alertque]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[alertque](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[counteragent_id] [int] NULL,
	[email] [varchar](2000) NULL,
	[message] [varchar](max) NULL,
	[date_created] [datetime] NULL CONSTRAINT [DF_alertque_date_created]  DEFAULT (getdate()),
	[issent] [bit] NULL,
	[date_sent] [datetime] NULL,
	[alert_que_guid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_alertque] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[alertque]  WITH CHECK ADD  CONSTRAINT [FK_alertque_counteragents] FOREIGN KEY([counteragent_id])
REFERENCES [dbo].[counteragents] ([id])
GO
ALTER TABLE [dbo].[alertque] CHECK CONSTRAINT [FK_alertque_counteragents]
GO
