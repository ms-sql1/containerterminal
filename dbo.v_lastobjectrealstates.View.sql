USE [sot]
GO
/****** Object:  View [dbo].[v_lastobjectrealstates]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[v_lastobjectrealstates] as
select * from objectstatehistory osk 
where id = (
select max(id) from objectstatehistory osk1 
where osk1.object_id = osk.object_id
and osk1.object_state_id 
in (select id from objectstatekinds sk where isnull(sk.isformalsign,0) = 0)
)

GO
