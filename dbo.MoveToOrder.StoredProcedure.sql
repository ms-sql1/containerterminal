USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[MoveToOrder]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MoveToOrder](@docid int, @selectguid varchar(40))
AS
BEGIN
	
	SET NOCOUNT ON;

	declare @e int, @dlvtypeold int, @dlvtypenew int, @dlvsectold varchar(50), @dlvsectnew varchar(50),
	@oldconf int, @newconf int;

	select @dlvtypeold = o.dlv_type_id, @dlvsectold = dt.global_section, @oldconf = isnull(d.isconfirmed,0)
	from docorder o, documents d, docorderspec os, deliverytypes dt, selectlist sl 
	where o.id = os.doc_id and d.id = o.id and o.dlv_type_id = dt.id
	and os.id = sl.id and sl.guid = @selectguid;

	select @dlvtypenew = o.dlv_type_id, @dlvsectnew = dt.global_section, @newconf = isnull(d.isconfirmed,0)
	from docorder o, deliverytypes dt, documents d
	where o.id = @docid and o.id = d.id and dt.id = o.dlv_type_id;

	if (@dlvsectnew <> @dlvsectold)
	begin
	  raiserror('Оба документа должны относиться к одинаковой операции.', 16, 1);
	  return;	
	end;

	if (@oldconf <> @newconf)
	begin
	  raiserror('Оба документа должны быть одинаково проведены или не проведены.', 16, 1);
	  return;	
	end;

	set @e = 0;

	select @e = 1 
	where exists (
		select 1 from docorderspec os, docload l, documents d, selectlist sl
		where l.orderspec_id = os.id and l.id = d.id and sl.id = os.id
		and /*isnull(d.isconfirmed,0) = 1 and*/ isnull(d.isabolished,0) = 0
		and sl.guid = @selectguid
		);

	if @e = 1
	begin
	  raiserror('Cреди выбранных контейнеров есть погруженные (или спланированные) контейнеры. Операция невозможна.', 16, 1);
	  return;	
	end;

	-- проверяем - а нет ли такого же контейнера в новом документе

	set @e = 0;

	select @e = 1 
	where exists (
		select 1 from docorderspec os where os.doc_id = @docid and
		(
			os.container_num in (select container_num from docorderspec os1, selectlist sl where os1.id = sl.id and sl.guid = @selectguid)
			or 
			os.container_id in (select container_id from docorderspec os1, selectlist sl where os1.id = sl.id and sl.guid = @selectguid)
		));

	if @e = 1
	begin
	  raiserror('В выбранной заявке уже есть такой контейнер(контейнеры).', 16, 1);
	  return;	
	end;

	-- переносим doc_id в objects2docspec

	-- контейнер
	update o2s set 
	doc_id = @docid 
	from objects2docspec o2s, docorderspec os, docorder o, selectlist sl
	where os.doc_id =  o2s.doc_id and os.container_id = o2s.object_id 
	and sl.id = os.id and sl.guid = @selectguid
	and o.id = os.doc_id;

	-- груз
	update o2s set 
	doc_id = @docid 
	from objects2docspec o2s, docorderspec os, docorder o, selectlist sl
	where os.doc_id =  o2s.doc_id and os.cargo_id = o2s.object_id 
	and sl.id = os.id and sl.guid = @selectguid
	and o.id = os.doc_id;


	-- переносим doc_id в objectstatehistory

	-- контейнер
	update osh
	set doc_id = @docid 
	from objectstatehistory osh, docorderspec os, docorder o, selectlist sl
	where os.doc_id =  osh.doc_id and os.container_id = osh.object_id 
	and sl.id = os.id and sl.guid = @selectguid
	and o.id = os.doc_id;

	-- груз
	update osh
	set doc_id = @docid 
	from objectstatehistory osh, docorderspec os, docorder o, selectlist sl
	where os.doc_id =  osh.doc_id and os.cargo_id = osh.object_id 
	and sl.id = os.id and sl.guid = @selectguid
	and o.id = os.doc_id;

	-- переносим саму спецификацию
	update docorderspec set doc_id = @docid where id in (select id from selectlist where guid = @selectguid);

	-- меняем задачу
	update t set 
	t.basedoc_id = @docid, 
	t.payer_id = o.payer_id,
	t.consignee_id = o.consignee_id,
	t.forwarder_id = o.forwarder_id,
	t.station_id = o.station_id
	from tasks t, objects2docspec o2s, docorderspec os, docorder o, selectlist sl
	where os.doc_id = o2s.doc_id and os.container_id = o2s.object_id 
	and t.id = o2s.task_id
	and sl.id = os.id and sl.guid = @selectguid
	and o.id = os.doc_id and o.id = @docid;

	-- пересчет процентов - тут все правильно

	update docorder set  
	usedproc = dbo.f_GetDocUsedProcs(id, '-16-'),
	ldproc = dbo.f_GetDocUsedProcs(id, '-12-'),
	uplproc = dbo.f_GetDocUsedProcs(id, '-13-')
	where id in (select id from selectlist where guid = @selectguid);

	update docorder set  
	usedproc = dbo.f_GetDocUsedProcs(id, '-16-'),
	ldproc = dbo.f_GetDocUsedProcs(id, '-12-'),
	uplproc = dbo.f_GetDocUsedProcs(id, '-13-')
	where id = @docid;

END

GO
