USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[CancelConfirmInspectDoc]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CancelConfirmInspectDoc](
@DocId int
) 
AS
BEGIN
	SET NOCOUNT ON;
	
	update docinspect set isconfirmed = 0, date_confirm = null where id = @DocId;
	
	
	-- вагоны
	
	update c set c.techcond_id = null, c.date_inspection = null, c.isdefective = 0
	from carriages c, docinspectspec s where s.object_id = c.id
	and s.parent_id = @DocId;
	
	update c set c.techcond_id = s.techcond_id, c.date_inspection = i.fact_datetime, 
	c.isdefective = t.isdefective
	from carriages c, docinspectspec s, docinspect i, techconditions t, 
	(select max(i.date_confirm) as maxdateconfirm, s.object_id
	from carriages c, docinspectspec s, docinspect i
	where s.object_id = c.id and s.parent_id = i.id
	and i.isconfirmed = 1 group by s.object_id) s1
	where s.object_id = c.id and s.parent_id = i.id and t.id = s.techcond_id
	and s.object_id = s1.object_id and i.date_confirm = s1.maxdateconfirm;
	
	-- контейнеры
	
	update c set c.techcond_id = null, c.date_inspection = null, c.isdefective = 0
	from containers c, docinspectspec s where s.object_id = c.id
	and s.parent_id = @DocId;
	
	update c set c.techcond_id = s.techcond_id, c.date_inspection = i.fact_datetime, 
	c.isdefective = t.isdefective
	from containers c, docinspectspec s, docinspect i, techconditions t, 
	(select max(i.date_confirm) as maxdateconfirm, s.object_id
	from carriages c, docinspectspec s, docinspect i
	where s.object_id = c.id and s.parent_id = i.id
	and i.isconfirmed = 1 group by s.object_id) s1
	where s.object_id = c.id and s.parent_id = i.id and t.id = s.techcond_id
	and s.object_id = s1.object_id and i.date_confirm = s1.maxdateconfirm;
	
	
	
END
GO
