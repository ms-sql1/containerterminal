USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[UpdateObjectState]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateObjectState] (@DocId int, @IsCancel bit)
AS
BEGIN
	SET NOCOUNT ON;
	
	if isnull(@IsCancel,0) = 0
	begin
	
		update o set 
		state_id = os.object_state_id,
		linked_object_id = os.linked_object_id,
		current_task_id = os.task_id
		from objects o 
		left outer join (
			select * from objectstatehistory os1
			where os1.date_factexecution = (
				select max(date_factexecution) from objectstatehistory os2 
				where os2.object_id = os1.object_id 
				and not os2.object_state_id is null
			)
			and not os1.object_state_id is null
			) os on (o.id = os.object_id)
		where o.id in (select object_id from objects2docspec ds where ds.doc_id = @DocId);

		update o set 
		real_state_id = os.object_state_id
		from objects o 
		left outer join (
			select os1.* from objectstatehistory os1, objectstatekinds sk1
			where os1.date_factexecution = (
				select max(date_factexecution) from objectstatehistory os2, objectstatekinds sk2 
				where os2.object_id = os1.object_id 
				and not os2.object_state_id is null
				and sk2.id = os2.object_state_id
				and isnull(sk2.isformalsign,0) = 0
			)
			and not os1.object_state_id is null
			and sk1.id = os1.object_state_id
			and isnull(sk1.isformalsign,0) = 0
			) os on (o.id = os.object_id)
		where o.id in (select object_id from objects2docspec ds where ds.doc_id = @DocId);

		
	end else
	begin

		update o set 
		state_id = isnull(os.object_state_id, o.start_state_id),
		linked_object_id = os.linked_object_id,
		current_task_id = os.task_id
		from objects o 
		left outer join (
			select * from objectstatehistory os1 where os1.doc_id <> @DocId 
			and os1.date_factexecution = (
				select max(date_factexecution) from objectstatehistory os2
				where os2.object_id = os1.object_id 
				and os2.Doc_id <> @DocId 
				and not os2.object_state_id is null
			)
			and not os1.object_state_id is null
			) os on (o.id = os.object_id)
		where o.id in (select object_id from objects2docspec ds where ds.doc_id = @DocId)
		and not exists (select 1 from objectstatehistory osh2 where osh2.doc_id = @DocId and osh2.object_id = o.id);
	

		update o set 
		real_state_id = os.object_state_id
		from objects o 
		left outer join (
			select os1.* from objectstatehistory os1, objectstatekinds sk1 where os1.doc_id <> @DocId 
			and os1.date_factexecution = (
				select max(date_factexecution) from objectstatehistory os2, objectstatekinds sk2  
				where os2.object_id = os1.object_id 
				and os2.Doc_id <> @DocId 
				and not os2.object_state_id is null
				and sk2.id = os2.object_state_id
				and isnull(sk2.isformalsign,0) = 0
			)
			and not os1.object_state_id is null
			and sk1.id = os1.object_state_id
			and isnull(sk1.isformalsign,0) = 0
			) os on (o.id = os.object_id)
		where o.id in (select object_id from objects2docspec ds where ds.doc_id = @DocId)
		and not exists (select 1 from objectstatehistory osh2 where osh2.doc_id = @DocId and osh2.object_id = o.id);

	end;
		
END

GO
