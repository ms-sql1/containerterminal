USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[GetContainerCustomer]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetContainerCustomer]
(
 @docid int, @objectid int
)
RETURNS varchar(250)
AS
BEGIN

	declare @taskid int, @dlvtypeid int, @section varchar(50), @customername varchar(250);
	
	select @taskid = t.id, @dlvtypeid = t.dlv_type_id 
	from objects2docspec o2s, tasks t
	where o2s.doc_id = @docid and o2s.object_id = @objectid
	and t.id = o2s.task_id
	
	select @customername = ISNULL(c.name, C.CODE) from counteragents c, tasks t
	where t.forwarder_id = c.id and t.id = @taskid and not t.dlv_type_id in (1,2)

	if isnull(@customername,'')='' 
	select @customername = isnull(c.name, c.code) from counteragents c, objects r 
	where r.owner_id = c.id and r.id = @objectid;

	return @customername;	

END

GO
