USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[Add2SelectList]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add2SelectList](@id integer, @guid varchar(40))
AS
BEGIN
	
	SET NOCOUNT ON;
	
	insert into selectlist (id, guid) values (@id, @guid);
	
END
GO
