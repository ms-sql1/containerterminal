USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[UpdateDeliveryType]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateDeliveryType](@TaskId int, @DlvTypeId int)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @e int;
	
	set @e = 0;
	-- проверяем, а вдруг уже доставка состоялась
	select @e = 1 from docremoval r, documents d, objects2docspec o2s
	where r.id = d.id and o2s.doc_id = d.id
	and o2s.task_id = @TaskId;
	
	if @e>0 
	begin
		raiserror('Уже создан Документ доставки. Изменение типа доставки невозможно.',16,-1);
		rollback transaction;
		return 0;
	end;
	
	set @e = 0;
	-- проверяем, а вдруг уже доставка состоялась
	select @e = 1 from docincome r, documents d, objects2docspec o2s
	where r.id = d.id and o2s.doc_id = d.id
	and o2s.task_id = @TaskId;
	
	if @e>0 
	begin
		raiserror('Уже создан документ Акт выдачи. Изменение типа доставки невозможно.',16,-1);
		rollback transaction;
		return 0;
	end;

	
	update tasks set dlv_type_id = @DlvTypeId where id = @TaskId;
	update doccargosheet set dlv_type_id = @DlvTypeId where id in
	(select doc_id from objects2docspec o2s where o2s.task_id = @TaskId);

END

GO
