USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[ChangeCarriage]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ChangeCarriage](@car1 int, @car2 int, @planid int, @setnew bit)
AS
BEGIN

	SET NOCOUNT ON;

	declare @path1 int, @path2 int;

	if isnull(@car2,0) = 0 
	begin
		raiserror('Не указан вагон для замены.', 16, 1);
		return;
	end;

	if @setnew = 0
	begin
	  
	  select @path1 = path_id from docloadjoint where carriage_id = @car1;
	  select @path2 = path_id from docloadjoint where carriage_id = @car2;
	  
	  update docloadjoint set carriage_id = null where carriage_id = @car1 and load_plan_id = @planid;	
	  
	  update docloadjoint set carriage_id = @car1, path_id = @path1 where carriage_id = @car2 and load_plan_id = @planid;	
	  update docloadjoint set carriage_id = @car2, path_id = @path2 where carriage_id is null and load_plan_id = @planid;	

	end else
	begin

	   update docloadjoint set carriage_id = @car2 where carriage_id = @car1 and load_plan_id = @planid;	

	end;

END
GO
