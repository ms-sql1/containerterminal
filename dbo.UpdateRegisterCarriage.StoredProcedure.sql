USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[UpdateRegisterCarriage]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateRegisterCarriage]
(
@ObjectId int output,
@Cnum varchar(50),
@KindCode varchar(50),
@OwnerId int,
@Note varchar(450),
@ParentObjectId int,
@DateIncome datetime = null
)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @oldownerid int;
	
	if isnull(@Objectid,0)=0
	begin
		select @Objectid = o.id from carriages c, objects o 
		where c.pnum = @Cnum and o.id = c.id and o.object_type = 'carriage';
	end;
	
	if isnull(@Objectid,0)=0
	begin
	   
	   exec InsertObject 'carriage', @ObjectId output;
	   
	   insert into carriages (id, pnum, kind_id, note, date_income)
	   values (@ObjectId, @Cnum, (select id from carriagekinds k where (k.code = @KindCode or convert(varchar, k.footsize) = @KindCode)),
	   @Note, @DateIncome);
	   
	end else
	begin
	   update carriages set 
	   pnum = isnull(@Cnum,pnum),
	   kind_id =  isnull(kind_id, (select id from carriagekinds k where (k.code = @KindCode or convert(varchar, k.footsize) = @KindCode))),
	   note = isnull(@Note,note), 
	   date_income = isnull(@DateIncome,date_income)
	   where id = @ObjectId;
	end;
	
	select @oldownerid = owner_id from objects where id = @ObjectId;
	--if (isnull(@oldownerid,0) <> @OwnerId) and (ISNULL(@OwnerId,0)<>0)
	
	if isnull(@oldownerid,0) <= 0
	insert into objectownerhistory (object_id, owner_id, date_change)
	values (@ObjectId, @OwnerId, GETDATE());
	
	update objects set owner_id = isnull(@OwnerId,owner_id) where id = @Objectid;
	
END

GO
