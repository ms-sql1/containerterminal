USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[mf_isEqual]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[mf_isEqual]
(
	@val1		sql_variant,
	@val2		sql_variant
)
returns tinyint
as
begin

if @val1 is null and @val2 is null return 1

if @val1 is null return 0
if @val2 is null return 0

if convert(varchar(50),@val1) = convert(varchar(50),@val2) return 1

return 0
end
GO
