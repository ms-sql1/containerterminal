USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[GenerateUnloadDocs]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenerateUnloadDocs] (@selectguid varchar(40), @loadplanid int, @folderid int, @userid int)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @newid int, @id int, @doctypeid int, @plandate datetime,
	@trainnum varchar(50), @contnum varchar(20), @contfoot varchar(50), 
	@contownerid int, @carnum varchar(20), @cartype varchar(50), @carownerid int,
	@sealnumber varchar(50), @weightsender float, @weightdoc float, @weightfact float, 
	@decr varchar(2000), @taskid int,  @dlvtypeid int, @e int;
	
	-- проверка 1
	set @e = 0;
	select @e = 1 where exists
	(select 1 from docfeed f, selectlist s, doccargosheet c 
		where f.id = c.unload_doc_id and s.id = c.id
		and not (f.load_plan_id is null) and f.load_plan_id <> @loadplanid 
		and s.guid = @selectguid
	);
	
	if @e = 1 
	begin
	  raiserror('В данной выборке уже создавались документы разгрузки по другому плану.', 16, 1);
	  return;
	end;
	
	
	-- проверка 2
	set @e = 0;
	select @e = 1 where exists
	(select 1 from selectlist s, doccargosheet c where s.id = c.id
		and c.dlv_type_id is null
	);
	
	if @e = 1 
	begin
	  raiserror('В данной выборке есть документы без установленного типа доставки.', 16, 1);
	  return;
	end;
	
	
	select @doctypeid = id from doctypes where system_section = 'income_unload';
	select @plandate = plan_date from loadplan where id = @loadplanid;
	
	DECLARE cDocSpec CURSOR FOR   
	select 
	d.id, train_num, container_num, container_foot, container_owner_id, carriage_num, carriage_type, carriage_owner_id,
	seal_number, weight_sender, weight_doc, weight_fact, cargo_description, dlv_type_id, task_id
	from doccargosheet d, selectlist s
	where d.id = s.id and s.guid = @selectguid
	and not exists (select 1 from docfeed f where f.id = d.unload_doc_id);
	
	open cDocSpec;
	
	fetch next from cDocSpec into 
	@id, @trainnum, @contnum, @contfoot, @contownerid, @carnum, @cartype, @carownerid,
	@sealnumber, @weightsender, @weightdoc, @weightfact, @decr, @dlvtypeid, @taskid;
	
	while @@fetch_status = 0  
	begin 
	
		insert into documents (doctype_id, folder_id) 
		values (@doctypeid, @folderid);

		select @newid = IDENT_CURRENT('documents');
		
		insert into docfeed 
		(
			id, load_plan_id, train_num, container_num, container_foot, container_owner_id, 
			carriage_num, carriage_type, carriage_owner_id, seal_number, 
			weight_sender, weight_doc, weight_fact, cargo_description, 
			dlv_type_id, task_id, user_created
		)
		values 
		(
			@newid, @loadplanid, @trainnum, @contnum, @contfoot, @contownerid, 
			@carnum, @cartype, @carownerid, @sealnumber,
			@weightsender, @weightdoc, @weightfact, @decr, 
			@dlvtypeid, @taskid, @userid
		);
		
		update doccargosheet set unload_doc_id = @newid where id = @id;

		update carriages set date_income = @plandate, train_num = @trainnum where pnum = @carnum;
	
		fetch next from cDocSpec into 
		@id, @trainnum, @contnum, @contfoot, @contownerid, @carnum, @cartype, @carownerid,
		@sealnumber, @weightsender, @weightdoc, @weightfact, @decr, @dlvtypeid, @taskid;
	
	end;

	close cDocSpec;
	deallocate cDocSpec;
	
	exec RegisterCarriagesFromSheet @selectguid, @plandate;
	
	delete from selectlist where guid = @selectguid;
	
END

GO
