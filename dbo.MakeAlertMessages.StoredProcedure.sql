USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[MakeAlertMessages]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MakeAlertMessages](@selectguid varchar(40))
AS
BEGIN
	SET NOCOUNT ON;

	declare @container_num varchar(50), @task_id int, @typed_restr varchar(250), @text_restr varchar(250),
	@email_consignee varchar(2000), @email_payer  varchar(2000), @consignee_id int, @old_consignee_id int, 
	@isactive bit, @elid int, @id int, @message varchar(MAX), @delim varchar(2), @restrmessage varchar(250), 
	@amount int, @guid uniqueidentifier, @old_alert_exists int, @old_container_num varchar(50), 
	@old_email_consignee varchar(2000), @old_email_payer varchar(2000), @footer varchar(2000),
	@kind_code varchar(50), @seal_number varchar(20), @contweight varchar(50), @weightshift bit, @weight_over float;

	-- удалим лишние записи в event_log

	delete from event_log where not exists (select 1 from alertque q where q.alert_que_guid = event_log.alert_que_guid)
	and exists (select 1 from event_log el where el.restriction_id = event_log.restriction_id and el.id>event_log.id);

	select @footer  = 
	'Предлагаем Вам воспользоваться услугой автодоставки контейнера до склада получателя.'+char(13)+char(10)+
	'Стоимость услуги в пределах г.Хабаровска:'+char(13)+char(10)+
	'20ф контейнер - 9 153,00 руб.'+char(13)+char(10)+
	'40ф контейнер - 11 186,00 руб.'+char(13)+char(10)+
	'Стоимость указана с учётом НДС.'+char(13)+char(10)+char(13)+char(10)+

	--'С 4 апреля 2020 г. вводится повышение тарифа на автодоставку контейнеров в связи с Постановлением Администрации города Хабаровска от 17.02.2020г. № 458 "О введении временного ограничения движения транспортных средств, в период снижения несущей способности конструктивных элементов автомобильных дорог общего пользования местного значения городского округа «Город Хабаровск» в весенне-летний период 2020 года". Указанное постановление вводит в период с 03.04.2020г. по 12.05.2020г. на автомобильных дорогах общего пользования местного значения городского округа «Город Хабаровск» временное ограничение движения транспортных средств, осевая нагрузка которых превышает 3,5 тонны на ось транспортного средства.'+char(13)+char(10)+
	--'Стоимость разрешения на проезд в период распутице можно уточнить по телефону (4212) 41-77-65'+char(13)+char(10)++char(13)+char(10)+


	'Информацию по ставкам на междугородние перевозки вы можете узнать по телефону 8(4212) 41-77-65 или по электронной почте sotsales@fesco.com.'+char(13)+char(10)++char(13)+char(10)+
	'С уважением, команда АО Стройоптторг.';

 	-- Пробежимся по выделенным doccargosheet

	DECLARE cDocs CURSOR FOR   
	select distinct isnull(isnull(sh.consignee_id,sh.payer_id),0), sh.container_num, sh.task_id, 
	isnull(sh.container_foot,''), isnull(sh.seal_number,'б/н'), isnull(str(sh.weight_fact, 10,0),''),
	(select name from restrictkinds rk where rk.id = rsa.type_id) as typed_restr,
	rsa.base as text_restr,
	rsa.isactive, 
	f.weight_shift,

	(case when  
	 (sh.container_foot like '20%' and f.weight_fact > 24000+500) then f.weight_fact
	when 
	 (sh.container_foot like '40%' and f.weight_fact > 30480+500) then f.weight_fact
	else 0
	end
	) as weight_over,   

	(select rtrim(ltrim(email)) from counteragents cr where cr.id = sh.consignee_id) as email_consignee, 
	(select rtrim(ltrim(email)) from counteragents cr where cr.id = sh.payer_id) as email_payer,
	rsa.elid, sh.id, 
	(select 1 where exists (select 1 from alertque q1 where q1.alert_que_guid = sh.alert_que_guid)) as old_alert_exists
	from selectlist sl, doccargosheet sh
	left outer join 
	(
		select el.id as elid, rs.* 
		from doccargorestrictions rs
		left outer join event_log el on (el.restriction_id = rs.id)
		and not exists (select 1 from alertque q where q.alert_que_guid = el.alert_que_guid)) rsa
	on (rsa.doc_id = sh.id)
	left outer join docfeed f on (sh.unload_doc_id = f.id)
	where sl.id = sh.id and sl.guid = @selectguid
	--and not sh.task_id is null 
	and not (sh.consignee_id is null and sh.payer_id is null)
	and (
		not exists (select 1 from alertque q where q.alert_que_guid = sh.alert_que_guid)
		--or sh.alert_date is null 
		or (not rsa.elid is null)
	)
	order by isnull(isnull(sh.consignee_id,sh.payer_id),0);

	open cDocs;
		
    set @old_consignee_id = -1;
    set @old_email_consignee = '-1';
    set @old_email_payer = '-1';
	set @message = '';
	set @delim = '';
	set @amount = 0;
	set @guid = NEWID();
	set @old_container_num = '-1';

	while 1=1
	begin 

		fetch next from cDocs into 
		@consignee_id, @container_num, @task_id, @kind_code, @seal_number, @contweight,
		@typed_restr, @text_restr, @isactive, @weightshift, @weight_over, @email_consignee, @email_payer, @elid, @id, @old_alert_exists;

		if @old_consignee_id = -1 set @old_consignee_id = @consignee_id;
		if @old_email_consignee = '-1' set @old_email_consignee = @email_consignee;
		if @old_email_payer = '-1' set @old_email_payer = @email_payer;

		update event_log set alert_que_guid = @guid where id = @elid;
		update doccargosheet set alert_que_guid = @guid where id = @id;

		if ((@old_consignee_id <> @consignee_id) or (@@fetch_status <> 0)) and (isnull(@consignee_id,0)>0)
		begin

			if @amount > 1
			set @message = 'Уважаемый клиент! На терминал прибыли следующие контейнеры: '+char(13)+char(10)+@message+
			char(13)+char(10)+char(13)+char(10)+@footer;

			if @amount = 1
			set @message = 'Уважаемый клиент! На терминал прибыл контейнер: '+char(13)+char(10)+@message+
			char(13)+char(10)+char(13)+char(10)+@footer;

			if isnull(@old_email_consignee,'') = isnull(@old_email_payer,'') 
			select @old_email_payer = '';

			select @old_email_consignee = isnull(@old_email_consignee,'')+(case when isnull(@old_email_payer,'')='' then '' else ','+@old_email_payer end);

			if substring(@old_email_consignee,1,1) = ',' set @old_email_consignee = substring(@old_email_consignee,2,2000);
			if substring(@old_email_consignee,len(@old_email_consignee)-1,1) = ',' set @old_email_consignee = substring(@old_email_consignee,1,len(@old_email_consignee)-1);
			
			insert into alertque (counteragent_id, email, message, alert_que_guid, issent)
			values (@old_consignee_id, @old_email_consignee, @message, @guid, 0);

			set @restrmessage = '';
			set @message = '';
			set @delim = '';
			set @old_consignee_id = @consignee_id;
			set @old_email_consignee = @email_consignee;
			set @old_email_payer = @email_payer;
			set @amount = 0;
			set @guid = NEWID();

		end;

		set @restrmessage = ' -';

		if @old_container_num <> @container_num
		begin
			if @isactive = 1 set @restrmessage = ' на контейнер наложен запрет.'; -- +isnull(@text_restr, @typed_restr);
			if (isnull(@isactive,0) = 0) and (@old_alert_exists=1) and (not @elid is null) set @restrmessage = ' контейнер снят с запрета';
			if isnull(@weightshift,0) = 1 set @restrmessage = @restrmessage+(case when len(@restrmessage)=1 then '' else ', ' end)+' имеется смещение центра тяжести';
			if isnull(@weight_over,0) >0 set @restrmessage = @restrmessage+(case when len(@restrmessage)=1 then '' else ', ' end)+' обнаружен перевес ('+str(@weight_over)+'кг.)';
			set @message = @message + @delim + @container_num +', '+@kind_code + 'фт. ('+@seal_number+', '+ltrim(@contweight)+' кг.)' + @restrmessage;
			set @delim = char(13)+char(10);
			set @amount = @amount + 1;
			set @old_container_num = @container_num;
		end;

		if @@fetch_status <> 0 break;

	end;

	close cDocs;
	deallocate cDocs;

	delete from selectlist where guid = @selectguid;

END

GO
