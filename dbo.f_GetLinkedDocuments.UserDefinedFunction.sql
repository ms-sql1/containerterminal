USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetLinkedDocuments]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[f_GetLinkedDocuments](@DocId int)
	RETURNS TABLE
AS
	RETURN 
	select d1.*, t1.code as doc_type_code
	from documents d1, doctypes t1 where  d1.id>@DocId and d1.doctype_id = t1.id and d1.id in
	(
		select ds1.doc_id from 
		(
		select ds.object_id, ds.task_id 
		from documents d, objects2docspec ds 
		where ds.doc_id = d.id and d.id = @DocId
		and isnull(d.isdeleted,0)=0 
		) s1, objects2docspec ds1
		where ds1.task_id = s1.task_id
		and ds1.object_id = s1.object_id 
		and ds1.doc_id<> @DocId
	) 	and isnull(d1.isdeleted,0)=0; 
GO
