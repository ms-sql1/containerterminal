USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[SetObjectAddress]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetObjectAddress](@ObjectId int, @AddressId int)
AS
BEGIN
	SET NOCOUNT ON;
	
	update objects set address_id = @AddressId where id = @ObjectId;

END

GO
