USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetFilterListExact]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[f_GetFilterListExact](@userid int, @doctype varchar(40))
RETURNS TABLE 
AS
RETURN 
(
	select 1 as filter_exist, f.v from 
	temp_filter f where f.c = 'object_num' 
	and (f.userid = @userid and @doctype = f.doctypes)

)
GO
