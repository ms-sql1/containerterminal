USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[split_pair]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[split_pair](@pair varchar(200), @delim char(1), @num int)
returns varchar(200)
AS
BEGIN
  declare @pos int, @temp varchar(200), @i integer;

  if @pair = '' return '';
  
  set @pos = CHARINDEX(@delim, @pair);
  if @pos=0 and @num=2 return '';

  if @pos=0 and @num=1 set @pos = 256;
  
	  if @num = 1
	  begin
		--if (@pos-1)>=LEN(@pair) return '';
		return substring(@pair, 1, @pos-1);
	  end;
	  
	  if @num = 2
	  begin
		if (@pos)>=LEN(@pair) return '';
		return substring(@pair, @pos+1, 255);
	  end;
  
  return '';

END;

GO
