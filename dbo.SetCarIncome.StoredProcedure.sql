USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[SetCarIncome]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetCarIncome](@guid varchar(40), @DateIncome datetime)
AS
BEGIN

	SET NOCOUNT ON;

	update carriages set date_income = @DateIncome 
	where id in (select id from selectlist where guid = @guid);

	delete from selectlist where guid = @guid;

END

GO
