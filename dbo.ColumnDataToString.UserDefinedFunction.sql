USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[ColumnDataToString]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ColumnDataToString](@columndata ColumnData readonly, @delimiter varchar(1))
	RETURNS varchar(2000)
AS
BEGIN

	declare @data varchar(250), @str varchar(2000), @d varchar(1);

	declare data cursor for select data from @columndata;
	open data;

	set @str = ''
	set @d = '';

	fetch next from data INTO @data
  
	While @@FETCH_STATUS = 0  
	begin
		
		set @data = replace(isnull(@data,''),' ','');

		if isnull(@data,'')<>''
		begin
			set @str = @str+@d+ltrim(rtrim(@data));
			set @d = @delimiter;
		end;

		fetch next from data INTO @data
	end;

	close data;
	deallocate data;

	return @str;

END;



GO
