USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[MatrixMark]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MatrixMark](@operation int, @cnum varchar(20), @mark int)
AS
BEGIN
	SET NOCOUNT ON;

    delete from matrixmarks where container_id in (select id from containers c where c.cnum = @cnum);
	
	
	if @operation = 1
	insert into matrixmarks (container_id, mark) 
	select c.id, @mark from containers c where c.cnum = @cnum;

END

GO
