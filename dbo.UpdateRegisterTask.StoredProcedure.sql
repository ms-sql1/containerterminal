USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[UpdateRegisterTask]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateRegisterTask](
@TaskId int out,
@DeliveryTypeId int, 
@PlanDate datetime, 
@ForwarderId int, 
@ConsigneeId int, 
@PayerId int, 
@ObjectId int,
@BaseDocId int,
@BaseDocNumber varchar(50),
@BaseDocDate datetime, 
@StationId int = null
) 
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @id int, @m varchar(450);
	
	--select @id = max(id) from tasks where dlv_type_id = @DeliveryTypeId and object_id = @ObjectId;
	set @id  = @TaskId;
	
	if @id is null
	begin
		insert into tasks (start_date, dlv_type_id, plan_date, forwarder_id, consignee_id, payer_id, object_id, basedoc_id, basedoc_number, basedoc_date, station_id) 
		values (getdate(), @DeliveryTypeId, @PlanDate, @ForwarderId, @ConsigneeId, @PayerId, @ObjectId, @BaseDocId, @BaseDocNumber, @BaseDocDate, @StationId);
		
		select @id = @@IDENTITY;
		
	end else
	begin
	
		update tasks set
		dlv_type_id = isnull(@DeliveryTypeId,dlv_type_id), 
		plan_date = isnull(@PlanDate,plan_date), 
		forwarder_id = isnull(@ForwarderId,forwarder_id),
		consignee_id = isnull(@ConsigneeId,consignee_id),
		payer_id = isnull(@PayerId,payer_id),
		--object_id = isnull(@ObjectId,object_id),
		basedoc_id = isnull(basedoc_id, @BaseDocId),
		basedoc_number = isnull(basedoc_number, @BaseDocNumber), 
		basedoc_date = isnull(basedoc_date, @BaseDocDate),
		station_id = isnull(@StationId, station_id)
		where id = @id;
		
	end;
	
	update [objects] set current_task_id = @id where id = @ObjectId;
	set @TaskId = @id;
	
	
END

GO
