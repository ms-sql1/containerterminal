USE [sot]
GO
/****** Object:  View [dbo].[v_objects]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[v_objects] as
SELECT     o.id, o.object_type, 'контейнер' as rus_object_type, o.start_datetime, c1.cnum, o.owner_id, o.state_id, o.linked_object_id, o.current_task_id,
                          (SELECT     code
                            FROM          containerkinds k
                            WHERE      k.id = c1.kind_id) AS kind_code, c1.kind_id, null as isempty
FROM         objects o, containers c1
WHERE     c1.id = o.id AND o.object_type = 'container'
UNION ALL
SELECT     o.id, o.object_type, 'вагон' as rus_object_type, o.start_datetime, c2.pnum, o.owner_id, o.state_id, o.linked_object_id, o.current_task_id,
                          (SELECT     code
                            FROM          carriagekinds k
                            WHERE      k.id = c2.kind_id) AS kind_code, c2.kind_id, null as isempty
FROM         objects o, carriages c2
WHERE     c2.id = o.id AND o.object_type = 'carriage'
UNION ALL
SELECT     o.id, o.object_type, 'груз' as rus_object_type, o.start_datetime, c3.seal_number, o.owner_id, o.state_id, o.linked_object_id, o.current_task_id,
                          (SELECT     code
                            FROM          cargotypes t
                            WHERE      t .id = c3.type_id) AS kind_code, c3.type_id, isempty
FROM         objects o, cargos c3
WHERE     c3.id = o.id AND o.object_type = 'cargo'

GO
