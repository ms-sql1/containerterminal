USE [sot]
GO
/****** Object:  View [dbo].[v_activecontainertask]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[v_activecontainertask]
as
select t.id as task_id, 
los.doc_id,   
t.dlv_type_id, 
dlv.code as dlv_code, 
o.id as container_id, 
los.object_id as cargo_id, 
t.start_date, 
los.date_factexecution as stay_start_date, 
o.state_id, 
o.real_state_id, 
sk.code as inplace_state_code, 
sk1.code as formal_state_code, 
(
	select min(r2s.end_object_state_id) 
	from docroutes r, docroute2objectstates r2s, tasks t1, objectstatekinds sk1
	where t1.id = o.current_task_id
	and r.dlv_type_id = t1.dlv_type_id and r2s.docroute_id = r.id 
	and r2s.objecttype_id = 1
	and (r2s.start_object_state_id = o.state_id /*or r2s.start_object_state_id is null*/)
	and (r2s.start_real_state_id = o.real_state_id or r2s.start_real_state_id is null)
	and (r2s.end_object_state_id <> o.state_id)
	and r2s.end_object_state_id = sk1.id
	and isnull(sk1.isformalsign,0) = 0
) as next_state_id
from tasks t, deliverytypes dlv, v_lastobjectrealstates los,  
objects o1, objects o, objectstatekinds sk, objectstatekinds sk1 
where o1.id = t.object_id and o.id = o1.linked_object_id
and los.linked_object_id = o.id 
and los.task_id = t.id 
and o.real_state_id = sk.id
and isnull(sk.inplacesign,0) = 1
and o.state_id = sk1.id
and t.dlv_type_id = dlv.id
and los.date_factexecution = (
	select max(los1.date_factexecution) from v_lastobjectrealstates los1 
	where /*los1.task_id = los.task_id and*/ los1.linked_object_id = los.linked_object_id
)



GO
