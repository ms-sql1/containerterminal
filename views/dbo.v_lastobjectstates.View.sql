USE [sot]
GO
/****** Object:  View [dbo].[v_lastobjectstates]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[v_lastobjectstates] as
select osk.*, o.object_type, o.state_id, o.real_state_id 
from objectstatehistory osk with (index(objectid_idx)), objects o 
where osk.id = (
select max(id) from objectstatehistory osk1 
where osk1.object_id = osk.object_id
and osk1.task_id = osk.task_id
and not osk1.object_state_id is null
)
and not osk.object_state_id is null
and o.id = osk.object_id


GO
