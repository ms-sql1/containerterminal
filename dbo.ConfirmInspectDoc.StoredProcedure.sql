USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[ConfirmInspectDoc]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ConfirmInspectDoc](
@DocId int
) 
AS
BEGIN
	SET NOCOUNT ON;
	
	-- вагоны
	update c set c.techcond_id = s.techcond_id, c.date_inspection = d.fact_datetime, 
	c.isdefective = t.isdefective
	from carriages c, docinspectspec s, docinspect d, techconditions t
	where s.object_id = c.id and s.parent_id = d.id and t.id = s.techcond_id
	and d.id = @DocId;
		
	-- контейнеры
	update c set c.techcond_id = s.techcond_id, c.date_inspection = d.fact_datetime, 
	c.isdefective = t.isdefective
	from containers c, docinspectspec s, docinspect d, techconditions t
	where s.object_id = c.id and s.parent_id = d.id and t.id = s.techcond_id
	and d.id = @DocId;
		
	update docinspect set isconfirmed = 1, date_confirm = null where id = @DocId;
	
END
GO
