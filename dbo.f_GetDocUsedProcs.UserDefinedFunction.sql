USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetDocUsedProcs]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[f_GetDocUsedProcs](@DocId int, @doctype varchar(50))
	RETURNS float
AS
BEGIN

	declare @usedcount float, @basecount float;
	
	select @basecount = count(distinct ds.object_id)
	from objects2docspec ds, containers v  
	where ds.doc_id = @DocId and v.id = ds.object_id;

	select @usedcount = count(distinct ds0.object_id) 
	from objects2docspec ds0, documents d0,
	(
	 select ds.object_id, ds.task_id 
	 from objects2docspec ds, containers v  
	 where ds.doc_id = @DocId and v.id = ds.object_id
	) s1
	where s1.object_id = ds0.object_id
	and s1.task_id = ds0.task_id
	and d0.id = ds0.doc_id
	and d0.isconfirmed = 1
	and 
	(
		(isnull(@doctype,'')='' and ds0.doc_id > @DocId) 
		or 
		charindex('-'+convert(varchar, d0.doctype_id)+'-', '-'+@doctype+'-')>0
	)
	and isnull(d0.isdeleted,0)=0; 
	
	if @basecount=0 return null;
	
	return round((@usedcount/@basecount)*100,0);
		
END;
GO
