USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[SetAlertDate]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetAlertDate](@guid varchar(40), @alertdate datetime)
AS
BEGIN
	SET NOCOUNT ON;

    update doccargosheet set alert_date = @alertdate
    where id in (select id from selectlist where guid = @guid);

	update tasks set alert_date = @alertdate 
	where tasks.id in (select task_id from doccargosheet sh, selectlist sl where sh.id = sl.id and sl.guid = @guid);

    delete from selectlist where guid = @guid;

END

GO
