USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[AbolishDoc]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AbolishDoc](@DocId int, @UserId int) 
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @id int,@RootDocId int,@PlanDate datetime,@ForwarderId int,@ConsigneeId int,@PayerId int, @ObjectId int;
	
	declare @e int, @m varchar(250);
	
	set @e = 0;
	
	--1) Проверяем, что запись в истории последняя
	
	select @e=max(1) from  objectstatehistory osh 
	where osh.doc_id = @DocId
	and exists (
		select 1 from objectstatehistory osh1  where osh1.object_id = osh.object_id 
		and osh1.task_id = osh.task_id
		and osh1.id>osh.id 
	);
	
	if @e = 1 
	begin
		select @m = dt.code+' № '+d.doc_number from doctypes dt, documents d where d.id = @DocId and d.doctype_id = dt.id;
		set @m = 'Документ '+@m+' имеет более поздние операции. Аннулирование возможно, только после аннулирования документов, созданных на основании данного документа.';
		raiserror(@m, 16,1);
		return 0;
	end;

	-- удалили корневой обьект - груз
	delete from objectstatehistory where doc_id = @DocId;

	-- удаляем все связанные с грузом
	--1)
	delete from objectstatehistory
	where not exists (
		select 1 from objectstatehistory osh1  
		where osh1.doc_id = @DocId 
		and osh1.linked_object_id = objectstatehistory.object_id
		and osh1.task_id = objectstatehistory.task_id
	) 
	and not exists 
	(select 1 from objects o where o.id = objectstatehistory.object_id and o.object_type = 'cargo')
	and doc_id = @DocId;

	--2)
	delete from objectstatehistory
	where not exists (
		select 1 from objectstatehistory osh1  
		where osh1.doc_id = @DocId 
		and osh1.linked_object_id = objectstatehistory.object_id
		and osh1.task_id = objectstatehistory.task_id
	) 
	and not exists 
	(select 1 from objects o where o.id = objectstatehistory.object_id and o.object_type = 'cargo')
	and doc_id = @DocId;

	delete from containerdefecthistory where register_doc_id = @DocId and container_id in 
	(select object_id from objects2docspec where doc_id = @Docid);
	
    exec UpdateObjectState @DocId, 1;

    -- проверяем полное уничтожение всей истории документа. если так, то удаляем задачу
    delete from tasks where not exists (select 1 from objectstatehistory osh where osh.task_id = tasks.id);

	delete from objects2docspec where doc_id = @DocId;

	insert into change_log(table_id, table_name, user_id, event_type, event_datetime)
	values (@DocId, 'documents', @UserId, 'abolished', getdate());
    
	update documents set isabolished = 1 where id = @DocId;


END

GO
