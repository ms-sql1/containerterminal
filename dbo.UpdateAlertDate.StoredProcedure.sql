USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[UpdateAlertDate]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateAlertDate](@alert_que_guid uniqueidentifier, @alertdate datetime)
AS
BEGIN
	SET NOCOUNT ON;

    update doccargosheet set alert_date = @alertdate where alert_date is null and alert_que_guid = @alert_que_guid;
	update doccargosheet set last_alert_date = @alertdate where alert_que_guid = @alert_que_guid;

	update tasks set alert_date = @alertdate
	where tasks.id in (select task_id from doccargosheet sh where alert_que_guid = @alert_que_guid)
	and alert_date is null;

END

GO
