USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[CopyCons2Payer]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyCons2Payer](@selectguid varchar(40))
AS
BEGIN
	SET NOCOUNT ON;
	
	update sh set payer_id = consignee_id 
	from doccargosheet sh, documents d
	where d.id = sh.id and isnull(d.isconfirmed,0) = 0
	and sh.payer_id is null and sh.id in 
	(select id from selectlist sl where sl.guid = @selectguid);

END

GO
