USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[PrepareTransformFromPassOp]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PrepareTransformFromPassOp] (@passopid int, @docrouteid int, @userid int)
AS
BEGIN
	SET NOCOUNT ON;

		IF OBJECT_ID('tempdb.dbo.##temp_prepared_newdoc', 'U') IS NOT NULL DROP TABLE ##temp_prepared_newdoc; 	
	
		select distinct @passopid as selectguid, 0 as checkcolumn, s1.task_id, s1.object_id, s1.linked_object_id,
		(
			select count(*) where exists 
			(select 1 from objects2docspec ods2, documents d2 where ods2.doc_id = d2.id and ods2.object_id = s1.object_id and isnull(d2.isdeleted,0)=0 and d2.doctype_id = r.end_doctype_id and ods2.task_id = s1.task_id)
		) as docexists,
		(select code from doctypes where id = r.end_doctype_id) as doctype_code,
		s1.dlv_type_id, 
		(select code from deliverytypes dt where dt.id = s1.dlv_type_id) as dlvtype_code,
		s1.object_type, s1.cnum, s1.kind_code, r.copy_prev_state
		into ##temp_prepared_newdoc
		from docroutes r with (nolock), docroute2objectstates drs with (nolock), 
		(
			select ods.id as docspec_id, ods.object_id, 
			o.linked_object_id,
			(case when o.linked_object_id is null then 0 else 1 end) as link_type,
			o.object_type,
			o.cnum, o.kind_code,
			(select id from objecttypes where code = o.object_type) as objecttype_id,
			ods.task_id, o.state_id, t.dlv_type_id
			from objects2docspec ods, v_objects o, tasks t, passoperations po
			where po.id = @passopid
			and po.task_id = t.id
			and o.id = ods.object_id
			and o.id = t.object_id
			and t.id = ods.task_id
		) s1
		where r.id = drs.docroute_id
		and r.id = @docrouteid
		--and r.end_doctype_id = @end_doctype_id
		and (drs.start_object_state_id = s1.state_id or drs.start_object_state_id is null)
		and drs.objecttype_id = s1.objecttype_id
		and r.dlv_type_id = s1.dlv_type_id;
		
		update ##temp_prepared_newdoc set checkcolumn =1 where docexists = 0;
		delete from ##temp_prepared_newdoc where object_type <> 'cargo';
		delete from ##temp_prepared_newdoc where docexists = 1;
		
		--select * from ##temp_prepared_newdoc;
	
END

GO
