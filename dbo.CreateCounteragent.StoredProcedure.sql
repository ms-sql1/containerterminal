USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[CreateCounteragent]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateCounteragent](@code varchar(250))
AS
BEGIN
	SET NOCOUNT ON;

	declare @m varchar(250);
	
	begin try
		insert into counteragents (code, name, folder_id) 
		values (@code, @code, (select id from folders where folder_section = 'counteragents' and folder_name = 'Импорт'));
	end try
	begin catch
		set @m = 'Ошибка при добавлении контрагента '+@code;
		raiserror(@m, 16, 1);
	end catch;

END
GO
