USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[lstint]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[lstint]
(
	@Val1 int, @Val2 int
)
RETURNS int
AS
BEGIN

	return (case when @Val1>@Val2 then @Val2 else @Val1 end);

END

GO
