USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[lstdate]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[lstdate]
(
	@date1 datetime, @date2 datetime
)
RETURNS datetime
AS
BEGIN

	RETURN (case when @date1>@date2 then @date2 else @date1 end);

END

GO
