USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[TransformDocumentAdd]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransformDocumentAdd] (@docid int, @userid int)
AS
BEGIN
	SET NOCOUNT ON;

	declare @newdoctypeid int, @folderid int, @e int;
	declare @taskid int, @objectid int, @linkedobjectid int;

	declare cDocNewData cursor for 
	select task_id, object_id, linked_object_id
	from ##temp_prepared_newdoc where checkcolumn = 1;
 
	open cDocNewData;
	
	fetch next from cDocNewData into @taskid, @objectid, @linkedobjectid;

	while @@fetch_status = 0  
	begin 
	
		Exec RegisterDocSpec @docid, @taskid, @objectid, @e out;
		
		fetch next from cDocNewData into @taskid, @objectid, @linkedobjectid;
 
	end;

	insert into docdispatchspec 
	(doc_id, o2s_id, carriage_id, container_id, cargo_id, car_feed_date, car_paper_number, mtukind_id, picturekind_id, dispatch_numbers, receipt_number, receipt_summa)
	select 
	o2s.doc_id, o2s.id, j.carriage_id, l.container_id, o2s.object_id, c.date_income, c.car_paper_number, l.mtukind_id, l.picturekind_id, l.dispatch_numbers, l.receipt_number, l.receipt_summa
	from docloadjoint j, docload l, objects2docspec o2s
	left outer join v_objects v on (v.id = o2s.linked_object_id and v.object_type = 'container') 
	left outer join carriages c on (c.id = v.linked_object_id) 
	where j.id = l.joint_id and o2s.doc_id = @docid and not v.cnum is null 
	and l.task_id = o2s.task_id and l.container_id = o2s.linked_object_id
	and not o2s.id in (select o2s_id from docdispatchspec where doc_id = @docid);
	
	close cDocNewData;
	deallocate cDocNewData;

end;
GO
