USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[MoveToFolder]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MoveToFolder](@selectguid varchar(40), @folderid int)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	update documents set folder_id = @folderid 
	where id in (select id from selectlist s where s.guid = @selectguid)
	
	delete from selectlist where guid = @selectguid;
	
END

GO
