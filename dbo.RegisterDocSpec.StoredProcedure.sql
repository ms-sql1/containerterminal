USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[RegisterDocSpec]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RegisterDocSpec](@DocId int, @TaskId int, @RootObjectId int, @RowNums int out)
AS
BEGIN

	SET NOCOUNT ON;

	declare @StartObjectId int;

	select @StartObjectId = object_id from tasks t where t.id = @TaskId;

	insert into objects2docspec (task_id, doc_id, object_id, linked_object_id, date_created)
	select @TaskId, @DocId, 
	(case when o.object_type = 'cargo' then @RootObjectId else o.id end), 
	o.linked_object_id, GETDATE() from f_objects_tree(@StartObjectId) o
	where not exists (
		select 1 from objects2docspec od1 where od1.doc_id = @DocId and od1.task_id = @TaskId and od1.object_id = (case when o.object_type = 'cargo' then @RootObjectId else o.id end)
    );
    
	update objects2docspec set linked_object_id = @RootObjectId where task_id = @TaskId and doc_id = @DocId and linked_object_id = @StartObjectId;

    --update objects2docspec set task_id = @TaskId where doc_id = @DocId and task_id is null;
    update o set current_task_id = ods.task_id
    from objects o, objects2docspec ods where ods.doc_id = @DocId
    and o.id = ods.object_id;
    

    update ods set linked_object_id = o.linked_object_id
    from objects o, objects2docspec ods where ods.doc_id = @DocId
    and o.id = ods.object_id;

	delete from objects2docspec 
	where exists (
		select 1 from objects o where o.object_type = 'cargo' and o.id = objects2docspec.object_id
		and objects2docspec.linked_object_id is null
	);

	select @RowNums = count(*) from objects2docspec where doc_id = @DocId and task_id = @TaskId;
	
END

GO
