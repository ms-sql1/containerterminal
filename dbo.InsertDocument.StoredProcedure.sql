USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[InsertDocument]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[InsertDocument]
(
	@id int out,
	@folderid int,
	@doctypeid int, 
	@docdate datetime, 
	@docnumber varchar 
)
as
begin
	
	insert into documents (folder_id, doctype_id) values (@folderid, @doctypeid); 
	
	select @id = @@IDENTITY;
	
end;
GO
