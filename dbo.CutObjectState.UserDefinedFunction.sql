USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[CutObjectState]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CutObjectState](@datecut datetime, @excludestates varchar(50))
 RETURNS TABLE
as
	RETURN 
	select osk.*, o.owner_id, o.object_type, o.state_id, o.real_state_id,
	(
		select isnull(isempty,0) from cargos cr
		where cr.id = osk.object_id 
	) as isempty 
	from objectstatehistory osk, objects o, objectstatekinds sk
	where osk.date_factexecution = (
		select max(date_factexecution) from objectstatehistory osk1, objectstatekinds sk1 
		where osk1.linked_object_id = osk.linked_object_id
		and osk1.object_state_id = sk1.id
		and isnull(sk1.isformalsign,0) = 0
		and not osk1.object_state_id is null
		and osk1.date_factexecution < @datecut+1
		and (@excludestates is null or charindex(','+convert(varchar, sk1.id)+',', @excludestates)= 0)
	)
	and not osk.object_state_id is null
	and o.id = osk.linked_object_id
	and osk.object_state_id = sk.id
	and isnull(sk.isformalsign,0) = 0
	and osk.date_factexecution < @datecut+1
	and (@excludestates is null or charindex(','+convert(varchar, sk.id)+',', @excludestates)= 0)

GO
