USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[CheckNonConfirmedChildDocs]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CheckNonConfirmedChildDocs] (@selectguid varchar(40))
AS
BEGIN
	SET NOCOUNT ON;

	declare @e int;

	set @e = 0;

	select @e=1 where exists (
		select 1 from documents d, selectlist sl, documents d1, objects2docspec o2s, objects2docspec o2s1, ##temp_prepared_newdoc tmp
		where sl.id = d.id and o2s.doc_id = d.id and o2s1.doc_id = d1.id
		and o2s.task_id = o2s1.task_id and o2s.object_id = o2s1.object_id
		and d1.id > d.id and isnull(d1.isconfirmed,0) = 0
		and sl.guid = @selectguid and tmp.selectguid = @selectguid
		and tmp.checkcolumn = 1
		and o2s.object_id = tmp.object_id
	);

	if @e=1 begin
		raiserror('Есть НЕпроведенные документы, созданные на основании выбранных документов. Создание новых документов невозможно!', 16, 1);
		return;
	end;

END;
GO
