USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[filter_objects]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[filter_objects](@guid varchar(40), @cnum varchar(50))
RETURNS int
AS
BEGIN

	declare @res int;

	if @guid = '' return 1;

	select @res = 0;

	select @res = 1 where exists (select 1 from temp_filter t where t.doctypes = @guid and CHARINDEX(rtrim(ltrim(t.v)), @cnum)>0);

	select @res = 1 where not exists (select 1 from temp_filter t where t.doctypes = @guid and rtrim(isnull(t.doctypes,''))<>'');

	return @res;
	

END;
GO
