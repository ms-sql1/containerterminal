USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[TransformDocument]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransformDocument] (@routeid int, @newdoctypeid int, @folderid int, @userid int)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @id int, @e int, @systemsection varchar(50), @defaultdlvtypeid int, 
	@docnumber int, @direction int, @station int, @m varchar(450);
	declare @taskid int, @docid int, @objectid int, @linkedobjectid int;
	declare @dlv_type_id int, @globalsection varchar(50), @createnewobject bit,
	@defaultisempty bit, @deafaultsealnumber varchar(50), @drivername varchar(50), @cardata varchar(50), @attorney varchar(120),
	@recept_org_name varchar(450), @recept_address varchar(450), @recept_contact varchar(450), 
	@delivery_org_name varchar(450), @delivery_address varchar(450), @delivery_contact varchar(450), 
	@return_address varchar(450), @return_contact varchar(450), 
	@consigneeid int, @forwarderid int, @copyprevstate bit, @oldobjectid int;
	
	select @systemsection = system_section, @defaultdlvtypeid = dlv_type_id 
	from doctypes where id = @newdoctypeid;

	select @createnewobject = create_new_object,  
	@defaultisempty  = drs.default_isempty, @deafaultsealnumber = drs.default_seal_number
	from docroutes r, docroute2objectstates drs, objecttypes ot
	where r.id = @routeid and drs.docroute_id = r.id and drs.objecttype_id = ot.id
	and ot.code = 'cargo';

	-- подготавливаем первую запись
	
	if @systemsection = 'income_unload'
	begin
		insert into documents (doctype_id, folder_id, isbased, user_created) 
		values (@newdoctypeid, @folderid, 1, @userid);
		select @id = @@IDENTITY;
		insert into docunload(id) values (@id);
	end;
	
	if @systemsection like 'income%removal'
	begin
	
		-- Вычисляем параметры доставки

		select top 1 @taskid = task_id, @objectid = object_id, @linkedobjectid = linked_object_id from ##temp_prepared_newdoc;
		
		select @dlv_type_id = t.dlv_type_id, 
		@globalsection = d.global_section, @consigneeid = t.consignee_id, @forwarderid = t.forwarder_id
		from tasks t, deliverytypes d where t.id = @taskid and d.id = t.dlv_type_id;
		
		if @globalsection = 'input'
		begin
		
			select @delivery_org_name = c.name, 
			@delivery_address = location, @delivery_contact = manager+', '+contacts 
			from counteragents c where c.id = @consigneeid;
			
			select @recept_org_name = s.value from settings s where s.code = 'main_name';
			select @recept_address = s.value from settings s where s.code = 'main_address';
			select @recept_contact = u.short_name+', '+u.contact from users u where u.id = @userid;
			
			select @return_address = s.value from settings s where s.code = 'main_address';
			select @return_contact = u.short_name+', '+u.contact from users u where u.id = @userid;

		end;
		
		if @globalsection = 'store'
		begin
		
			select @recept_org_name = c.name, 
			@recept_address = location, @recept_contact = manager+', '+contacts 
			from counteragents c where c.id = @forwarderid;
			
			select @delivery_org_name = s.value from settings s where s.code = 'main_name';
			select @delivery_address = s.value from settings s where s.code = 'main_address';
			select @delivery_contact = u.short_name+', '+u.contact from users u where u.id = @userid;
			
		end;
		
		if @globalsection = 'output'
		begin
		
			select @recept_org_name = c.name, 
			@recept_address = location, @recept_contact = manager+', '+contacts 
			from counteragents c where c.id = @forwarderid;
			
			select @delivery_org_name = s.value from settings s where s.code = 'main_name';
			select @delivery_address = s.value from settings s where s.code = 'main_address';
			select @delivery_contact = u.short_name+', '+u.contact from users u where u.id = @userid;
			
		end;


		if @globalsection = 'output-output'
		begin
		
			select @recept_org_name = c.name, 
			@recept_address = location, @recept_contact = manager+', '+contacts 
			from counteragents c where c.id = isnull(@forwarderid, @consigneeid);

			select @delivery_org_name = c.name, 
			@delivery_address = location, @delivery_contact = manager+', '+contacts 
			from counteragents c where c.id = @consigneeid;
			
		end;

		
		insert into documents (doctype_id, folder_id, isbased, user_created) 
		values (@newdoctypeid, @folderid, 1, @userid);
		select @id = @@IDENTITY;
		insert into docremoval(id, dlv_type_id, recept_org_name, recept_address, recept_contact, 
		delivery_org_name, delivery_address, delivery_contact, return_address, return_contact) 
		values (@id, @dlv_type_id, @recept_org_name, @recept_address, @recept_contact, 
		@delivery_org_name, @delivery_address, @delivery_contact, @return_address, @return_contact);
		
	end;
	
	if @systemsection like 'income_empty'
	begin
	
		insert into documents (doctype_id, folder_id, isbased, user_created) 
		values (@newdoctypeid, @folderid, 1, @userid);
		select @id = @@IDENTITY;

		select top 1 @taskid = task_id, @objectid = object_id, @linkedobjectid = linked_object_id, @defaultdlvtypeid = dlv_type_id from ##temp_prepared_newdoc;

		select @drivername = pr.person_name, @cardata  = p.car_data+' '+p.car_number, @attorney = p.attorney
		from passes p inner join passoperations po on (po.parent_id = p.id)
		left outer join persons pr on (pr.id = p.driver_id)
		where po.task_id = @taskid;

		insert into docincome (id, dlv_type_id, driver_name, car_data, attorney) values (@id, @defaultdlvtypeid, @drivername, @cardata, @attorney);
		
	end;

	if @systemsection like 'outcome%dispatch'
	begin

		declare @dicpatchkind int, @trainnum varchar(50);

		select top 1 @taskid = task_id, @objectid = object_id, @linkedobjectid = linked_object_id from ##temp_prepared_newdoc;

		select @station = o.station_id, @dicpatchkind = p.dispatch_kind_id, @trainnum = j.train_num
		from tasks t, objects2docspec o2s, documents d, docload o, docloadjoint j
		left outer join loadplan p on (p.id = j.load_plan_id)
		where t.id = @taskid and t.object_id = @objectid and o.joint_id = j.id
		and t.object_id = o2s.object_id and o2s.doc_id = d.id
		and o.id = d.id and isnull(o.station_id,0)<>0;
		
		if @station is null
		begin
		   set @m = 'В заявке на отправку не указана Станция'+convert(varchar(10), @taskid);
		   raiserror(@m,16,1);
		   close cDocNewData;
		   deallocate cDocNewData;
		   return;
		end;

		insert into documents (doctype_id, folder_id, isbased, user_created) 
		values (@newdoctypeid, @folderid, 1, @userid);
		select @id = @@IDENTITY;

		insert into docdispatch(id, station_id, dispatchkind_id, train_num) values (@id, @station, @dicpatchkind, @trainnum);

	end;
	
	-- перебираем объекты
	declare cDocNewData cursor local for 
	select task_id, object_id, linked_object_id, copy_prev_state
	from ##temp_prepared_newdoc where checkcolumn = 1;
 
	open cDocNewData;
	
	fetch next from cDocNewData into @taskid, @objectid, @linkedobjectid, @copyprevstate;

	while @@fetch_status = 0  
	begin 
	
		if @systemsection like 'outcome_loads'
		begin
		
			insert into documents (doctype_id, folder_id, isbased, user_created) 
			values (@newdoctypeid, @folderid, 1, @userid);
			select @id = @@IDENTITY;
			
			select @docnumber = MAX(d.doc_number), @direction = max(o.direction_id), @station = MAX(o.station_id)
			from tasks t, objects2docspec o2s, documents d, docorder o
			where t.id = @taskid and t.object_id = @objectid 
			and t.object_id = o2s.object_id and o2s.doc_id = d.id
			and o.id = d.id;

			insert into docload(id, container_id, order_num, station_id) 
			values (@id, @linkedobjectid, @docnumber, @station);
			
		end;
		
		insert into temp_log (id1) values (@objectid);
		
		if @createnewobject = 1
		begin
			set @objectid  = null;
			exec UpdateRegisterCargo @objectid output, null, null, null, null, null, /*@deafaultsealnumber*/null, @defaultisempty, null, @linkedobjectid;
		end;

		insert into temp_log (id1) values (@objectid);

		Exec RegisterDocSpec @id, @taskid, @objectid, @e;

		-- определяем данные текущего груза
		if isnull(@copyprevstate, 0) = 1
		begin
		   select @oldobjectid = object_id from v_lastobjectrealstates v where v.linked_object_id = @linkedobjectid;
		   Exec CopyCargoParams @oldobjectid, @objectid;
		end;
		
		fetch next from cDocNewData into @taskid, @objectid, @linkedobjectid, @copyprevstate;
 
	end 
	
	if @systemsection like 'income_empty'
	begin

		insert into docincomespec (doc_id, o2s_id, container_id, container_num, container_kind_id, container_owner_id, 
		cargo_id, seal_number, cargotype_id, isempty, carrying, weight_cargo, weight_container, container_weight, weight_shift, is_defective)
		select o2s.doc_id, o2s.id, cn.id, cn.cnum, cn.kind_id, (select owner_id from objects o where o.id = cn.id),
		cr.id, cr.seal_number, 
		(case when t.dlv_type_id=6 and ic.dlv_type_id=15 then null else cr.type_id end), 
		(case when t.dlv_type_id=6 and ic.dlv_type_id=15 then 1 else cr.isempty end),
		cn.carrying, cr.weight_fact, cn.container_weight, cn.container_weight,
		(case when @createnewobject = 1 then 0 else
		  (select isnull(weight_shift,0) from docfeed f where f.task_id = o2s.task_id)
		end), 
		(case when ob.real_state_id in (select id from objectstatekinds sk where isnull(sk.inplacesign,0) = 1) then cn.isdefective else 0 end)
		from docincome ic, tasks t,  objects2docspec o2s
		left outer join cargos cr on (cr.id = o2s.object_id)
		left outer join containers cn on (cn.id = o2s.linked_object_id)
		left outer join objects ob on (ob.id = o2s.linked_object_id)
		where o2s.doc_id = @id and ic.id = o2s.doc_id and t.id = o2s.task_id
		and not cn.cnum is null;

	end;
	
	
	if @systemsection like 'outcome%dispatch'
	begin
		insert into docdispatchspec 
		(doc_id, o2s_id, carriage_id, container_id, cargo_id, car_feed_date, car_paper_number, mtukind_id, picturekind_id, dispatch_numbers, receipt_number, receipt_summa)
		select 
		o2s.doc_id, o2s.id, j.carriage_id , l.container_id, o2s.object_id, c.date_income, c.car_paper_number, l.mtukind_id, l.picturekind_id, l.dispatch_numbers, l.receipt_number, l.receipt_summa
		from docloadjoint j, docload l, objects2docspec o2s
		left outer join v_objects v on (v.id = o2s.linked_object_id and v.object_type = 'container') 
		left outer join carriages c on (c.id = v.linked_object_id) 
		where j.id = l.joint_id and o2s.doc_id = @id and not v.cnum is null 
		and l.task_id = o2s.task_id and l.container_id = o2s.linked_object_id;
	end;
	
	close cDocNewData;
	deallocate cDocNewData;
	
END;



GO
