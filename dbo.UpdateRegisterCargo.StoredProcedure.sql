USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[UpdateRegisterCargo]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateRegisterCargo]
(
@ObjectId int output,
@Cnum varchar(50),
@TypeId int,
@WeightSender float,
@WeightDoc float,
@WeightFact float,
@SealNumber varchar(50),
@IsEmpty bit,
@Note varchar(450),
@ParentObjectId int
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	/*if isnull(@Objectid,0)=0
	begin
	    if not (@SealNumber is null)
		select @Objectid = o.id from cargos c, objects o 
		where c.seal_number = @SealNumber and o.id = c.id and o.object_type = 'cargo';
	end;*/
	
	if isnull(@Objectid,0)=0
	begin
	
	   exec InsertObject 'cargo', @ObjectId output; 
	   
	   if isnull(@SealNumber,'')=''
	   set @SealNumber = '{n/a:'+convert(varchar,@ObjectId)+'}';
	
	   insert into cargos (id, cnum, type_id, note, weight_sender, weight_doc, weight_fact, seal_number, isempty)
	   values (@ObjectId, @Cnum, @TypeId, @Note, @WeightSender, @WeightDoc, @WeightFact, @SealNumber, @Isempty);
	   
	end else
	begin
	
	   if @Cnum = '' set @Cnum = null;
	   if @TypeId = 0 set @TypeId = null; 
	   if @Note = '' set @Note = null;
	   if @WeightSender = 0 set @WeightSender = null; 
	   if @WeightDoc = 0 set @WeightDoc = null; 
	   if @WeightFact = 0 set @WeightFact = null; 
	   if @SealNumber = '' set @SealNumber = null;
	
	   update cargos set 
	   cnum = isnull(@Cnum, cnum),
	   type_id = isnull(@TypeId, type_id),
	   note = isnull(@Note,note),
	   weight_sender = isnull(@WeightSender, weight_sender),
	   weight_doc = isnull(@WeightDoc, weight_doc),
	   weight_fact = isnull(@WeightFact,weight_fact), 
	   isempty = isnull(@Isempty, 0),
	   seal_number = isnull(@SealNumber, seal_number)
	   where id = @ObjectId;
	   
	end;
	
	update objects set linked_object_id = @ParentObjectId where id = @Objectid;

	update objects set state_id = (select state_id from objects o1 where o1.id = @ParentObjectId) where id = @ObjectId and state_id is null;
	
END
GO
