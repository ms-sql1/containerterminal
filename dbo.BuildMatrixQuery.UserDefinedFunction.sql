USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[BuildMatrixQuery]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[BuildMatrixQuery](@sectorid int, @rownum int, @levelnum int)
RETURNS varchar(MAX)
as
begin	
	
	declare @sql_text varchar(MAX), @stacknumfull varchar(50), @stacknum int, @oldstacknum int, @cellnum int,  @delim varchar(1), @columndelim varchar(50), 
	@columnsfull varchar(MAX), @columns varchar(MAX), @sfilter varchar(100), @rowreverse bit, @stackreverse bit, @levelreverse bit, 
	@order1 varchar(10), @order2 varchar(10), @i int;

	select @rowreverse = isnull(s.roworderreverse, 0), @levelreverse = isnull(s.levelorderreverse, 0), @stackreverse = isnull(s.stackorderreverse, 0)
	from matrixsectors s where s.id = @sectorid;

	set @order1 = '1, ';
	set @order2 = '2, ';
	
	if @rowreverse = 1
	set @order1 = '1 desc, '
	
	if @levelreverse = 1
	set @order2 = '2 desc, '

	if @stackreverse = 0
	begin
		declare stasks cursor for
		select 
		convert(varchar, s.order_num)+'_'+convert(varchar, c.cell_num) as stacknum,
		s.order_num as stacknum, c.cell_num 
		from matrixstacks s, matrixcells c 
		where s.sector_id = @sectorid
		and (isnull(s.isnarrow,0)=0 or c.cell_num<2)
		order by s.order_num, c.cell_num;
	end else
	begin
		declare stasks cursor for
		select 
		convert(varchar, s.order_num)+'_'+convert(varchar, c.cell_num) as stacknumfull,
		s.order_num as stacknum, c.cell_num 
		from matrixstacks s, matrixcells c 
		where s.sector_id = @sectorid
		and (isnull(s.isnarrow,0)=0 or c.cell_num<2)
		order by s.order_num desc, c.cell_num;
	end;

	OPEN stasks

	set @delim = '';
	set @columndelim = '';
	set @columns = '';
	set @columnsfull = '';
	set @cellnum = 0;
	set @i = 1;
	set @oldstacknum = -1;

	while 1=1
	begin 
	
	  FETCH NEXT FROM stasks INTO @stacknumfull, @stacknum, @cellnum;

	  if @oldstacknum>0 and @stacknum<>@oldstacknum
	  set  @columndelim = ', '' '' as w'+convert(varchar,@i);
	  
	  if @@FETCH_STATUS <> 0 break;

	  set @columns = @columns+@delim+'['+@stacknumfull+']';
	  set @columnsfull = @columnsfull+@columndelim+@delim+'['+@stacknumfull+'] as s'+@stacknumfull;

      set @delim = ',';
	  set @columndelim = '';
	  set @oldstacknum = @stacknum;

	  set @i = @i+1;

	end;

	set @sfilter = '';
	if @rownum>0 and @levelnum>0
	set @sfilter = ' and r.order_num = '+convert(varchar,@rownum)+' and l.level_num = '+convert(varchar,@levelnum);

	set @sql_text = 
	'SELECT t2.row_num, t2.level_num,'+@columnsfull+
	' FROM  '+
    '( '+
	'select r.order_num as row_num, '+
	'convert(varchar, t.order_num)+''_''+convert(varchar, c.cell_num) as order_num_stacks, '+
	'l.level_num, '+
	'dbo.GetContainerMatrixInfo(m2c.container_id, me.levels, me2.cells)+''|''+convert(varchar,m2c.container_id) as ContInfo '+
	'from matrixsectors s '+
	'inner join matrixrows r on (r.sector_id = s.id) '+
	'inner join matrixstacks t on (t.sector_id = s.id) '+
	'inner join matrixlevels l on (l.level_num <= s.levels) '+
	'inner join matrixcells c on (1=1) '+
	'left outer join matrix2containers m2c '+
	'on (m2c.sector_id = r.sector_id and m2c.row_id = r.id and m2c.stack_id = t.id and m2c.level_num = l.level_num and m2c.cell_num = c.cell_num) '+
	'left outer join matrixexcludes me '+
	'on (me.sector_id = s.id and me.limit_type = 1'+
	     'and me.row_start <= r.order_num and me.row_end >= r.order_num '+
		 'and me.stack_start <= t.order_num and me.stack_end >= t.order_num '+
		 'and me.levels < s.levels '+
	' ) '+
	'left outer join matrixexcludes me2 '+
	'on (me2.sector_id = s.id and me2.limit_type = 2'+
	     'and me2.row_start <= r.order_num and me2.row_end >= r.order_num '+
		 'and me2.stack_start <= t.order_num and me2.stack_end >= t.order_num '+
		 'and me2.cells < c.cell_num '+
	' ) '+
	'where s.id = '+convert(varchar, @sectorid)+@sfilter+
	') AS t1 '+
	'PIVOT '+
	'( '+
	'	max(ContInfo) '+
	'   FOR t1.order_num_stacks IN ('+@columns+')'+
	') AS t2 '+
	'order by '+@order1+@order2+'3'
	
	return @sql_text

end;


GO
