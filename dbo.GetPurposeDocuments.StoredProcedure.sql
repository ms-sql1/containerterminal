USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[GetPurposeDocuments]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPurposeDocuments] (@selectguid int)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @docid int, @objectId int, @rootdocid int, @doctypeid int, @deliverytypeid int,  @docrouteid int, @objecttypeid int, @startstate int, @endstate int, @startlink int, @endlink int;
	
	select @doctypeid = d.doctype_id, @deliverytypeid = t.dlv_type_id from documents d, tasks t where d.id = @DocId and t.root_doc_id = d.root_doc_id;
	
	select @docrouteid = id from docroutes r where r.dlv_type_id = @deliverytypeid and r.end_doctype_id = @doctypeid;
	
	select @objecttypeid = id from objecttypes where code = (select object_type from v_objects where id = @ObjectId);
	
	select 
	@startstate = s.start_object_state_id,  
	@endstate = s.end_object_state_id,
	@startlink = s.start_link_type,
	@endlink = s.end_link_type
	from docroute2objectstates s 
	where s.docroute_id = @docrouteid and objecttype_id = @objecttypeid;
	
END

GO
