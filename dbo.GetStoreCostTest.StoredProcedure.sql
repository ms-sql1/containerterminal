USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[GetStoreCostTest]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[GetStoreCostTest] (@Type int, @KindId int, @CustomerId int, 
@DateStartStore datetime, @DateCalcBegin datetime, @DateCalcEnd datetime)
As
Begin 

	if @DateCalcEnd is null set @DateCalcEnd = GETDATE();
	
	if @DateStartStore > @DateCalcEnd return 0;
	
	if @DateStartStore > @DateCalcBegin set @DateCalcBegin = @DateStartStore;
	
	declare @Sum float, 
	@curdays int, @cid int, @days int, @price float, @datelastchange datetime, @datechange datetime, @daysstart int, @daysstore int;
	
	-- получаем максимальную дату последнего изменения цены
	
	select @datelastchange = c.date_change from price_store_custs c,
	(
	select t.id from price_store_types t
	where (conttype = @Type or (conttype is null and not exists (select 1 from price_store_types t1 where t1.conttype = @Type)))
	and (
			contkind_id = @KindId or 
			(contkind_id is null and not exists (select 1 from price_store_types t1 where t1.contkind_id = @KindId))
		) 
	) as t2
	where c.type_id = t2.id and c.date_change <= @DateStartStore
	and (
		customer_id = @CustomerId or 
		(customer_id is null and not exists (select 1 from price_store_custs where customer_id = @CustomerId))
	)
	order by c.date_change desc;
	
	
	--declare cStore cursor for
	select * from price_store_days d,
	(
		select c.id, c.date_change from price_store_custs c,
		(
		select t.id from price_store_types t
		where (conttype = @Type or (conttype is null and not exists (select 1 from price_store_types t1 where t1.conttype = @Type)))
		and (
				contkind_id = @KindId or 
				(contkind_id is null and not exists (select 1 from price_store_types t1 where t1.contkind_id = @KindId))
			) 
		) as t2
		where c.type_id = t2.id and c.date_change >= @datelastchange and c.date_change < @DateStartStore
		and (
			customer_id = @CustomerId or 
			(customer_id is null and not exists (select 1 from price_store_custs where customer_id = @CustomerId))
		)
	) c2 where d.pricecust_id = c2.id
	order by c2.date_change;
	
	
end;

GO
