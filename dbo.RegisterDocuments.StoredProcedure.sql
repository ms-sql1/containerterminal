USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[RegisterDocuments]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RegisterDocuments]
(@systemsection varchar(50), @trainnum varchar(50), @folderid int, @startnumber int, @userid int) 
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @num int;
	declare @doctypeid int, @uid uniqueidentifier, @id int;
	
	if @systemsection = 'income_unload'
	begin
	
		
		select @doctypeid = id from doctypes where system_section = @systemsection;
		
		declare cDocData cursor for 
		select rowuid from docfeed 
		where id is null;
	 
		open cDocData;
		
		fetch next from cDocData into @uid;
		
		set @num = @startnumber;
	      
		while @@fetch_status = 0  
		begin 
		
			if ISNULL(@startnumber,0) = 0 set @num = null;
	 
			insert into documents (doc_number, doctype_id, folder_id, date_modified, user_created) 
			values (@num, @doctypeid, @folderid, null, @userid); 
			
			set @num = @num + 1;
			
			select @id = @@IDENTITY;
			
			update docfeed set id = @id, train_num = @trainnum where rowuid = @uid;
			
			fetch next from cDocData into @uid;
	 
		end 
	 
		close cDocData;
		deallocate cDocData;
    
    end;
    
    
	if @systemsection = 'income_sheet'
	begin
	
		select @doctypeid = id from doctypes where system_section = @systemsection;

		declare cDocData cursor for 
		select rowuid from doccargosheet 
		where id is null;
	 
		open cDocData;
		
		fetch next from cDocData into @uid;
		
		set @num = @startnumber;
	      
		while @@fetch_status = 0  
		begin 
		
			if ISNULL(@startnumber,0) = 0 set @num = null;
	 
			insert into documents (doc_number, doctype_id, folder_id, date_modified, user_created) values (@num, @doctypeid, @folderid, null, @userid); 
			
			set @num = @num + 1;
			
			select @id = @@IDENTITY;
			
			update doccargosheet set id = @id, train_num = @trainnum where rowuid = @uid;
			
			fetch next from cDocData into @uid;
	 
		end 
	 
		close cDocData;
		deallocate cDocData;
    
    end;
    
END

GO
