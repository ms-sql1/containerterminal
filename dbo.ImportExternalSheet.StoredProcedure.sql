USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[ImportExternalSheet]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ImportExternalSheet] 
(
@guid varchar(40), @systemsection varchar(50), 
@folderid int, @trainnum varchar(50), @prefix varchar(50), @startnumber int, @userid int,
@emptytype int, @loadtype int
)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @e int, @m varchar(450), @mess varchar(1024), @doctypeid int;
	
	select @e=0, @m='', @mess='';
	
	-- проверяем наличие контрагентов
	
	select top 1 @m =s.contowner from ExternalCargoSheet s where guid = @guid 
	and not (
		ltrim(rtrim(s.contowner)) in (select code from counteragents c)
		or
		ltrim(rtrim(s.contowner)) in (select name from counteragents c)
	);
	
	if @m <> ''
	begin
	  select @mess = 'Собственник контейнера '+@m+' не был определен в словаре Контрагенты.'+CHAR(10);
	  exec CreateCounteragent @m;
	end;
	
	select top 1 @m =s.carowner from ExternalCargoSheet s where guid = @guid 
	and not (
		ltrim(rtrim(s.carowner)) in (select code from counteragents c)
		or
		ltrim(rtrim(s.carowner)) in (select name from counteragents c)
	);
	
	if @m <> ''
	begin
	  select @mess = @mess+'Собственник вагона '+@m+' не был определен в словаре Контрагенты.'+CHAR(10);
	  exec CreateCounteragent @m;
	end;
	
	if @systemsection = 'income_unload'
	begin
		begin try
			insert into docfeed (
				prefix,	container_num, container_foot, carriage_num, 
				seal_number, container_owner_id, carriage_owner_id, 
				weight_sender,cargo_description
				)
			select 
			@prefix, s.contnumber, s.conttype, s.carnumber, s.seal,
			(select min(id) from counteragents where ltrim(rtrim(code)) = ltrim(rtrim(s.contowner)) or ltrim(rtrim(name)) = ltrim(rtrim(s.contowner))),
			(select min(id) from counteragents where ltrim(rtrim(code)) = ltrim(rtrim(s.carowner)) or ltrim(rtrim(name)) = ltrim(rtrim(s.carowner))),
			s.weight, s.descr
			from ExternalCargoSheet s 
			where rtrim(ltrim(guid)) = rtrim(ltrim(@guid)) and (rtrim(ltrim(s.contnumber))<>'' or rtrim(ltrim(s.carnumber))<>'') 
			and not rtrim(ltrim(s.contnumber)) like 'Итог%';
		end try
		begin catch
		  select @mess = @mess+'Возникала ошибка при импорте данных:'+ERROR_MESSAGE()+CHAR(10);
		end catch;	
		
		exec RegisterDocuments 'income_unload', @trainnum, @folderid, @startnumber,@userid;
		
	end;
	
	
	if @systemsection = 'income_sheet'
	begin
	
		begin try
			insert into doccargosheet (
				prefix,	datesent, container_num, container_foot, carriage_num, 
				seal_number, container_owner_id, carriage_owner_id, 
				weight_sender,cargo_description, dlv_type_id, isempty, consignee_id, guard_sign
				)
			select @prefix, s.datesent, s.contnumber, s.conttype, s.carnumber, s.seal,
			(select min(id) from counteragents where ltrim(rtrim(code)) = ltrim(rtrim(s.contowner))),
			(select min(id) from counteragents where ltrim(rtrim(code)) = ltrim(rtrim(s.carowner))),
			replace(s.weight,',','.'), s.descr,
			(case when isnull(s.seal,'') = '' then @emptytype else @loadtype end), 
			(case when isnull(s.seal,'') = '' then 1 else 0 end), 
			(select top 1 c.id from counteragents c where ltrim(rtrim(code)) = ltrim(rtrim(s.descr)) or ltrim(rtrim(name)) = ltrim(rtrim(s.descr))),
			(case when rtrim(s.guard)='' then 0 else 1 end)
			from ExternalCargoSheet s 
			where rtrim(ltrim(guid)) = rtrim(ltrim(@guid)) and (rtrim(ltrim(s.contnumber))<>'' or rtrim(ltrim(s.carnumber))<>'') 
			and not rtrim(ltrim(s.contnumber)) like 'Итог%';
		end try
		begin catch
		  select @mess = @mess+'Возникала ошибка при импорте данных:'+ERROR_MESSAGE()+CHAR(10);
		end catch;	
		
		exec RegisterDocuments 'income_sheet', @trainnum, @folderid, @startnumber,@userid;
		
	end;
	
	
	delete from ExternalCargoSheet where guid = @guid;
	
	if @mess <> '' RAISERROR(@mess,16,-1);
	
END
GO
