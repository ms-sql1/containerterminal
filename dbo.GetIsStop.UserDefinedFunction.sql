USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[GetIsStop]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[GetIsStop](@docid integer)
RETURNS integer
as
begin
	

  declare @stopsign integer;

  select @stopsign = count(*) from v_lastobjectstates ls, objectstatekinds sk, objects2docspec ds
  where ds.doc_id = @docid and ds.task_id = ls.task_id and ls.object_state_id = sk.id 
  and ls.object_id = ds.object_id and sk.stop_sign = 1;

  return @stopsign;

end;

GO
