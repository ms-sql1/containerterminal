USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[GetContainerInfo]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetContainerInfo](@Docid int)
	RETURNS varchar(250)
AS
BEGIN

	declare @cnum varchar(50), @isempty bit, @kind varchar(50), @kindreal varchar(50), @info varchar(250), @docnumber varchar(50), @note varchar(250), @station varchar(450),
	@isconfirmed int, @ordercontid int, @state int, @sealnumber varchar(50), @weight float;

	declare @ContainerId int, @IsFreeLoad bit, @KindId int, @TaskId int, @OrderSpecId int;
	
	select @ContainerId = l.container_id, @IsFreeLoad = l.isfreeload, @KindId = l.kind_id, @TaskId = l.task_id, @OrderSpecId = l.orderspec_id 
	from docload l where id = @Docid;  

	set @isconfirmed = 0;
	set @ordercontid = 0;
	
	select 
	@docnumber = d.doc_number, @note = o.note, @station = (select name from stations st where o.station_id = st.id),
	@ordercontid = s.container_id
	from docorderspec s, documents d, docorder o where o.id = s.doc_id and s.id = @OrderSpecId and o.id = d.id

	select 	@isconfirmed = d.isconfirmed, @state = ls.object_state_id
	from documents d, docload l, v_lastobjectstates ls
	where d.id = @Docid and l.id = d.id and ls.task_id = l.task_id 
	and ls.object_id = l.container_id;

	set @kindreal = null;
	select @kind = isnull(code,'') from containerkinds k where k.id = @KindId;

	if ISNULL(@ContainerId,-1)>0 
	begin

		select @sealnumber = isnull(cr.seal_number,''), @weight = cr.weight_fact from cargos cr where cr.id = (select object_id from tasks where id = @TaskId);
		select @isempty = s.isempty from docorderspec s where s.id = @OrderSpecId;

		select @sealnumber = (case when @sealnumber like '{%' then '' else @sealnumber end);
		select @cnum = c.cnum+','+isnull(@sealnumber,''), @kindreal = isnull(k.code,'') from containers c 
		left outer join containerkinds k on k.id = c.kind_id 
		where c.id = @ContainerId;

	end else
	begin

		if isnull(@IsFreeLoad,0) = 1
		select @cnum = '{порожний}'; 

	end;

	set @kindreal = isnull(@kindreal, @kind);

	if @isempty=1 set @cnum = '['+@cnum+']'

	set @info = 
	(case when isnull(@state,0) = 12 then '!' when isnull(@state,0) = 16 then '~' else ' ' end)+
	(case when isnull(@ordercontid,0) = 0 then '*' else '=' end)+''+
	@cnum +','+	(case when @kind = @kindreal then @kind else @kind+'>'+@kindreal end)+'='+isnull(format(@weight, N'#'),'')+','+
	'№'+isnull(@docnumber,'!удален!')+','+isnull(@station,'')+','+isnull(@note,'');
	
	return @info;
		
END;

GO
