USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[GetContainerInfoExt]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetContainerInfoExt](@ContainerId int, @IsFreeLoad bit, @KindId int, @TaskId int)
	RETURNS varchar(250)
AS
BEGIN

	declare @cnum varchar(50), @isempty bit, @kind varchar(50), @weight varchar(12), @seal varchar(50), @info varchar(250);
	
	if ISNULL(@ContainerId,-1)>0 
	begin
		
		select @isempty = isnull(isempty,0), @weight = ltrim(str(cr.weight_fact/1000, 10)), @seal = ltrim(isnull(cr.seal_number,'-'))
		from cargos cr where cr.id = (select object_id from tasks where id = @TaskId);
		
		if @seal like '{%' set @seal = '-';
		
		select @cnum = c.cnum+(case when @isempty = 1 then ',*' else '' end), @kind = isnull(k.code,'') from containers c 
		left outer join containerkinds k on k.id = c.kind_id 
		where c.id = @ContainerId;
		
	end else
	begin
		
		if isnull(@IsFreeLoad,0) = 1
		select @cnum = '', @kind = isnull(code,'') from containerkinds k where k.id = @KindId;
		
		select @weight = '', @seal = '';
		
	end;

	set @info = @cnum+'|'+@kind+'|'+isnull(@weight,'')+'|'+isnull(@seal,'');
	
	return @info;
		
END;

GO
