USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[ImportCarLinks]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ImportCarLinks]
(@start bit, @col1 varchar(200),@col2 varchar(200),@col3 varchar(200),
@col4 varchar(200),@col5 varchar(200),@col6 varchar(200),@col7 varchar(200))
AS
BEGIN
	
	SET NOCOUNT ON;
	
	declare @t TABLE(rn int, cont_num varchar(200));
	declare @p int, @contnum1 varchar(50), @contnum2 varchar(50), @contnum3 varchar(50), @contnum4 varchar(50);

	if @start = 1
	begin
		IF OBJECT_ID('tempdb.dbo.##temp_prepared_newdoc', 'U') IS NOT NULL DROP TABLE ##temp_prepared_newdoc; 	
		create table ##temp_car_links 
		(car_err varchar(250), car_num varchar(50), car_foot varchar(50), car_owner varchar(100), car_model varchar(50),
			cont1_err varchar(250), cont1_num varchar(50), 
			cont2_err varchar(250), cont2_num varchar(50), 
			cont3_err varchar(250), cont3_num varchar(50), 
			cont4_err varchar(250), cont4_num varchar(50) 
			); 
	end;
	
	-- разбираем входные данные
	
	insert into @t (rn, cont_num) values (1, dbo.split_pair(@col5,',',1));
	set @col5 = dbo.split_pair(@col5,',',2);
	insert into @t (rn, cont_num) values (2, dbo.split_pair(@col5,',',1));
	set @col5 = dbo.split_pair(@col5,',',2);
	insert into @t (rn, cont_num) values (3, dbo.split_pair(@col5,',',1));
	
	insert into @t (rn, cont_num) values (4, dbo.split_pair(@col6,',',1));
	set @col6 = dbo.split_pair(@col6,',',2);
	insert into @t (rn, cont_num) values (5, dbo.split_pair(@col6,',',1));
	set @col6 = dbo.split_pair(@col6,',',2);
	insert into @t (rn, cont_num) values (6, dbo.split_pair(@col6,',',1));
	
	insert into @t (rn, cont_num) values (7, dbo.split_pair(@col7,',',1));
	set @col7 = dbo.split_pair(@col7,',',2);
	insert into @t (rn, cont_num) values (8, dbo.split_pair(@col7,',',1));
	set @col7 = dbo.split_pair(@col7,',',2);
	insert into @t (rn, cont_num) values (9, dbo.split_pair(@col7,',',1));
	
	delete from @t where isnull(cont_num,'')='';
	
	declare t cursor for select cont_num from @t order by rn;
	open t;
	
	fetch next from t into @contnum1;
	fetch next from t into @contnum2;
	fetch next from t into @contnum3;
	fetch next from t into @contnum4;
	
	close t;
	deallocate t;

	insert into ##temp_car_links 
	(car_num, car_foot, car_owner, car_model, cont1_num, cont2_num, cont3_num, cont4_num)
	values
	(@col1, @col2, @col3, @col4, @contnum1, @contnum2, @contnum3, @contnum4);

END
GO
