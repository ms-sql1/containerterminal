USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[SetParentCounteragent]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetParentCounteragent](@selectguid varchar(40), @ParentId int)
AS
BEGIN

	SET NOCOUNT ON;
	
	declare @parent_id int, @e int, @folderid int;
	
	set @parent_id = null;
	select @parent_id = parent_id, @folderid  = folder_id from counteragents where id = @ParentId;

	if isnull(@parent_id, 0)<>0 
	begin
	   raiserror('Главный контрагент уже входит в группу. Операция отменена.',16,1);	
	   return;
	end;
	
	set @e = 0;
	select @e = 1 where exists 
	(select 1 from  counteragents c, selectlist sl where sl.guid = @selectguid and sl.id = c.id and c.id = @ParentId);
	
	if @e = 1 
	begin
	   raiserror('Контрагент не может ссылаться на самого себя. Операция отменена.',16,1);	
	   return;
	end;
	

	set @e = 0;
	select @e = 1 where exists 
	(
		select 1 from  counteragents c, selectlist sl where sl.guid = @selectguid and sl.id = c.id 
		and exists (select 1 from counteragents c1 where c1.parent_id = c.id)
	);
	
	if @e = 1 
	begin
	   raiserror('Контрагент является главным. Операция отменена.',16,1);	
	   return;
	end;

	
	set @e = 0;
	select @e = 1 where exists 
	(select 1 from  counteragents c, selectlist sl where sl.guid = @selectguid and sl.id = c.id and not c.parent_id is null);
	
	if (@e = 1) and ISNULL(@ParentId,0)<>0
	begin
	   raiserror('Контрагент уже входит в группу. Операция отменена.',16,1);	
	   return;
	end;

	
	update counteragents set parent_id = @ParentId, folder_id = isnull(@folderid,folder_id)
	where id in	(select id from selectlist where guid = @selectguid);
	
	delete from selectlist where guid = @selectguid;
	
END

GO
