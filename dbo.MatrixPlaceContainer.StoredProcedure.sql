USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[MatrixPlaceContainer]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[MatrixPlaceContainer](@Operation int, @ContainerId int, @SectorId int, @RowNum int, @StackNum int, @LevelNum int, @CellNum int) 
AS
BEGIN
	SET NOCOUNT ON;

	declare @oldSector varchar(50), @oldRowNum int, @oldStackNum int, @e int, @m varchar(450), @ContNearId int, @CellNearNum int, @FootMain int, @FootNear int;
	declare @RowId int, @StackId int; 

	select @RowId = id from matrixrows where sector_id = @SectorId and order_num = @RowNum;
	select @StackId = id from matrixstacks where sector_id = @SectorId and order_num = @StackNum;

	begin tran

	if @Operation in (1,2)
	begin

	  if @ContainerId = 0
	  
		  delete from matrix2containers 
		  where sector_id = @SectorId
		  and row_id = @RowId 
		  and stack_id = @StackId 
		  and cell_num = @CellNum 
		  and level_num = @LevelNum;

	  else

		  delete from matrix2containers 
		  where container_id = @ContainerId;

	end;
	  
	if @Operation in (0,2)
	begin

	  set @e=0;
	  select @e=1, 
	  @oldSector = s.code,
	  @oldRowNum = r.order_num, 
	  @oldStackNum = t.order_num
	  from matrix2containers m2c, matrixsectors s, matrixrows r, matrixstacks t
	  where m2c.container_id = @ContainerId
	  and m2c.sector_id = s.id and r.id = m2c.row_id and t.id = m2c.stack_id;

	  if @e=1
	  begin

		set @m = 'Такой контейнер уже размещен: участок '+ @oldSector+', штабель: '+convert(varchar, @oldRowNum)+', секция: '+convert(varchar, @oldStackNum);
		raiserror(@m, 16, 1);
		rollback tran
	    return;

	  end;

	  -- проверяем соседнюю ячейку
	  if @CellNum = 1 set @CellNearNum = 2;
	  if @CellNum = 2 set @CellNearNum = 1;

	  select @FootMain = ck.footsize from containers c, containerkinds ck where ck.id = c.kind_id and c.id = @ContainerId;

	  set @ContNearId = null;

	  select @ContNearId = m2c.container_id from matrix2containers m2c 
	  where sector_id = @SectorId 
	  and row_id = @RowId 
	  and stack_id = @StackId 
	  and level_num = @LevelNum
	  and cell_num = @CellNearNum; 

	  if isnull(@ContNearId,0)<>0
	  begin

		  select @FootNear = ck.footsize from containers c, containerkinds ck where ck.id = c.kind_id and c.id = @ContNearId;

		  if @FootMain = 40 or @FootNear = 40
		  begin
			 raiserror('Невозможно разместить рядом контейнеры такой размерности.', 16, 1);
			 rollback tran
			 return;
		  end;

	  end;

	  if @FootMain = 40 and @CellNum = 2
		begin
			raiserror('Для контейнера 40фут. доступна только ячейка 1', 16, 1);
			rollback tran
			return;
		end;

	  -- проверяем на запрет
	  if @FootMain = 40
	  begin

		set @e=0
		select @e = 1 where exists (select 1 from matrixexcludes me 
		where me.limit_type = 2 
		and me.sector_id = @SectorId
		and me.row_start<=@RowNum
		and me.row_end>=@RowNum
		and me.stack_start<=@StackNum
		and me.stack_end>=@StackNum
		and me.cells<2);

		if @e=1
		begin
			raiserror('Невозможно разместить в этом месте контейнер 40фут.', 16, 1);
			rollback tran
			return;
		end;

	  end; 

	  insert into matrix2containers (sector_id, row_id, stack_id, cell_num, level_num, container_id)
	  values (@Sectorid, @RowId, @StackId, @CellNum, @LevelNum, @ContainerId);

	end;

	commit tran;

END



GO
