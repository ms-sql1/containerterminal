USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[AllowEdit]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AllowEdit](@DocId int, @UserId int)
AS
BEGIN
	SET NOCOUNT ON;

	update documents set allow_edit = 1, allow_user_id = @UserId where id = @DocId;

	insert into change_log(table_id, table_name, user_id, event_type, event_datetime)
	values (@DocId, 'documents', @UserId, 'allowedit', getdate());


END

GO
