USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[ChangeOrder]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ChangeOrder](@selectguid varchar(42), @neworderspecid int)
AS
BEGIN

	SET NOCOUNT ON;

	declare @e int;

	select @e=0;

	select orderspec_id into #temporderspec from docload l, selectlist sl where sl.guid = @selectguid and sl.id = l.id

	select @e=1 where exists (
		select 1 from docload l
		where l.id in
		(
			select id from selectlist sl where sl.guid = @selectguid
		) 
		and isnull(l.isfreeload,0)=0
	);

	if @e=1 
	begin
	   raiserror('Можно подменять заявку только при свободной погрузке.',16,1);
	   return;
	end;

	select @e=1 where exists (
		select 1 from docload l, docorderspec s, docorderspec s1 
		where s1.id = @neworderspecid 
		and s.id = l.orderspec_id and s1.id = @neworderspecid
		and	l.id in
		(
			select id from selectlist sl where sl.guid = @selectguid
		) 
		and s1.container_kind_id <> s.container_kind_id
	);

	if @e=1 
	begin
	   raiserror('Типы контейнеров сейчас и в новой заявке не совпадают.',16,1);
	   return;
	end;

	SET CONTEXT_INFO 0x001;

	update l set 
	l.orderspec_id = @neworderspecid,
	l.station_id = o1.station_id,
	l.note = o1.note,
	l.owner_id = s1.container_owner_id
	from docload l, docorderspec s, docorderspec s1, docorder o1
	where s.id = l.orderspec_id 
	and s1.id = @neworderspecid
	and o1.id = s1.doc_id
	and	l.id in
	(
		select id from selectlist sl where sl.guid = @selectguid
	) and l.isfreeload = 1
	and s1.container_kind_id = s.container_kind_id;

	update t set 
	t.basedoc_date = d1.doc_date, 
	t.basedoc_number = d1.doc_number,
	--t.basedoc_id = d1.id,
	t.consignee_id = o1.consignee_id,
	t.forwarder_id = o1.forwarder_id,
	t.payer_id = o1.payer_id,
	t.station_id = o1.station_id
	from tasks t, docload l, docorderspec s, docorderspec s1, docorder o1, documents d1
	where t.id = l.task_id
	and s.id = l.orderspec_id 
	and s1.id = @neworderspecid
	and o1.id = s1.doc_id and o1.id = d1.id
	and	l.id in
	(
		select id from selectlist sl where sl.guid = @selectguid
	) and l.isfreeload = 1
	and s1.container_kind_id = s.container_kind_id;

	SET CONTEXT_INFO 0x000;

	update docorderspec 
	set used_amount = (select count(*) from docload l where l.orderspec_id = docorderspec.id)
	/*where id in (
		select orderspec_id from #temporderspec 
	)*/

END
GO
