USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[RegisterDataChange]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RegisterDataChange](@TableName varchar(250), @TableId int, @EventType varchar(50), @UserId int) 
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @user_id int, @id int, @eventtypeold varchar(50);
	set @id = null;
	
	select top 1 @user_id = user_id, @id = id, @eventtypeold = event_type
	from change_log where table_name = @TableName and table_id = @TableId
	order by event_datetime desc;
	
	if @user_id = @UserId and @eventtypeold = @EventType
	delete from change_log where id = @id; 
	
	insert into change_log (table_name, table_id, event_type, event_datetime, user_id)
	values (@TableName, @TableId, @EventType, GETDATE(), @UserId);
END
GO
