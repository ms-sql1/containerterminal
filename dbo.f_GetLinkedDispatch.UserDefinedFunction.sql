USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetLinkedDispatch]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[f_GetLinkedDispatch](@DocId int)
	RETURNS TABLE
AS
	RETURN 
	select d1.*
	from documents d1, doctypes t1 
	where  d1.id>@DocId and t1.id = d1.doctype_id
	and t1.system_section = 'outcome_dispatch' and d1.id in
	(
		select ds1.doc_id from 
		(
		select ds.object_id, ds.task_id 
		from documents d, objects2docspec ds 
		where ds.doc_id = d.id and d.id = @DocId
		and isnull(d.isdeleted,0)=0 
		) s1, objects2docspec ds1
		where ds1.task_id = s1.task_id
		and ds1.object_id = s1.object_id 
		and ds1.doc_id<> @DocId
	) 	and isnull(d1.isdeleted,0)=0; 
GO
