USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[RegisterCarriagesFromSheet]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RegisterCarriagesFromSheet](@g varchar(40), @dateincome datetime)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @ObjectId int, @CarNum varchar(50), @CarOwnerId int, @CarType varchar(50), @DocId int, @TrainNum varchar(50);
	
	DECLARE cDocs CURSOR FOR   
	select sh.carriage_num, sh.carriage_owner_id, sh.carriage_type, sh.id, sh.train_num
	from doccargosheet sh, selectlist sl where sl.guid = @g and sl.id = sh.id;
	
	open cDocs;
	
	fetch next from cDocs into @CarNum, @CarOwnerId, @CarType, @DocId, @TrainNum;
      
	while @@fetch_status = 0  
	begin 
	
		exec UpdateRegisterCarriage @ObjectId, @CarNum, @CarType, @CarOwnerId, null, null, @dateincome;

		update carriages set train_num = @TrainNum where id = @ObjectId;
		
		fetch next from cDocs into @CarNum, @CarOwnerId, @CarType, @DocId, @TrainNum;
		
	end;
	
	close cDocs;
	deallocate cDocs;

END
GO
