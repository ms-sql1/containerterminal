USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[CopyCargoParams]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyCargoParams](@oldid int, @newid int)
AS
BEGIN
	SET NOCOUNT ON;
	
	update c1 set 
	c1.weight_doc = c2.weight_doc, 
	c1.weight_fact = c2.weight_fact, 
	c1.weight_sender = c2.weight_sender, 
	c1.seal_number = c2.seal_number
	from cargos c1, cargos c2 
	where c2.id = @oldid
	and c1.id = @newid;

END

GO
