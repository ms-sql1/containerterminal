USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[f_folders_tree]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[f_folders_tree] (@startid int)
	RETURNS TABLE
AS
	RETURN 
	WITH folders_tree (id, fname, folder_section, flevel) AS
	(
		SELECT Id, folder_name, folder_section, 0 AS flevel FROM folders WHERE ( isnull(Id,0) = @startid) 
		UNION ALL
		SELECT f.Id, f.folder_name, f.folder_section, t.flevel + 1 from folders f INNER JOIN folders_tree t ON f.Parent_Id = t.id
	)
	SELECT * FROM folders_tree;

GO
