USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetFilterParams]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[f_GetFilterParams](@userid int, @doctype int)
RETURNS @return_params TABLE (datebegin datetime, dateend datetime, isconfirmed int)
AS
begin

	declare @datebegin datetime, @dateend datetime, @isconfirmed int;

	select @datebegin = convert(datetime, min(f.v), 104) from temp_filter f where f.c = 'date_begin' 
	and (f.userid = @userid and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0);

	select @dateend = convert(datetime, max(f.v), 104) from temp_filter f where f.c = 'date_end' 
	and (f.userid = @userid and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0);

	select @isconfirmed = convert(int, max(f.v)) from temp_filter f where f.c = 'isconfirmed' 
	and (f.userid = @userid and CHARINDEX(','+convert(varchar(10),@doctype)+',',f.doctypes)>0);

	insert @return_params select @datebegin, @dateend, @isconfirmed;

	return;

end;
GO
