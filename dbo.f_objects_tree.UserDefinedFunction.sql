USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[f_objects_tree]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[f_objects_tree] (@startid int)
	RETURNS TABLE
AS
	RETURN 
	WITH objects_tree (id, start_datetime, object_type, state_id, linked_object_id, address_id, owner_id, current_task_id, olevel) AS
	(
		SELECT id, start_datetime, object_type, state_id, linked_object_id, address_id, owner_id, current_task_id, 0 AS olevel 
		FROM objects WHERE ( isnull(Id,0) = @startid or @startid = -1) 
		UNION ALL
		SELECT f.id, f.start_datetime, f.object_type, f.state_id, f.linked_object_id, f.address_id, f.owner_id, f.current_task_id, t.olevel + 1 from 
		objects f INNER JOIN objects_tree t ON t.linked_object_id = f.id
	)
	SELECT * FROM objects_tree;
GO
