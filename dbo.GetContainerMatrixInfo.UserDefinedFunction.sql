USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[GetContainerMatrixInfo]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetContainerMatrixInfo](@Containerid int, @Levels int, @Cells int)
	RETURNS varchar(250)
AS
BEGIN

	if @Containerid is null
	begin
		if not @Levels is null return '~-';
		if not @Cells is null return '~-';
	end
	
	declare @cnum varchar(50), @ContInfo varchar(120), @Marks varchar(120);
	declare @type varchar(20), @owner varchar(50), @ownmark varchar(1), @defect varchar(1), @notready varchar(1), @expir varchar(1), @loaded varchar(1), @longstay varchar(1); 
	declare @statemark varchar(4), @formalstatemark varchar(4), @nextstatemark varchar(4), @loadplanned varchar(1), @colormark varchar(1);
	declare @allowed varchar(1); 

	select top 1 @cnum = c.cnum, 
	@defect  = (case when isnull(c.isdefective,0) = 1 then '1' else '0' end),
	@notready  = (case when isnull(c.isnotready,0) = 1 then '1' else '0' end),
	@expir = (case when isnull(c.acep,0) = 0 and getdate()- date_stiker < 30 then '1' else '0' end), 
	@loaded = (case when cr.isempty = 1 then '0' when cr.isempty = 0 then '1' else '0' end), 
	@statemark = isnull(sk_real.mark,isnull((select mark from objectstatekinds sk3 where sk3.id = o.real_state_id),'')),  
	@formalstatemark = isnull(sk.mark,''),  
	@nextstatemark = isnull(sk_next.mark,''),  
	@loadplanned = 
	(case 
	  when exists (
		select 1 from tasks t0, objects o0, objectstatekinds sk0, deliverytypes dt0 
		where t0.object_id = o0.id and o0.state_id = sk0.id and t0.dlv_type_id = dt0.id
		and dt0.global_section='output'
		and isnull(sk0.inplacesign,0)=1 and isnull(sk0.isformalsign,0) = 1 and isnull(sk0.inworksign,0) = 0 
		and (lat.start_date-t0.start_date < 100) and o0.linked_object_id = c.id
	  ) 
	  then '2'
	  when exists (select 1 from docload l, documents d where l.id = d.id and isnull(d.isconfirmed,0) = 0 and l.container_id = c.id)
	  then '1' 
	  else '0'
	  end
	),
	@owner = isnull((select ct.code from counteragents ct, objects o where o.id = c.id and ct.id = o.owner_id), ''),

	@ownmark = (case 
	  when o.owner_id in (select ct.id from counteragents ct, settings s where s.code = 'fesco_owners' and charindex(ct.code, s.value)>0)
	  then '' else 'С'
	end),

	@type = isnull((select ck.code from containerkinds ck where ck.id = c.kind_id), ''),
	@longstay = isnull((case when getdate()- lat.start_date > 80 then '1' else '0' end), 0), 
	@colormark = isnull((select convert(varchar(1), mark) from matrixmarks mr where mr.container_id = c.id),'') 
	from containers c
	inner join objects o on (o.id = c.id)
	left outer join v_activecontainertask lat on (lat.container_id = c.id)
	left outer join cargos cr on (cr.id = lat.cargo_id)
	left outer join objectstatekinds sk on (sk.id = lat.state_id) 
	left outer join objectstatekinds sk_real on (sk_real.id = lat.real_state_id) 
	left outer join objectstatekinds sk_next on (sk_next.id = lat.next_state_id) 
	where c.id = @Containerid
	order by lat.stay_start_date desc;

	set @allowed = '+';
	if (not @Levels is null) or (not @Cells is null)
	set @allowed = '-';

	if @cnum is null 
	begin
		set @ContInfo = '';
		set @Marks = '';
	end else
	begin
		set @ContInfo = @ownmark+'|'+@cnum+'|'+@type;
		--+'|'+@owner;
		set @Marks = @allowed+'|'+@loaded+'|'+@statemark+'|'+@nextstatemark+'|'+@loadplanned+'|'+@formalstatemark+'|'+@defect+'|'+@notready+'|'+@expir+'|'+@longstay+'|'+@colormark;
	end;

    return @ContInfo+'~'+@Marks;
		
END;
GO
