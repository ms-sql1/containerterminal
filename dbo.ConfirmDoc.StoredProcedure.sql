USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[ConfirmDoc]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ConfirmDoc](
@DocId int,
@SpecId int,
@DateFactExecution datetime, 
@UserId int
) 
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @m varchar(450), @e int, @id int,@SystemType varchar(50), @isconfirmed bit, 
	@WeightDoc float, @WeightSender float, @WeightFact float, @SealNumber varchar(50),
	@PlanDate datetime, @DefDeliveryTypeId int, @DeliveryTypeId int, @ForwarderId int, 
	@StationId int, @ConsigneeId int,@PayerId int, @OrderId int, @checkresult int, 
	@ObjectId int, @ContId int, @CarId int, @CargoId int, @CargoTypeId int, @isdefective bit, 
	@ContNumber varchar(50), @ContFoot varchar(50), @CarNumber  varchar(50), @CarFoot varchar(50),
	@ContOwnerId int, @CarOwnerId int, @Note varchar(450), @Size varchar(50), @ContKindId int,
	@DocNumber varchar(50), @DocDate datetime, @TaskId int, 
	@email varchar(250), @globalsection varchar(50),
	@avoidobjectupdate bit, @isdeleted bit;


	set @e = 0;
	select @e=1 where exists 
	(
		select 1 from objectstatehistory osh, objects2docspec o2s 
		where o2s.doc_id = @DocId and o2s.task_id = osh.task_id
		and osh.date_factexecution > @DateFactExecution
		and (o2s.id = @SpecId or @SpecId is null)
	);


	if @e = 1
	begin
	  raiserror('Проведение документа датой, которая раньше даты проведения документа-основания, невозможно.',16,1);	
	  return;
	end;

	
	declare @CargoObjectId int, @ContObjectId int, @CarObjectId int, @IsEmpty bit, @IsFreeLoad bit;
	
	select @SystemType = t.system_section, 
	@isconfirmed = isconfirmed, @isdeleted = isdeleted, 
	@avoidobjectupdate = t.avoid_object_update
	from documents d, doctypes t where d.id = @DocId and t.id = d.doctype_id;

	if isnull(@isdeleted,0) = 1 return;
	
	if isnull(@isconfirmed,0) = 1 and isnull(@SpecId,0)=0 return;

	set @ContKindId = null;
	
	if @SystemType = 'income_unload'
	begin
	
		begin transaction;
		
		select @DocNumber = d.doc_number, @DocDate = d.doc_date,	
		@ContNumber = s.container_num, 
		@ContKindId = sh.container_kind_id, @ContFoot = s.container_foot, 
		@Size = s.container_foot, 
		@CarNumber = s.carriage_num, @ContOwnerId = s.container_owner_id, 
		@CarFoot = s.carriage_type, @CarOwnerId = s.carriage_owner_id, 
		@WeightSender = s.weight_sender, @WeightFact = s.weight_fact, @WeightDoc = s.weight_doc,
		@SealNumber = s.seal_number, @Note = s.cargo_description, 
		@DeliveryTypeId = s.dlv_type_id, @checkresult = s.check_result, 
		@IsEmpty = sh.isempty
		from docfeed s, documents d, doccargosheet sh
		where s.id = d.id and sh.unload_doc_id = s.id  and d.id = @DocId;
		
		if (
			isnull(@ContNumber,'')='' or  
			isnull(@CarNumber,'')='' or  
			isnull(@DeliveryTypeId,0)=0 or
			--@ContOwnerId is null or  
			--@CarOwnerId is null or
			@WeightFact is null
		) 
		begin
			raiserror('Не заполнены обязательные поля',16,-1);
			rollback transaction;
			return 0;
		end;

		select @TaskId = o2s.task_id from objects2docspec o2s where o2s.doc_id = @DocId;

		if @TaskId is null
		select @TaskId = o2s.task_id from objects2docspec o2s where o2s.doc_id = (select id from doccargosheet sh where sh.unload_doc_id = @DocId);
		
		-- проверим наличие этих обьектов и истории у них
		set @e = 0;
		select @e = 1 where exists
		(
			select 1 from objects o, containers c where o.id = c.id 
			and c.cnum in (@ContNumber) and o.real_state_id in
			(select id from objectstatekinds where inworksign=1)
		);

		if @e>0 
		begin
			set @m = 'Документ с номером контейнера '+@ContNumber+' уже проведен. Контейнер находится в работе.';
			raiserror(@m,16,1);
			rollback transaction;
			return 0;
		end;

		set @e = 0;
		select @e=1 where exists 
		(
			select 1 from v_lastobjectstates los, containers c 
			where los.object_id = c.id 
			and c.cnum in (@ContNumber) and los.object_state_id in 
			(select id from objectstatekinds where inworksign=0 and inplacesign=1)
		);
		
		if @e>1 
		begin
			set @m = 'Контейнер '+@ContNumber+' находится на терминале в статусе хранения. Проведение невозможно.';
			raiserror(@m,16,1);
			rollback transaction;
			return 0;
		end;
		
		select @CarObjectId = cr.id from objects2docspec o2s, carriages cr where o2s.object_id = cr.id and o2s.task_id = @TaskId;
		select @ContObjectId = cr.id from objects2docspec o2s, containers cr where o2s.object_id = cr.id and o2s.task_id = @TaskId;
		select @CargoObjectId = cr.id from objects2docspec o2s, cargos cr where o2s.object_id = cr.id and o2s.task_id = @TaskId;
		
		exec UpdateRegisterCarriage @CarObjectId output, @CarNumber, @CarFoot, @CarOwnerId, null, null;
		exec UpdateRegisterContainer @ContObjectId output, @ContNumber, @ContKindId, @Size, @ContOwnerId, null, @CarObjectId;
		exec UpdateRegisterCargo @CargoObjectId output, @ContNumber, @CargoTypeId, @WeightSender, @WeightDoc, @WeightFact, @SealNumber, @IsEmpty, @Note, @ContObjectId;
		
		set @ObjectId = @CargoObjectId; 
		if (@CargoObjectId is null) set @ObjectId = @ContObjectId; 
		
		exec UpdateRegisterTask @TaskId output, @DeliveryTypeId, @PlanDate, @ForwarderId, @ConsigneeId, @PayerId, @ObjectId, @DocId, @DocNumber, @DocDate; 
	
		update docfeed set task_id = @TaskId where id = @DocId;
		
		exec RegisterDocSpec @DocId, @TaskId, @ObjectId, @e out;

		if @e = 0
		begin
			rollback tran;
			raiserror('Проведение документа не имеет результата(1). Скорее всего, неправильно оформлен документ.',16, 1);
			return;
		end;

		
		update l set task_id = o2s.task_id, object_id = o2s.linked_object_id
		from doccargoletters l, objects2docspec o2s where l.doc_id = o2s.doc_id;
		
		update l set task_id = o2s.task_id, object_id = o2s.linked_object_id
		from doccargorestrictions l, objects2docspec o2s where l.doc_id = o2s.doc_id;
		
		update l set task_id = o2s.task_id, object_id = o2s.linked_object_id
		from doccargoservices l, objects2docspec o2s where l.doc_id = o2s.doc_id;
		
		commit transaction;	
	
	end;
	
	
	if @SystemType = 'income_sheet'
	begin
		
		begin transaction;
		
		select @DocNumber = d.doc_number, @DocDate = d.doc_date,	
		@PlanDate = s.dlv_term, @DeliveryTypeId = s.dlv_type_id, 
		@ForwarderId = s.forwarder_id, @ConsigneeId = s.consignee_id, 
		@PayerId = s.payer_id, @ContNumber = s.container_num, 
		@ContFoot = s.container_foot, @Size = s.container_foot, 
		@CarNumber = s.carriage_num, @ContOwnerId = s.container_owner_id, @CarFoot = s.carriage_type,
		@CarOwnerId = s.carriage_owner_id, @CargoTypeId = s.cargotype_id,
		@WeightSender = s.weight_sender, @WeightFact = s.weight_fact, @WeightDoc = s.weight_doc,
		@SealNumber = s.seal_number, @IsEmpty = s.isempty, 
		@Note = s.cargo_description
		from doccargosheet s, documents d
		where s.id = d.id and d.id = @DocId;
		
		if (
			@DeliveryTypeId is null or
			@ForwarderId is null or  
			@ConsigneeId is null or  
			@PayerId is null or  
			isnull(@ContNumber,'')='' or  
			isnull(@CarNumber,'')='' or  
			@ContOwnerId is null or  
			@CarOwnerId is null or
			@WeightDoc is null
		) 
		begin
			raiserror('Не заполнены обязательные поля',16,-1);
			rollback transaction;
			return 0;
		end;

		select @TaskId = o2s.task_id from objects2docspec o2s where o2s.doc_id = @DocId;

		if @TaskId is null
		select @TaskId = o2s.task_id from objects2docspec o2s where o2s.doc_id = 
		(select f.id from docfeed f, doccargosheet sh where f.id = sh.unload_doc_id and sh.id = @DocId);
		
		-- проверим наличие этих обьектов и истории у них
		set @e = 0;
		select @e = 1 where exists
		(
			select 1 from v_lastobjectstates los, containers c 
			where los.object_id = c.id 
			and c.cnum in (@ContNumber) and los.object_state_id in 
			(select id from objectstatekinds where inworksign=0 and inplacesign=1)
		)
		
		if @e>1 
		begin
			set @m = 'Контейнер '+@ContNumber++' находится в терминале в статусе хранения. Проведение невозможно.';
			raiserror(@m,16,1);
			rollback transaction;
			return 0;
		end;


		select @CarObjectId = cr.id from objects2docspec o2s, carriages cr where o2s.object_id = cr.id and o2s.task_id = @TaskId;
		select @ContObjectId = cr.id from objects2docspec o2s, containers cr where o2s.object_id = cr.id and o2s.task_id = @TaskId;
		select @CargoObjectId = cr.id from objects2docspec o2s, cargos cr where o2s.object_id = cr.id and o2s.task_id = @TaskId;

		exec UpdateRegisterCarriage @CarObjectId output, @CarNumber, @CarFoot, @CarOwnerId, null, null;
		exec UpdateRegisterContainer @ContObjectId output, @ContNumber, @ContKindId, @Size, @ContOwnerId, null, @CarObjectId;
		exec UpdateRegisterCargo @CargoObjectId output, @ContNumber, @CargoTypeId, @WeightSender, @WeightDoc, @WeightFact, @SealNumber, @IsEmpty, @Note, @ContObjectId;
		
		set @ObjectId = @CargoObjectId; 
		if (@CargoObjectId is null) set @ObjectId = @ContObjectId; 
		
		exec UpdateRegisterTask @TaskId output, @DeliveryTypeId, @PlanDate, @ForwarderId, @ConsigneeId, @PayerId, @ObjectId, @DocId, @DocNumber, @DocDate; 
	
		update doccargosheet set task_id = @TaskId where id = @DocId;
		
		exec RegisterDocSpec @DocId, @TaskId, @ObjectId, @e out;

		if @e = 0
		begin
			rollback tran;
			raiserror('Проведение документа не имеет результата(2). Скорее всего, неправильно оформлен документ.',16, 1);
			return;
		end;

		update l set task_id = o2s.task_id, object_id = o2s.linked_object_id
		from doccargoletters l, objects2docspec o2s where l.doc_id = o2s.doc_id;
		
		update l set task_id = o2s.task_id, object_id = o2s.linked_object_id
		from doccargorestrictions l, objects2docspec o2s where l.doc_id = o2s.doc_id;
		
		update l set task_id = o2s.task_id, object_id = o2s.linked_object_id
		from doccargoservices l, objects2docspec o2s where l.doc_id = o2s.doc_id;
		
		commit transaction;
		
	end;
	
	
	if @SystemType = 'outcome_apps'
	begin
		
		begin transaction;
		
		select @DocNumber = d.doc_number, 
		@DocDate = d.doc_date,	
		@DeliveryTypeId = o.dlv_type_id, 
		@ForwarderId = o.forwarder_id, 
		@ConsigneeId = o.consignee_id, 
		@PayerId = o.payer_id,
		@StationId = o.station_id
		from docorder o, documents d
		where o.id = d.id and d.id = @DocId;

		select @globalsection = global_section from deliverytypes t where t.id = @DeliveryTypeId;
		
		if (
			@DeliveryTypeId is null or
			@ForwarderId is null or  
			(@globalsection = 'output' and @ConsigneeId is null) or  
			(@globalsection = 'output' and @StationId is null) or  
			@PayerId is null
		) 
		begin
			raiserror('Не заполнены обязательные поля',16,1);
			rollback transaction;
			return 0;
		end;
		
		DECLARE cDocSpec CURSOR FOR   
		select container_id, cargo_id, container_num, s.container_kind_id, 
		container_owner_id, weight_cargo, seal_number, cargotype_id, isempty
		from docorderspec s where s.doc_id = @DocId
		and not exists (select 1 from objects2docspec o2s where o2s.doc_id = @DocId and o2s.object_id = s.cargo_id)
		and (isnull(@SpecId,0) = 0 or s.id = @SpecId);
		
		open cDocSpec;
		
		fetch next from cDocSpec into 
		@ContObjectId, @CargoObjectId, @ContNumber, @ContKindId, @ContOwnerId, @WeightFact, @SealNumber, @CargoTypeId, @IsEmpty;
	      
		while @@fetch_status = 0  
		begin 
		
			if (
				--isnull(@ContNumber,'')='' or  
				(@IsEmpty=0 and @CargoTypeId is null)
			) 
			begin
				close cDocSpec;
				deallocate cDocSpec;
				raiserror('Не заполнены обязательные поля',16,1);
				rollback transaction;
				return 0;
			end;
			
			-- проверим наличие этих обьектов и истории у них
			set @e = 0;
			select @e = 1 where exists
			(
				select 1 from objects o, containers c where o.id = c.id 
				and c.cnum in (@ContNumber) and o.real_state_id in
				(select id from objectstatekinds where inworksign=1)
			)
			
			/*if @e>0 
			begin
				close cDocSpec;
				deallocate cDocSpec;
				set @m = 'Документ с номером контейнера '+@ContNumber+' уже проведен. Контейнер находится в работе.';
				raiserror(@m,16,1);
				rollback transaction;
				return 0;
			end;*/

			-- Заявки всегда генерят новую задачу на каждую строчку спецификации
			set @TaskId = null;
			
			if ISNULL(@ContNumber,'')<>''
			begin
			
				set @CarObjectId = null;
				
				exec UpdateRegisterContainer @ContObjectId output, @ContNumber, @ContKindId, null, @ContOwnerId, null, @CarObjectId;
				exec UpdateRegisterCargo @CargoObjectId output, @ContNumber, @CargoTypeId, @WeightSender, @WeightDoc, @WeightFact, @SealNumber, @IsEmpty, @Note, @ContObjectId;
				
				set @ObjectId = @CargoObjectId; 
				if (@CargoObjectId is null) set @ObjectId = @ContObjectId; 
				
				exec UpdateRegisterTask @TaskId output, @DeliveryTypeId, @PlanDate, @ForwarderId, @ConsigneeId, @PayerId, @ObjectId, @DocId, @DocNumber, @DocDate, @StationId; 
				exec RegisterDocSpec @DocId, @TaskId, @ObjectId, @e out;

				/*if @e = 0
				begin
				  close cDocSpec;
				  deallocate cDocSpec;
				  rollback tran;
				  raiserror('Проведение документа не имеет результата(3). Скорее всего, неправильно оформлен документ.',16, 1);
				  return;
				end;*/
			
			end;
			
			fetch next from cDocSpec into 
			@ContObjectId, @CargoObjectId, @ContNumber, @ContKindId, @ContOwnerId, @WeightFact, @SealNumber, @CargoTypeId, @IsEmpty;
	 
		end; 
		
		close cDocSpec;
		deallocate cDocSpec;
		
		commit transaction;
		
	end;	
	
	if @SystemType = 'outcome_loads'
	begin
		
		begin transaction;
		
		set @e = null;
		select @e=1 from docloadjoint k, docload l where k.id = l.joint_id and l.id = @DocId and not l.container_id is null;
		
		if @e is null 
		begin
			raiserror('Контейнер не привязан к вагону. Проведение невозможно.',16,1);
			rollback transaction;
			return 0;
		end;

		set @CarObjectId = null;
		set @ContObjectId = null;
		set @CargoObjectId = null;


		-- изменение 20/08/2019
		update docload set task_id = null where id in 
		(
			select l.id from docload l, documents d where not exists (select 1 from tasks t where t.id = l.task_id) 
			and  d.id = l.id and d.isconfirmed = 0 and d.id = @DocId
		);

		-- изменение 04/08/2019
		update docload set task_id = (select t.id from tasks t, docorderspec s where t.object_id = s.cargo_id and t.basedoc_id = s.doc_id and s.id = docload.orderspec_id)
		where task_id is null and docload.id = @DocId;


		select 
		@CarObjectId = k.carriage_id, 
		@ContObjectId = l.container_id, 
		@ContNumber = c.cnum, 
		@CargoObjectId = t.object_id,
		@IsEmpty = isnull(cr.isempty, s.isempty),
		@IsFreeLoad = isnull(l.isfreeload,0),
		@DeliveryTypeId = t.dlv_type_id,
		@TaskId = l.task_id,
		@OrderId = l.orderspec_id
		from docloadjoint k, containers c, docload l
		left outer join tasks t on (t.id = l.task_id) 
		left outer join objects o1 on (o1.linked_object_id = l.container_id and o1.current_task_id = l.task_id)
		left outer join cargos cr on (cr.id = t.object_id)
		left outer join docorderspec s on (s.id = l.orderspec_id)
		where k.id = l.joint_id 
		and l.container_id = c.id and l.id = @DocId;
		
		if @DeliveryTypeId is null
		select @DefDeliveryTypeId = t.dlv_type_id from documents d, doctypes t where d.id = @DocId and t.id = d.doctype_id;

		if (@TaskId is null) and (not @DeliveryTypeId is null)
		begin
			set @m = 'Контейнер '+@ContNumber+' не находится в задаче доставки. Невозможно определить грузополучателя и станцию назначения.';
			raiserror(@m,16,1);
			return;
		end;

		if /*(@DeliveryTypeId<>@DefDeliveryTypeId) or*/ (@DeliveryTypeId is null)
		begin
			select 
			@ForwarderId = o.forwarder_id, 
			@ConsigneeId = o.consignee_id, 
			@PayerId = o.payer_id, 
			@StationId = o.station_id			 
			from docorder o, docorderspec s
			where o.id = s.doc_id and s.id = @OrderId;
			
			set @DeliveryTypeId = @DefDeliveryTypeId;
			set @TaskId = null;
			
		end;
		
		begin transaction;
		
		exec UpdateRegisterCarriage @CarObjectId output, @CarNumber, null, @CarOwnerId, null, null;
		
		exec UpdateRegisterContainer @ContObjectId output, @ContNumber, @ContKindId, @Size, @ContOwnerId, null, @CarObjectId;
	
		exec UpdateRegisterCargo @CargoObjectId output, @ContNumber, @CargoTypeId, @WeightSender, @WeightDoc, @WeightFact, @SealNumber, @IsEmpty, @Note, @ContObjectId;
		
		set @ObjectId = @CargoObjectId; 
		if (@CargoObjectId is null) set @ObjectId = @ContObjectId; 

		-- погруженный контейнер удаляем из матрицы
		delete from matrix2containers where container_id = @ContObjectId;
		delete from matrixhelper  where container_id = @ContObjectId;
		
		exec UpdateRegisterTask @TaskId output, @DeliveryTypeId, @PlanDate, @ForwarderId, @ConsigneeId, @PayerId, @ObjectId, @DocId, @DocNumber, @DocDate, @StationId; 
	
		exec RegisterDocSpec @DocId, @TaskId, @ObjectId, @e out;


		if @e = 0
		begin
			rollback tran;
			raiserror('Проведение документа не имеет результата(4). Скорее всего, неправильно оформлен документ.',16, 1);
			return;
		end;

		update docload set task_id = @TaskId where id = @DocId and task_id is null;

		commit transaction;
		commit transaction;
		
	end;	
	

	if @SystemType = 'income_empty'
	begin
		
		begin transaction;
		
		select @DeliveryTypeId = o.dlv_type_id 
		from docincome o, documents d
		where o.id = d.id and d.id = @DocId;

		if (
			@DeliveryTypeId is null
		) 
		begin
			raiserror('Не заполнены обязательные поля',16,1);
			rollback transaction;
			return 0;
		end;
		

		DECLARE cDocSpec CURSOR FOR   
		select container_id, cargo_id, container_num, s.container_kind_id, 
		container_owner_id, weight_cargo, seal_number, cargotype_id, isempty, ds.task_id,
		s.is_defective
		from docincomespec s
		left outer join objects2docspec ds on (ds.doc_id = s.doc_id and ds.object_id = s.container_id)
		where s.doc_id = @DocId;
		
		open cDocSpec;
		
		fetch next from cDocSpec into 
		@ContObjectId, @CargoObjectId, @ContNumber, @ContKindId, @ContOwnerId, @WeightFact, @SealNumber, @CargoTypeId, @IsEmpty, @TaskId, @isdefective;
	      
		while @@fetch_status = 0  
		begin 
		
			if (
				isnull(@ContNumber,'')='' 
				--or (@IsEmpty=0 and @CargoTypeId is null)
			) 
			begin
				raiserror('Не заполнены обязательные поля(2)',16,1);
				rollback transaction;
				close cDocSpec;
				deallocate cDocSpec;
				return 0;
			end;
			
			-- проверим наличие этих обьектов и истории у них

			-- Приемка
			set @e = 0;
			select @e = 1 where exists
			(
				select 1 from objects o, containers c where o.id = c.id 
				and c.cnum in (@ContNumber) and o.real_state_id in
				(select id from objectstatekinds where inplacesign=1)
			) and exists (
				select 1 from deliverytypes d where d.id = @DeliveryTypeId and d.global_section = 'output-store'
			)
			
			if @e>0 
			begin
				close cDocSpec;
				deallocate cDocSpec;
				raiserror('Согласно данным учета контейнер находится на терминале. Повторная приемка невозможна.',16,1);
				rollback transaction;
				return 0;
			end;

			-- Выдача
			set @e = 0;
			select @e = 1 where exists
			(
				select 1 from objects o, containers c where o.id = c.id 
				and c.cnum in (@ContNumber) and o.real_state_id in
				(select id from objectstatekinds where inplacesign=0)
			) and exists (
				select 1 from deliverytypes d where d.id = @DeliveryTypeId and d.global_section = 'input-store'
			)
			
			if @e>0 
			begin
				close cDocSpec;
				deallocate cDocSpec;
				raiserror('Согласно данным учета контейнер не находится на терминале. Выдача контейнера невозможна.',16,1);
				rollback transaction;
				return 0;
			end;

			-- КОНЕЦ ПРОВЕРОК
			
			set @CarObjectId = null;
			
			if not @TaskId is null select @DeliveryTypeId = dlv_type_id from tasks where id = @TaskId;
			
			if isnull(@avoidobjectupdate, 0) = 0
			begin
				exec UpdateRegisterContainer @ContObjectId output, @ContNumber, @ContKindId, null, @ContOwnerId, null, @CarObjectId;
				exec UpdateRegisterCargo @CargoObjectId output, @ContNumber, @CargoTypeId, @WeightSender, @WeightDoc, @WeightFact, @SealNumber, @IsEmpty, @Note, @ContObjectId;
			end;
			
			set @ObjectId = @CargoObjectId; 
			if (@CargoObjectId is null) set @ObjectId = @ContObjectId; 

			exec UpdateRegisterTask @TaskId output, @DeliveryTypeId, @PlanDate, @ForwarderId, @ConsigneeId, @PayerId, @ObjectId, @DocId, @DocNumber, @DocDate; 
			
			exec RegisterDocSpec @DocId, @TaskId, @ObjectId, @e out;

			if @e = 0
			begin
			  close cDocSpec;
			  deallocate cDocSpec;
			  rollback tran;
			  raiserror('Проведение документа не имеет результата(5). Скорее всего, неправильно оформлен документ.',16, 1);
			  return;
			end;
			
			insert into containerdefecthistory (container_id, isdefective, register_date, register_doc_id)
			values (@ContObjectId, @isdefective, @DateFactExecution, @DocId);

			fetch next from cDocSpec into 
			@ContObjectId, @CargoObjectId, @ContNumber, @ContKindId, @ContOwnerId, @WeightFact, @SealNumber, @CargoTypeId, @IsEmpty, @TaskId, @isdefective;
	 
		end; 
		
		close cDocSpec;
		deallocate cDocSpec;
		
		commit transaction;

	end;
	
	
	if @SystemType = 'outcome_dispatch'
	begin
		update carriages set current_load_id = null
		where not exists 
		(
		select 1 from docloadjoint i, docload l, documents d 
		where carriages.id = i.carriage_id
		and l.joint_id = i.id
		and l.id = d.id and d.isconfirmed = 0
		)
		and not current_load_id is null;
	end;


	if @TaskId is null
	begin
	   select @TaskId = MAX(task_id) from objects2docspec ds where doc_id = @DocId;
	end;
	
	if @ObjectId is null
	begin
	   select @ObjectId = MAX(object_id) from tasks t where t.id = @TaskId;
	end;
	
	if (@ObjectId is null) and (@SystemType <> 'outcome_apps')
	begin
		raiserror('Ошибка регистрации обьектов(1).',16,1);
		return 0;
	end;

	exec RegisterState @DocId, @DateFactExecution;

	if @SystemType = 'income_empty'
	begin
	
		select @e = count(*) from objects2docspec ds, objectstatehistory osh, cargos cr
		where ds.doc_id = osh.doc_id and ds.object_id = osh.object_id and cr.id = osh.object_id
		and osh.task_id = ds.task_id and ds.doc_id = @DocId;

		if @e = 0
		begin
			delete from objectstatehistory where doc_id = @DocId;
			delete from objects2docspec where doc_id = @DocId;
			set @m = 'Проведение документа не имеет результата.'+char(10)+'Вероятно, неправильно оформлен документ. '+char(10)+'Попробуйте удалить документ и создать заново.';
			raiserror(@m,16, 1);
			return;
		end;
	
	end;

	insert into change_log(table_id, table_name, user_id, event_type, event_datetime)
	values (@DocId, 'documents', @UserId, 'confirm', getdate());
	
	update documents set isconfirmed = 1, date_confirm = GETDATE() where id = @DocId and isnull(isconfirmed,0) = 0;

	delete from matrixhelper where state_mark = '<';
	
END
GO
