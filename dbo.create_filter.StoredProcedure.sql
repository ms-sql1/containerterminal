USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[create_filter]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[create_filter](@userid int, @doctypes varchar(100), @conditions varchar(max))
as
begin

  --delete from temp_filter where userid = @userid and  exists (select 1 from dbo.StringSplit(doctypes,',') where value in (select value from dbo.StringSplit(@doctypes,',') ) and value<>'');	
  delete from temp_filter where userid = @userid and doctypes  = @doctypes;	

  if isnull(@conditions,'')='' return;
  
  insert into temp_filter select @userid, @doctypes, dbo.split_pair(value, '=', 1), dbo.split_pair(value, '=', 2) 
  from StringSplit2Table(@conditions, ';',2);

end;

GO
