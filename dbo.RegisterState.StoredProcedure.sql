USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[RegisterState]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RegisterState] (@DocId int, @DateFactExecution datetime)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @ObjectId int, @id int, @doctypeid int, @deliverytypeid int,  @docrouteid int;
	
	select @doctypeid = doctype_id from documents where id = @DocId;
	
	select doctype_id from documents where id = @DocId;

	insert into objectstatehistory (object_id, object_state_id, linked_object_id, docspec_id, doc_id, task_id, date_history, date_factexecution)
	select distinct s1.object_id, drs.end_object_state_id, --isnull(drs.end_object_state_id,s1.state_id), 
	(case when isnull(drs.end_link_type,1) = 1 then s1.linked_object_id else null end) as new_linked_object_id, 
	s1.docspec_id, @DocId, s1.task_id, GETDATE(), @DateFactExecution
	from docroutes r, docroute2objectstates drs,
	(
		select s0.docspec_id, s0.object_id, s0.linked_object_id, link_type, objecttype_id, s0.task_id, s0.dlv_type_id,
		isnull(ls.object_state_id, s0.state_id) as state_id
		from 
		(
			select ods.id as docspec_id, ods.object_id, 
			ods.linked_object_id, o.state_id,
			(case when ods.linked_object_id is null then 0 else 1 end) as link_type,
			(select id from objecttypes where code = o.object_type) as objecttype_id,
			ods.task_id, t.dlv_type_id
			from objects2docspec ods, objects o, tasks t
			where ods.doc_id = @DocId
			and o.id = ods.object_id
			and t.id = ods.task_id
		) s0
		--left outer join objectstatehistory osh on (osh.task_id = s0.task_id and osh.object_id = s0.object_id)
		left outer join v_lastobjectstates ls on (ls.task_id = s0.task_id and ls.object_id = s0.object_id)
	) s1
	where r.end_doctype_id = @doctypeid 
	and r.id = drs.docroute_id
	and r.dlv_type_id = s1.dlv_type_id
	and (drs.start_object_state_id = s1.state_id or drs.start_object_state_id is null)
	and drs.objecttype_id = s1.objecttype_id
	and (drs.start_link_type = s1.link_type or drs.start_link_type is null)
	and not exists (
		select 1 from objectstatehistory osh2 
		where osh2.task_id = s1.task_id 
		and osh2.object_id = s1.object_id
		and osh2.object_state_id = drs.end_object_state_id
		and osh2.doc_id = @DocId
	)
	
	exec UpdateObjectState @DocId, 0;	
	
END

GO
