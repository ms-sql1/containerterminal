USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[SetCarIncomeDate]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetCarIncomeDate](@guid varchar(40), @DateIncome datetime, @SheetNumber varchar(50), @TrainNum varchar(50))
AS
BEGIN

	SET NOCOUNT ON;

	set @TrainNum = isnull(ltrim(@TrainNum),'');

	update carriages set 
	date_income = @DateIncome, 
	car_paper_number = @SheetNumber, 
	train_num = (case when @TrainNum = '' then train_num else @TrainNum end)
	where id in (select id from selectlist where guid = @guid);

	delete from selectlist where guid = @guid;

END
GO
