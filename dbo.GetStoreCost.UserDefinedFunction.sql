USE [sot]
GO
/****** Object:  UserDefinedFunction [dbo].[GetStoreCost]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[GetStoreCost] (@Type int, @KindId int, @CustomerId int, 
@DateStartStore datetime, @DateEndStore datetime, @DateCalcBegin datetime, @DateCalcEnd datetime)
Returns float
As
Begin 

	if @DateCalcEnd is null set @DateCalcEnd = GETDATE();
	
	--if @DateCalcEnd > GETDATE() set @DateCalcEnd = GETDATE();
	
	if @DateEndStore is null set @DateEndStore = GETDATE();
	
	if @DateStartStore > @DateCalcEnd return 0;

	if @DateEndStore < @DateCalcBegin return 0;
	
	if @DateEndStore < @DateStartStore return 0;

	if @DateStartStore > @DateCalcBegin set @DateCalcBegin = @DateStartStore;
	
	declare @Sum float, 
	@curdays int, @cid int, @datechange datetime, @days int, @price float, 
	@datelastchange datetime, @daysstart int, @daysstore int;
	
	-- получаем максимальную дату последнего изменения цены
	select @datelastchange = c.date_change from price_store_custs c,
	(
	select t.id from price_store_types t
	where (conttype = @Type or (conttype is null and not exists (select 1 from price_store_types t1 where t1.conttype = @Type)))
	and (
			contkind_id = @KindId or 
			(contkind_id is null and not exists (select 1 from price_store_types t1 where t1.contkind_id = @KindId))
		) 
	) as t2
	where c.type_id = t2.id and c.date_change <= @DateStartStore
	and (
		customer_id = @CustomerId or 
		(customer_id is null and not exists (select 1 from price_store_custs cs1 where cs1.customer_id = @CustomerId and cs1.type_id = t2.id))
	)
	order by c.date_change desc;
	
	-- нам надо выяснить - сколько дней прошло от даты начала хранения, до даты слежения
	select @daysstart = DATEDIFF(dd, @DateStartStore, dbo.grtdate(@DateCalcBegin,@DateStartStore)); 
	
	-- так же надо подсчитать - сколько всего дней хранения в указанном периоде
	select @daysstore = DATEDIFF(dd, dbo.grtdate(@DateCalcBegin,@DateStartStore), dbo.lstdate(@DateCalcEnd,@DateEndStore)); 
	
	-- курсор
	declare cStore cursor for
	select c2.date_change, d.days, d.price from price_store_days d,
	(
		select c.id, c.date_change from price_store_custs c,
		(
		select t.id from price_store_types t
		where (conttype = @Type or (conttype is null and not exists (select 1 from price_store_types t1 where t1.conttype = @Type)))
		and (
				contkind_id = @KindId or 
				(contkind_id is null and not exists (select 1 from price_store_types t1 where t1.contkind_id = @KindId))
			) 
		) as t2
		where c.type_id = t2.id and c.date_change >= @datelastchange and c.date_change < @DateStartStore
		and (
			customer_id = @CustomerId or 
			(customer_id is null and not exists (select 1 from price_store_custs cs1 where cs1.customer_id = @CustomerId and cs1.type_id = t2.id))
		)
	) c2 where d.pricecust_id = c2.id
	order by c2.date_change, d.days;
	
	open cStore;
	
	fetch next from cStore into @datechange, @days, @price;
	
	set @Sum = 0;
	
	while @@FETCH_STATUS = 0  
	begin 
	
		if @daysstart <= @days 
		begin
			set @Sum = @price* (dbo.lstint((@days - @daysstart), @daysstore)+1);
			if @days>=@daysstore+1 break;
			set @daysstore = @daysstore - dbo.lstint((@days - @daysstart), @daysstore);
			set @daysstart = @days + 1;
		end;
		
		fetch next from cStore into @datechange, @days, @price;
		
	end;
	
	close cStore;

	return @Sum;
	
end;

GO
