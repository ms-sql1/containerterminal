USE [sot]
GO
/****** Object:  StoredProcedure [dbo].[UpdateRegisterContainer]    Script Date: 15.02.2024 17:41:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateRegisterContainer]
(
@ObjectId int output,
@Cnum varchar(50),
@KindId int,
@Size varchar(50),
@OwnerId int,
@Note varchar(450),
@ParentObjectId int,
@containerweight float = null,
@carrying float = null,
@datemade datetime = null,
@dateinspection datetime = null,
@isdefective bit = 0,
@datestiker datetime = null,
@picture bit = null,
@acep bit = null,
@isnotready bit = null
)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @e int, @cnumold varchar(50), @oldownerid int;
	
	if ISNULL(@Objectid,0)=0
	begin
		select @Objectid = o.id from containers c, objects o 
		where c.cnum = @Cnum and o.id = c.id and o.object_type = 'container';
	end;
	
	if ISNULL(@Objectid,0)=0
	begin

	   Exec InsertObject 'container', @Objectid output;

	   insert into containers (
	    id, 
	    cnum, 
	    kind_id, 
	    size, 
	    note,
		container_weight,
		carrying,
		date_made,
		date_inspection,
		isdefective,
		date_stiker,
		picture,
		acep,
		isnotready
	   )
	   values (
	   @ObjectId, 
	   @Cnum, 
		isnull(@KindId, (select top 1 id from containerkinds ck where ck.footsize = @Size and ck.isdefault = 1)),
	    @Size, 
	    @Note,
		@containerweight,
		@carrying,
		@datemade,
		@dateinspection,
		isnull(@isdefective,0),
		@datestiker,
		isnull(@picture,0),
		isnull(@acep,0),
		isnull(@isnotready,0)
	   );
	   
	end else
	begin
	
		set @e = 0;
		select @e=1 from objectstatehistory osh where osh.object_id = @ObjectId;
		select @cnumold = cnum from containers c where id = @ObjectId;
		
		if (@e = 1) and (@cnumold <> @Cnum)
		begin
			raiserror('Нельзя менять данные контейнера, который уже проходил по учету',16,1);
			return -1;
		end;

		update containers set 
		cnum = isnull(@Cnum,cnum),
		kind_id =  isnull(@KindId,kind_id),
		size = isnull(@Size,size), 
		note = isnull(@Note,note), 
		container_weight = isnull(@containerweight,container_weight),
		carrying = isnull(@carrying,carrying),
		date_made = isnull(@datemade,date_made),
		date_inspection = isnull(@dateinspection,date_inspection),
		isdefective = isnull(@isdefective,0),
		date_stiker = isnull(@datestiker,date_stiker),
		picture = isnull(@picture,0),
		acep = isnull(@acep,0),
		isnotready = isnull(@isnotready,0)
		where id = @ObjectId;
	   
	end;
	
	select @oldownerid = owner_id from objects where id = @ObjectId;
	if (isnull(@oldownerid,0) <> @OwnerId) and (ISNULL(@OwnerId,0)<>0)
	insert into objectownerhistory (object_id, owner_id, date_change)
	values (@ObjectId, @OwnerId, GETDATE());
	
	update containers set size = (select footsize from containerkinds k where k.id = containers.kind_id) where id = @ObjectId;
	update objects set linked_object_id = @ParentObjectId where id = @Objectid;
	
	return 0;
	
END

GO
